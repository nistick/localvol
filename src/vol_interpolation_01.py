# coding: utf-8
from __future__ import print_function, division
from dateutil.parser import parse
from numpy import *
from scipy import (stats, optimize, sparse, interpolate)
import csv
import logging
import matplotlib.pyplot as plt
import numpy as np
import os
import re
import sys
import io
import levmar
import cStringIO


class UserDict(dict):
    def __init__(self, *args, **kwds):
        super(UserDict, self).__init__(*args, **kwds)
        self.__dict__ = self


class VolatilitySurface(UserDict):
    def __init__(self, *args, **kwds):
        super(VolatilitySurface, self).__init__(*args, **kwds)

    def reset_tenor_dws(self):
        index = np.logical_and(self.day > 80, self.day < 1100)
        self.day = self.day[index]
        self.term = self.term[index]
        self.quote = self.quote[index, :]
        # by nzmars for test
        self.day = array([91, 182, 273, 365, 547, 730, 1095])

    def to_str(self):
        fp = cStringIO.StringIO()
        print('\t'.join(['Expiry/Strike'] + [str(d) for d in self.strike]),
              file=fp)
        for i, td in enumerate(self.day):
            print('\t'.join(['%sD' % td] + ['%.20f' % d for d in self.quote[i]]),
                  file=fp)
        return fp.getvalue().strip()


def debug_fn(*args, **kwds):
    if 1:
        print(*args, **kwds)


def read_sd_volatility_file(filename):
    """
    Read sd csv volatility data file and return as a dict
    """
    norm_key = lambda k: re.sub(r'[\s&]+', '_', re.sub(r'\([^\)]+\)', '', k))
    lower_key = lambda k, v: (norm_key(k.lower()), v)
    lines = [d for d in csv.reader(open(filename))]
    # parse first line
    result = UserDict([lower_key(*d.strip().split(':')) for d
            in lines[0][0].split(';')])
    # parse first line as header
    result['spot'] = float(result['spot'])
    result['trade_date'] = parse(result['trade_date'])

    strike = np.array([d[:-1] for d in lines[1][2:]], dtype=float) / 100

    terms = np.array([parse(d[0]) for d in lines[3:] if d])
    trade_date = result['trade_date']
    days = np.array([(d - trade_date).days for d in terms])
    result['day'] = days
    quotes = np.array([d[2:] for d in lines[3:] if d], dtype=float) / 100
    assert quotes.shape == (len(terms), strike.size)
    result['term'] = terms
    result['strike'] = strike
    result['quote'] = quotes
    return result


def read_sd_termstructure_file(filename):
    """
    Read sd csv termstructure data file and return as a dict
    """
    norm_key = lambda k: re.sub(r'[\s&]+', '_', re.sub(r'\([^\)]+\)', '', k))
    lower_key = lambda k, v: (norm_key(k.lower()), v)
    lines = [d for d in csv.reader(open(filename))]
    result = UserDict([lower_key(*d.strip().split(':')) for d
            in lines[0][0].split(';')])
    # parse first line as header
    result['spot'] = float(result['spot'])
    result['trade_date'] = parse(result['trade_date'])

    terms = np.array([parse(d[0]) for d in lines[2:] if d])
    trade_date = result['trade_date']
    days = np.array([(d - trade_date).days for d in terms])
    result['day'] = days
    result['term'] = terms
    keys = lines[1][1:]
    factor = np.array([100 if d.find('%') >= 0 else 1 for d in keys])
    keys = [norm_key(d).lower() for d in keys]
    quote = np.array([d[1:] for d in lines[2:] if d], dtype=float)
    for i, (k, f) in enumerate(zip(keys, factor)):
        quote[:, i] /= f
        result[k] = quote[:, i]
    return result


def price_black(K, F, v, w=1):
    """

    :param K: strike
    :param F: forward
    :param v: sigma * sqrt(t)
    :param w: 1(call), -1(put)
    :return: black option price
    """
    N = stats.norm.cdf
    d1 = (np.log(F / K) + v**2 / 2) / v
    d2 = d1 - v
    return w * (F * N(w * d1) - K * N(w * d2))


def calibrate_vol_interpolation(ts, vs, option):
    ks = vs.spot * vs.strike
    black_c = np.zeros_like(vs.quote)
    black_p = np.zeros_like(vs.quote)

    xs = np.linspace(np.log(option.smin), np.log(option.smax), option.ns)
    dx = xs[1] - xs[0]
    # compute black price
    td = vs.day / 365
    for i, t in enumerate(td):
        v = vs.quote[i, :] * np.sqrt(t)
        F = vs.spot * ts.forward_factor[i]
        black_c[i, :] = price_black(ks, F, v, 1)
        black_p[i, :] = price_black(ks, F, v, -1)

    def get_pos(i, ts, vs, xs):
        ks = vs.spot * vs.strike
        f_factor = ts.forward_factor[i]
        F = vs.spot * f_factor
        dx = xs[1] - xs[0]
        m = np.round(((log(ks[1:] / F) + log(ks[:-1] / F)) / 2 - xs[0])/dx).astype(int)
        pos = np.zeros(xs.size)
        for i, d in enumerate(m):
            if i > 0:
                pos[m[i-1]:m[i]] = i
            else:
                pos[:m[i]] = i
        pos[m[i]:] = i+1
        return pos.astype(int)

    for i, ffactr in enumerate(ts.forward_factor):
        f = vs.spot * ffactr
        m = np.round(((log(ks[1:] / f) + log(ks[:-1] / f)) / 2 - xs[0])/dx).astype(int)
        print('[i=%s] ipos = %s' % (i, ', '.join([str(d) for d in m]))) # OK

    def target(vsigma, i, vc, vp, black_c, black_p, pos, dt, xs, spot, ks, ts, return_mesh):
        # use piecewise step proxy vol
        vols = vsigma[pos]
        if 1:
            # use linear interpolated proxy vol
            lks = np.log(vs.strike/ts.forward_factor[i])
            fn = interpolate.interp1d(lks, vsigma)
            vols[xs <= lks[0]] = vsigma[0]
            vols[xs >= lks[-1]] = vsigma[-1]
            idx = np.logical_and(xs >= lks[0], xs <= lks[-1])
            vols[idx] = fn(xs[idx])
        dx = xs[1] - xs[0]
        mj = 0.5 * vols ** 2 * dt / dx
        l = - mj * (1 / dx + 0.5)
        d = (1 + 2 * mj / dx)
        u = - mj * (1 / dx - 0.5)
        if 0:
            fp = cStringIO.StringIO()
            print('i proxy a b c'.replace(' ', '\t'), file=fp)
            for j, (p, a, b, c) in enumerate(zip(vols, l, d, u)):
                print('%d\t%.20f\t%.20f\t%.20f\t%.20f' % (j, p, a, b, c), file=fp)
            open('c:/py_operator.txt', 'w').write(fp.getvalue())

        # boundary condition
        u[0] = l[-1] = 0
        d[0] = d[-1] = 1
        A = np.diag(d) + np.diag(l[1:], k=-1) + np.diag(u[:-1], k=1)
        A = sparse.csc_matrix(A)
        cx = sparse.linalg.spsolve(A, vc)
        px = sparse.linalg.spsolve(A, vp)
        if 0:
            fp = cStringIO.StringIO()
            for j, v in enumerate(cx):
                print('%d\t%.20f' % (j, v), file=fp)
            open('c:/py_mesh.txt', 'w').write(fp.getvalue())

        cfun = interpolate.interp1d(xs, cx)
        pfun = interpolate.interp1d(xs, px)
        F = spot * ts.forward_factor[i]
        cval = cfun(log(ks/F)) * F
        pval = pfun(log(ks/F)) * F
        # plt.plot(ks, cval)
        # plt.plot(ks, black_c[i])
        # plt.show()
        # print(cval/black_c[i])
        # print(black_c[i])
        err = np.where(ks >= F, cval / black_c[i] - 1, pval / black_p[i] - 1)
        # gaussian kernel
        # err = err * 1/sqrt(np.pi * 2) * exp(-0.5 * log(ks/F)**2)
        # err = np.where(ks >= spot, cval - black_c[i], pval - black_p[i])
        # plt.plot(ks, err)
        # plt.grid()
        # plt.title('err')
        # plt.show()
        rmse = sqrt((err**2).mean())
        # print('rmse = ', rmse)
        # return err
        if return_mesh:
            return err, cx, px
        else:
            return err
        # return rmse

    volproxy = np.zeros_like(vs.quote)

    if 1:
        # method = 'COBYLA'
        method = 'L-BFGS-B'
        # method = 'TNC'
        method = None
        # res = optimize.leastsq(target, quote[i], args)
        bounds = [(0.0001, 2)] * vs.strike.size
        # opts = {'maxiter': 100}
        # res = optimize.minimize(target, vs.quote[i], args, method=method,
        #                         bounds=bounds,
        #                         options=opts)
        opts = {'eps1': 1e-25, 'eps2': 1e-25, 'eps3': 1e-25}
        # opts = {'eps1': 1e-15, 'eps2': 1e-15, 'eps3': 1e-15}

        cs = maximum(1.0 - exp(xs), 0)
        ps = maximum(exp(xs) - 1.0, 0)
        tt = vs.day / 365
        rmse_arr = np.zeros(vs.day.size)
        for i in range(vs.day.size):
            pos = get_pos(i, ts, vs, xs)
            dt = tt[0] if i == 0 else tt[i] - tt[i-1]
            args = [i, cs, ps, black_c, black_p, pos, dt, xs, vs.spot, ks, ts, False]
            res = levmar.levmar_bc(target, vs.quote[i],[0]*vs.strike.size, bounds, args=tuple(args), cdiff=True, **opts)
            volproxy[i, :] = res[0]
            args[-1] = True
            err_arr, cs, ps = target(volproxy[i], *args)
            rmse = sqrt((err_arr**2).mean())
            rmse_arr[i] = rmse
            print('i=%d\tTd=%s\trmse=%e' % (i, vs.day[i], rmse))
            print('vp\t%s' % '\t'.join(['%.20f' % d for d in volproxy[i]]))
            print('cs\t%s' % '\t'.join(['%.20f' % d for d in cs]))
            print('ps\t%s' % '\t'.join(['%.20f' % d for d in ps]))
            print('err\t%s' % '\t'.join(['%.20f' % d for d in err_arr]))
            if 0:
                dxs = xs[1] - xs[0]
                cg = (cs[:-2] - 2 * cs[1:-1] + cs[2:])/dxs**2
                cd = (cs[2:] - cs[:-2])/(2 * dxs)
                plt.plot(exp(xs[1:-1]), cg - cd, label='T=%sD'%vs.day[i])
                plt.grid()
                plt.legend(loc='best')
                plt.show()

        print('rmse\t%s' % '\t'.join(['%.20f' % d for d in rmse_arr]))
        print('proxy')
        print('\t'.join(['Expiry/Strike'] + [str(d) for d in vs.strike]))
        for i, d in enumerate(vs.day):
            print('\t'.join(['%sD' % d] + ['%.20f' % d for d in volproxy[i]]))
        print('vp_min\t%.20f\tvp_max\t%.20f' % (volproxy.min(), volproxy.max()))
        sys.exit()


        pp = array('0.317733596977446460000	0.370512596507600210000	0.482533407416267380000	0.376200528629686500000	0.314668921463730100000	0.276267406063539890000	0.260065016794825020000	0.256783733838448990000	0.241179154989105670000	0.231199828317761060000	0.229581605652594390000	0.139633186432971980000	0.103383999405777210000	0.102828941430750120000	0.109118511168901770000	0.125605503421341300000	0.140482992409329770000	0.149367250908915670000	0.157790495904056080000	0.162647769655042490000	0.167740046675524890000'.split(), dtype=float)
        print('rmse_fng = ', sqrt((target(pp, *args)**2).mean()))

        return res


def test_read_sd_vs(sdate, base_dir=None):
    date = sdate.replace('-', '')
    if base_dir:
        filename = os.path.join(base_dir, 'SD_KS200_Fixed_%s.csv' % date)
    else:
        filename = 'data/SD_KS200_Fixed_%s.csv' % sdate.replace('-', '')
    return read_sd_volatility_file(filename)


def test_read_sd_ts(sdate, base_dir=None):
    date = sdate.replace('-', '')
    if base_dir:
        filename = os.path.join(base_dir, 'SD_KS200_Fixed_TS_%s.csv' % date)
    else:
        filename = 'data/SD_KS200_Fixed_TS_%s.csv' % sdate.replace('-', '')
    return read_sd_termstructure_file(filename)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format='%(message)s')
    calibration_opt = UserDict(
        smin = 0.1,
        smax = 2.5,
        ns = 1001
    )
    date_str = '2016-01-13'
    # date_str = '2015-12-01'
    base_dir = r''
    vs = VolatilitySurface(test_read_sd_vs(date_str, base_dir))
    ts = test_read_sd_ts(date_str, base_dir)
    ts.discount_factor = array((0.99586274186292134, 0.99200828409482189, 0.98829144396192603, 0.98454793565769261, 0.97705044962189502, 0.9695862601534454, 0.95371912219470023))
    ts.forward_factor = array((1.0041538737249611, 1.0072800801346671, 1.0110683341459556, 1.0019633850935281, 1.0088748538461689, 1.0036730085727663, 1.006584837092847))

    vs.reset_tenor_dws()
    print(vs.to_str())

    if 1:
        dwvol = np.array([d.split()[1:] for d in open('IVSKS_DW.txt', 'r').readlines()[2:]], dtype=float)
        # print(dwvol)
        vs.quote = dwvol[1:,:]
        pass

    res = calibrate_vol_interpolation(ts, vs, calibration_opt)

# -*- coding: utf-8 -*-
from __future__ import division
from cvxopt import solvers, matrix
import cStringIO
import cvxopt
import levmar
import logging
import math
import matplotlib.pyplot as plt
import numpy as np


def read_vs_from_txt(filename):
    lines = [d.split() for d in open(filename).readlines() if d]
    # print '\n'.join(['\t'.join(d) for d in lines])
    strike = np.array(lines[0][1:], dtype=float)
    tenor = np.array([d[0][:-1] for d in lines[1:]], dtype=int)
    quote = np.array([d[1:] for d in lines[1:]], dtype=float)
    T = tenor / 365
    return dict(strike=strike, tenor=tenor, quote=quote, T=T)


def optimize(y, max_vi, market_vols, tte, s, weights):
    y = y[market_vols > 0]
    vols = market_vols[market_vols > 0]
    w = weights[market_vols>0]

    n = len(y)

    R = np.concatenate([np.ones(n), np.sqrt(y**2+1), y]).reshape(3,n).T
    W = np.diag(w)

    var_t = vols**2*tte
    max_vi = np.nanmax(var_t)

    Q = 2*matrix(R.T.dot(W).dot(R))

    Y1 = np.sum(y)
    Y2 = np.sum(y**2)
    Y3 = np.sum(np.sqrt(y**2+1))
    Y4 = np.sum(y*np.sqrt(y**2+1))
    Y5 = np.sum(y**2+1)

    v = np.sum(var_t)
    vsq = np.sum(var_t*var_t)
    vY = np.sum(var_t*y)
    vY2 = np.sum(var_t*np.sqrt(y**2+1))

    q = -2*matrix(var_t.T.dot(W).dot(R))

    h = matrix([0.0, 4.0*s, 0.0, 0.0, 4.0*s, 4.0*s, 0.0, max_vi])

    # G is in column-major order
    G = matrix([[0.0,0.0,0.0,0.0,0.0,0.0,-1.0,1.0],[-1.0,1.0,-1.0,-1.0,1.0,1.0,0.0,0.0],[0.0,0.0,-1.0,1.0,-1.0,1.0,0.0,0.0]])

    cvxopt.solvers.options['show_progress'] = False
    sol = solvers.qp(Q,q,G,h)

    # I convert back to the notation in the paper here
    # to verify that the constraints are satisfied:
    a = sol['x'][0]
    c = sol['x'][1]
    d = sol['x'][2]

    if(c < 0 or c > 4*s):
        return [a,c,d,-1]
    if(math.fabs(d) > c or math.fabs(d) > ((4*s) -c)):
        return [a,c,d,-1]
    if(a < 0.0 or a > max_vi):
        return [a,c,d,-1]
    #1 indicates sucess, -1 failure (in case you want to do something about that...)
    return [a,c,d,1]


def target_quasi_explicit(p, i, y, vs, tot_var):
    """

    :param p: [c, d, a]
    :param i:
    :param y:
    :param vs:
    :param tot_var:
    :return:
    """
    c, d, a = p
    elem = (a + d * y + c * np.sqrt(y**2 + 1))
    err = (elem / tot_var) - 1
    # err = (elem - tot_var)
    return err


def target_ms(p, x, vs, i, opt):
    """

    :param p: [m, s]
    :param x:  log forward moneyness
    :param vs:
    :param i:
    :return:
    """
    m, s = p
    y = (x - m) / s
    tot_var = vs['quote'][i]**2 * vs['T'][i]
    opt_method = 1
    if opt_method == 1:
        # use levmar
        v_max = tot_var.max()
        bc = [(0, 4*s), (None, None), (0, v_max)]
        p_init = [s, 0, v_max/2]  # (c, d, a)
        args = (i, y, vs, tot_var)
        case = 0
        if case == 0:
            C = np.matrix([[1, -1, 0],
                           [1, 1, 0],
                           [-1, -1, 0],
                           [-1, 1, 0]], dtype=float)
            d = np.matrix([0, 0, -4*s, -4*s]).T
            lic = (C, d)
            # opts = {'eps1':1e-15, 'eps2':1e-15, 'eps3': 1e-15}
            opts = {}
            res = levmar.levmar_blic(target_quasi_explicit, p_init, [0]*y.size, bc, lic,
                                     args=args, cdiff=True, **opts)
        else:
            #TODO: check this
            res = levmar.levmar_bc(target_quasi_explicit, p_init, tot_var, bc, args=args,
                                   cdiff=True)
        # print res
        c, d, a = res[0]
    else:
        vi = tot_var.argmax()
        # a, c, d, res = optimize(y, vi, vols, t, s, np.ones_like(vols))
    err = ((a + d * y + c * np.sqrt(y*y + 1)) - tot_var)/ tot_var
    if opt:
        return err
    else:
        return err, res


def get_vs_str(tenor, strike, quote):
    assert quote.shape == (tenor.size, strike.size)
    fp = cStringIO.StringIO()
    print >>fp, '\t'.join(['Expiry/Strike'] + [str(d) for d in strike])
    for i, td in enumerate(tenor):
        print >>fp, '\t'.join(['%sD' % td] + ['%.20f' % d for d in quote[i]])
    data = fp.getvalue().strip()
    return data


def get_raw_svi_param(t, m, s, c, d, a):
    a = a.copy()/ t
    r = d / c
    b = c / s / t
    return (a, b, m, s, r)


def main(filename):
    vs = read_vs_from_txt(filename)
    dw_vs = read_vs_from_txt(filename.replace('_SD_', '_DW_'))
    nt, ns = vs['quote'].shape
    ks = vs['strike']

    svi_quote = np.zeros_like(vs['quote'])

    # SD forward
    forward = np.array([100.6, 100.73, 101.01, 99.84, 100.55, 99.75, 99.81], dtype=float) / 100
    print(forward)

    for i, td in enumerate(vs['tenor']):
        vol = vs['quote'][i]
        tot_var = vol**2 * vs['T'][i]
        x = np.log(ks / forward[i])
        # x = np.log(ks)
        p0 = [0, 0.2]   # m, s
        p0[0] = x[tot_var.argmin()]
        p0[1] = 0.3 if i == 0 else res[0][1]
        p0[0] = p0[0] if i == 0 else res[0][0]
        # print p0
        bc = [(None, None), (1e-3, None)]   # m, s
        argc = (x, vs, i, 1)
        opts = {'eps1':1e-15, 'eps2':1e-15, 'eps3': 1e-15}
        opts = {}
        res = levmar.levmar_bc(target_ms, p0, np.zeros(x.size), bc, argc, cdiff=True, **opts)
        # print res
        m, s = res[0]
        err, res_quasi = target_ms(res[0], x, vs, i, 0)
        c, d, a = res_quasi[0]
        # print m, s, c, d, a
        y = (x - m) / s
        var = (a + d * y + c * np.sqrt(y**2 + 1)) / vs['T'][i]
        raw_param = get_raw_svi_param(vs['T'][i], m, s, c, d, a)
        rmse = np.sqrt((err**2).mean())
        print('rmse[T=%sD]\t%s\t%s' % (td, rmse, '\t'.join([str(d) for d in raw_param])))
        svi_vol = np.sqrt(var)
        svi_quote[i, :] = svi_vol
        if 0:
            st = np.log(ks)
            # st = ks
            plt.subplot(211)
            plt.plot(st, vol, label='impvs')
            plt.plot(st, dw_vs['quote'][i], label='impvs_dw')
            plt.plot(st, svi_vol, label='svi')
            plt.grid()
            plt.title('T=%sD' % vs['tenor'][i])
            plt.legend(loc='best')
            plt.subplot(212)
            plt.plot(st, dw_vs['quote'][i] - vol, label='dw diff')
            plt.plot(st, svi_vol - vol, label='svi diff')
            plt.grid()
            plt.legend(loc='best')
            plt.show()
        # break
    sd_vs_str = get_vs_str(vs['tenor'], vs['strike'], vs['quote'])
    svi_vs_str = get_vs_str(vs['tenor'], vs['strike'], svi_quote)
    if 1:
        print()
        print('SD VolSurface')
        print(sd_vs_str)
        print()
        print('SVI VolSurface')
        print(svi_vs_str)


if __name__ == '__main__':
    plt.rcParams['figure.figsize'] = [8, 6]
    filename = 'vs/IVSKS_SD_2016-01-13.txt'
    main(filename)

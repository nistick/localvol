#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright 2016  <@NISTICK-PC>
#
# Distributed under terms of the MIT license.

"""
SVI JW Parametrization
"""


class SVIJW:
    def __init__(self, v, psi, p, c, varmin, exp):
        self.v = v
        self.psi = psi
        self.p = p
        self.c = c
        self.varmin = varmin
        self.exp = exp

    def __str__(self):
        return '%f\t%f\t%f\t%f\t%f\t%f' % (self.v, self.psi, self.p, self.c, self.varmin, self.exp)

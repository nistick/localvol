#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright 2016  <@NISTICK-PC>
#
# Distributed under terms of the MIT license.

"""
SVI Raw Parametrization
"""


class SVI:
    def __init__(self, a, b, sig, rho, m):
        self.a = a
        self.b = b
        self.sig = sig
        self.rho = rho
        self.m = m

    def __str__(self):
        return '%f\t%f\t%f\t%f\t%f' % (self.a, self.b, self.sig, self.rho, self.m)

    def toJW(self, t):




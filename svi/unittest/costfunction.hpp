#pragma once
#include <ql/quantlib.hpp>

#include "../svi/raw_parameter.hpp"

using namespace QuantLib;

class OneDimensionalPolynomialDegreeN : public CostFunction {
public:
	OneDimensionalPolynomialDegreeN(const RawSVIParameter& svi)
		: svi_(svi) {}

	Real value(const Array& x) const {
		QL_REQUIRE(x.size() == 1, "independent variable must be 1 dimensional");
		Real y = 0;
		y = svi_.distributionFunction(x[0]);
		return y;
	}

	Disposable<Array> values(const Array& x) const {
		QL_REQUIRE(x.size() == 1, "independent variable must be 1 dimensional");
		Array y(1);
		y[0] = value(x);
		return y;
	}

private:
	const RawSVIParameter svi_;
};
#pragma warning(disable: 4996)

#include <ql/quantlib.hpp>
#include "costfunction.hpp"



using namespace QuantLib;

int main() {
	RawSVIParameter vogtParams(-0.0410, 0.1331, 0.3060, 0.3586, 0.4153);

	double eps = 1e-8;
	int maxIter = 10000;

	OneDimensionalPolynomialDegreeN sviDist(vogtParams);
	BoundaryConstraint boxConstrate(-1.5, 1.5);
	const Array initialGuess(1, 0.0);
	Problem myProblem(sviDist, boxConstrate, initialGuess);
	EndCriteria endCriteria(1000, 100, 1e-5, 1e-5, 1e-5);
	ConjugateGradient optimizationMethod;
	EndCriteria::Type endCriteriaResult = optimizationMethod.minimize(myProblem, endCriteria);

	bool completed = false;
	switch (endCriteriaResult) {
	case EndCriteria::MaxIterations:
	case EndCriteria::Unknown:
	case EndCriteria::None:
		completed = false;
	default:
		completed = true;
	}

	if (completed)
	{
		Array xMinCalculated = myProblem.currentValue();
		Array yMinCalculated = myProblem.values(xMinCalculated);

		cout << "X : " << xMinCalculated << endl;
		cout << "Y : " << yMinCalculated << endl;
	}



	

	return 0;
}


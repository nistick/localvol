#pragma once
#include <cmath>
#include <iostream>
#include "jw_svi.hpp"

using namespace std;

class RawSVIParameter {
public:
	RawSVIParameter(double a,
		double b,
		double rho,
		double m,
		double sigma);

	double totalImpliedVariance(double strike) const;
	double volFirst(double strike) const;
	double volSecond(double strike) const;
	double distributionFunction(double strike) const;

	JW_SVI jwSVI(double time);

	double a() const;
	double b() const;
	double m() const;
	double rho() const;
	double sigma() const;

	void setA(double value);
	void setB(double value);
	void setM(double value);
	void setRho(double value);
	void setSigma(double value);

private:
	double a_;
	double b_;
	double rho_;
	double m_;
	double sigma_;

};

void plotSmile(ostream& os, const RawSVIParameter& svi);
void functionG(ostream& os, const RawSVIParameter& svi);
ostream& operator<<(ostream& os, const RawSVIParameter& jw);
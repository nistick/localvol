#include "raw_parameter.hpp"

RawSVIParameter::RawSVIParameter(double a, double b,
	double rho, double m, double sigma) {
	a_ = a;
	b_ = b;
	rho_ = rho;
	m_ = m;
	sigma_ = sigma;
}

double RawSVIParameter::totalImpliedVariance(double strike) const
{
	double ret;

	ret = a_ + b_ * (rho_*(strike - m_) + 
		std::sqrt(std::pow(strike - m_, 2) + std::pow(sigma_, 2)));

	return ret;
}

double RawSVIParameter::volFirst(double strike) const {
	double ret;
	
	ret = b_ * rho_ + b_*(strike - m_) / 
		std::sqrt(std::pow(strike - m_, 2) + std::pow(sigma_, 2));

	return ret;
}
double RawSVIParameter::volSecond(double strike) const
{
	double ret;
	
	ret = b_*std::pow(sigma_, 2) / 
		std::pow(std::sqrt(std::pow(strike - m_, 2) + std::pow(sigma_, 2)), 3);

	return ret;
}


double RawSVIParameter::distributionFunction(double strike) const {
	double ret;
	double term1 = std::pow(1.0 - strike * this->volFirst(strike) / 2 / totalImpliedVariance(strike), 2);
	double term2 = std::pow(volFirst(strike), 2) / 4 * (1 / totalImpliedVariance(strike) + 1 / 4);
	double term3 = volSecond(strike) / 2;

	ret = term1 - term2 + term3;

	double w = this->totalImpliedVariance(strike);
	double dw = this->volFirst(strike);
	double d2w = this->volSecond(strike);
	double discr = std::sqrt(std::pow(strike - m_, 2) + std::pow(sigma_, 2));
	cout << "w" << w << endl;
	cout << "dw" << dw << endl;
	cout << "d2w" << d2w << endl;
	cout << "discr" << discr << endl;
	cout << "g" << (1 - strike*dw / w + dw*dw / 4 * (-1 / w + strike*strike / (w*w) - 4) + d2w / 2) << endl;

	return ret;
}

JW_SVI RawSVIParameter::jwSVI(double time) {
	double v_t = (a_ + b_*(-rho_*m_ + sqrt(pow(m_, 2) + pow(sigma_, 2)))) / time;
	double w_t = v_t * time;
	double psi_t = 1. / sqrt(w_t)*b_ / 2.0*(-m_ / sqrt(pow(m_, 2) + pow(sigma_, 2)) + rho_);
	double p_t = b_*(1 - rho_) / sqrt(w_t);
	double c_t = b_*(1 + rho_) / sqrt(w_t);
	double v_tilta_t = 1.0 / time*(a_ + b_*sigma_*sqrt(1 - pow(rho_, 2)));
	JW_SVI ret(v_t, psi_t, p_t, c_t, v_tilta_t, time);
	return ret;
}

double RawSVIParameter::a() const { return a_; }
double RawSVIParameter::b() const {
	return b_;
}
double RawSVIParameter::m() const { return m_; }
double RawSVIParameter::rho() const { return rho_; }
double RawSVIParameter::sigma() const { return sigma_; }
void RawSVIParameter::setA(double value) { a_ = value; }
void RawSVIParameter::setB(double value) { b_ = value; }
void RawSVIParameter::setM(double value) { m_ = value; }
void RawSVIParameter::setRho(double value) { rho_ = value; }
void RawSVIParameter::setSigma(double value) { sigma_ = value; }
void plotSmile(ostream& os, const RawSVIParameter& svi)
{
	for (int i = 0; i < 300; i++) {
		double tt = -1.5 + i / 100.0;
		os << svi.totalImpliedVariance(tt);
		if (i == 299) {
			os << std::endl;
		}
		else {
			os << ",";
		}
	}
}

void functionG(ostream& os, const RawSVIParameter& svi)
{
	for (int i = 0; i < 300; i++) {
		double tt = -1.5 + i / 100.0;
		os << svi.distributionFunction(tt);
		if (i == 299) {
			os << std::endl;
		}
		else {
			os << ",";
		}
	}
}

ostream& operator<<(ostream& os, const RawSVIParameter& jw)
{
	os << "a\tb\tm\trho\tsigma" << endl;
	os << jw.a() << "\t" << jw.b() << "\t" << jw.m() << "\t" << jw.rho() << "\t" << jw.sigma() << endl;
	return os;
}
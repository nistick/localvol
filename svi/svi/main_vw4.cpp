#pragma warning(disable:4996)
#include <iostream>
#include <fstream>

#include <ql/quantlib.hpp>
#include "raw_parameter.hpp"
#include "jw_svi.hpp"
#include "natural_parameter.hpp"
#include "types.hpp"
#include "linspace.hpp"
#include "blackprice.hpp"
#include "svi_cost_function.hpp"

using namespace std;

int main(int argc, char** argv) {
	try {
		RawSVIParameter vogtParams(-0.0410, 0.1331, 0.3060, 0.3586, 0.4153);
		double a;
		std::ofstream ofOptionPrice("option_price.txt");
		std::ofstream ofSmile("smile.csv");
		std::ofstream offuncG("g.csv");

		

		JW_SVI vogtJW = vogtParams.jwSVI(1.0);
		JW_SVI vogtFixedJW = vogtJW;
		vogtFixedJW.setC(vogtJW.p() + 2 * vogtJW.psi());
		vogtFixedJW.setV_Tilde(vogtJW.v() * 4 * vogtJW.p()*vogtFixedJW.c() / pow(vogtJW.p() + vogtFixedJW.c(), 2));

		RawSVIParameter vogtFixedParams = vogtFixedJW.toRawSVI();

		cout << vogtJW << endl;
		cout << vogtFixedJW << endl;
		cout << vogtFixedParams << endl;

		Array strikes = linspace(-1, 1, 21);
		Array callVals(strikes.size());

		cout << strikes << endl;

		for (size_t i = 0; i < strikes.size(); i++)
			callVals[i] = BSFormula(1.0, std::exp(strikes[i]), 1.0, 0, std::sqrt(vogtParams.totalImpliedVariance(strikes[i])));

		SVIFunction sviCostFunction(vogtJW, strikes, callVals);
		double upperC = std::max(vogtJW.c(), vogtFixedJW.c());
		double lowerC = std::min(vogtJW.c(), vogtFixedJW.c());
		double upperV = std::max(vogtJW.v_tilde(), vogtFixedJW.v_tilde());
		double lowerV = std::min(vogtJW.v_tilde(), vogtFixedJW.v_tilde());
		Array upperBound(2);
		Array lowerBound(2);
		upperBound[0] = std::max(vogtJW.c(), vogtFixedJW.c());
		upperBound[1] = std::max(vogtJW.v_tilde(), vogtFixedJW.v_tilde());
		lowerBound[0] = std::min(vogtJW.c(), vogtFixedJW.c());
		lowerBound[1] = std::min(vogtJW.v_tilde(), vogtFixedJW.v_tilde());

		QuantLib::NonhomogeneousBoundaryConstraint constrate(lowerBound, upperBound);
		Array initialGuess(2);
		//initialGuess[0] = 0.9;
		initialGuess[0] = 1.316798;
		initialGuess[1] = 0.011624903235477872;

		cout << "Initial Guess : " << initialGuess << endl;

		QuantLib::Problem myProblem(sviCostFunction, constrate, initialGuess);

		Array cts = linspace(0.3493158, 1.316798, 1000);
		Array vtts = linspace(0.011624903235477872, 0.01548182, 1000);

		/*for each (double var in cts)
		{
			initialGuess[0] = var;
			ofSmile << myProblem.value(initialGuess) << ",";
		}*/

		for each (double var in vtts)
		{
			initialGuess[1] = var;
			ofSmile << myProblem.value(initialGuess) << ",";
		}
		ofSmile << endl;

		

		ofSmile.close();
		offuncG.close();
		ofOptionPrice.close();
	}
	catch (QuantLib::Error ex) {
		cerr << ex.what() << endl;
	}
	catch (std::exception ex) {
		cerr << ex.what() << endl;
	}
	return 0;
}
#ifndef blackprice_hpp
#define blackprice_hpp

#include <cmath>
#include "types.hpp"
#include <ql/quantlib.hpp>



//BSFormula <-function(S0, K, T, r, sigma)
//{
//	x <-log(S0 / K) + r*T;
//	sig <-sigma*sqrt(T);
//	d1 <-x / sig + sig / 2;
//	d2 <-d1 - sig;
//	pv <-exp(-r*T);
//	return(S0*pnorm(d1) - pv*K*pnorm(d2));
//}

/*
* K : strike
* F : forward
* v : sigma * sqrt(t)
* w : 1(call), -1(put)
* return black option price
*/
inline Array price_black(Array strike,
	double forward,
	Array stdDev,
	int w = 1) {
	Array FK = forward / strike;
	Array logFK(strike.size());
	std::transform(FK.begin(), FK.end(), logFK.begin(), (double(*)(double))log);
	Array d1 = logFK / stdDev + 0.5 * stdDev;
	Array d2 = d1 - stdDev;
	Array cum_d1(strike.size());
	Array cum_d2(strike.size());
	QuantLib::CumulativeNormalDistribution cdf;
	d1 = w * d1;
	d2 = w * d2;
	std::transform(d1.begin(), d1.end(), cum_d1.begin(), cdf);
	std::transform(d2.begin(), d2.end(), cum_d2.begin(), cdf);
	return w * (forward * cum_d1 - strike * cum_d2);
}

inline double BSFormula(double S0, double K, double T, double r, double sigma) {
	QuantLib::CumulativeNormalDistribution cdf;
	double x = std::log(S0 / K) + r*T;
	double sig = sigma*std::sqrt(T);
	double d1 = x / sig + sig / 2.0;
	double d2 = d1 - sig;
	double pv = std::exp(-r*T);
	
	return S0*cdf(d1) - pv*K*cdf(d2);
}


#endif

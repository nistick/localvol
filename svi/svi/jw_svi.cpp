#include "jw_svi.hpp"
#include "raw_parameter.hpp"

JW_SVI::JW_SVI(double v,
	double psi,
	double p,
	double c,
	double v_tilde,
	double t)
{
	v_ = v;
	psi_ = psi;
	p_ = p;
	c_ = c;
	v_tilde_ = v_tilde;
	t_ = t;
}

double JW_SVI::choosingC() {
	double ret;

	ret = p_ + 2 * psi_;

	return ret;
}
double JW_SVI::choosingV_tilda() {
	double ret;
	ret = v_ * 4 * p_*choosingC() / std::pow(p_ + choosingC(), 2);
	return ret;
}

double JW_SVI::v() const { return v_; }
double JW_SVI::psi() const { return psi_; }
double JW_SVI::p() const { return p_; }
double JW_SVI::c() const { return c_; }
double JW_SVI::v_tilde() const { return v_tilde_; }
double JW_SVI::t() const { return t_; }

void JW_SVI::setV(double value) { v_ = value; }
void JW_SVI::setPsi(double value) { psi_ = value; }
void JW_SVI::setP(double value) { p_ = value; }
void JW_SVI::setC(double value) { c_ = value; }
void JW_SVI::setV_Tilde(double value) { v_tilde_ = value; }
void JW_SVI::setT(double value) { t_ = value; }

RawSVIParameter JW_SVI::toRawSVI() const
{
	double w_t = v_ * t_;
	double b = sqrt(w_t) / 2.0*(c_ + p_);
	double rho = 1 - (p_*sqrt(w_t)) / b;
	double beta = rho - 2 * psi_*sqrt(w_t) / b;
	double alpha = boost::math::sign(beta)*sqrt(1 / pow(beta, 2) - 1);
	double m = (v_ - v_tilde_)*t_ / (b*(-rho + boost::math::sign(alpha)*sqrt(1 + pow(alpha, 2)) - alpha*sqrt(1 - pow(rho, 2))));
	double sigma = alpha * m;
	double a = v_tilde_*t_ - b*sigma*sqrt(1 - pow(rho, 2));
	RawSVIParameter ret(a, b, rho, m, sigma);
	return ret;
}

void plotSmile(ostream& os, const JW_SVI& jw)
{
	RawSVIParameter tempParams = jw.toRawSVI();
	for (int i = 0; i < 300; i++) {
		double tt = -1.5 + i / 100.0;
		os << tempParams.totalImpliedVariance(tt);
		if (i == 299) {
			os << std::endl;
		}
		else {
			os << ",";
		}

	}
}

ostream& operator<<(ostream& os, const JW_SVI& jw)
{
	//os << "v\tpsi\tp\tc\tv_tilde\tt" << endl;
	os <<setprecision(20)<< jw.v() << "\t" << jw.psi() << "\t" << jw.p() << "\t" << jw.c() << "\t" << jw.v_tilde() << "\t" << jw.t() << endl;
	return os;
}
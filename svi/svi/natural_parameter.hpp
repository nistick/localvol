#pragma once

#include <cmath>

class NaturalParameter {
public:
	NaturalParameter(double delta,
		double mu,
		double rho,
		double omega,
		double zeta);

	double totalImpliedVariance(double strike);

private:
	double delta_;
	double mu_;
	double rho_;
	double omega_;
	double zeta_;
};
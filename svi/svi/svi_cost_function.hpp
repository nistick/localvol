#pragma once
#include <algorithm>  
#include <vector>
#include <functional>
#include "types.hpp"
#include "jw_svi.hpp"
#include "raw_parameter.hpp"
#include "blackprice.hpp"
#include "linspace.hpp"
#include "costfunction.hpp"

class SVIFunction : public QuantLib::CostFunction {
public:
	SVIFunction(){}
	SVIFunction(
		JW_SVI jw_svi,
		Array strikes,
		Array callVals
	);

	Real value(const Array& x) const;
	virtual Real finiteDifferenceEpsilon() const;
	QuantLib::Disposable<Array> values(const Array&x) const;
private:
	JW_SVI jw_svi_;
	Array strikes_;
	Array callVals_;
};


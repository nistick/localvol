#pragma warning(disable:4996)
#include <iostream>
#include <fstream>

#include <ql/quantlib.hpp>
#include "raw_parameter.hpp"
#include "jw_svi.hpp"
#include "natural_parameter.hpp"
#include "types.hpp"
#include "linspace.hpp"
#include "blackprice.hpp"
#include "svi_cost_function.hpp"

using namespace std;

int main(int argc, char** argv) {
	try {
		RawSVIParameter vogtParams(-0.0410, 0.1331, 0.3060, 0.3586, 0.4153);
		double a;
		std::ofstream ofOptionPrice("option_price.txt");
		std::ofstream ofSmile("smile.csv");
		std::ofstream offuncG("g.csv");

		plotSmile(ofSmile, vogtParams);
		functionG(offuncG, vogtParams);

		JW_SVI vogtJW = vogtParams.jwSVI(1.0);
		JW_SVI vogtFixedJW = vogtJW;
		vogtFixedJW.setC(vogtJW.p() + 2 * vogtJW.psi());
		vogtFixedJW.setV_Tilde(vogtJW.v() * 4 * vogtJW.p()*vogtFixedJW.c() / pow(vogtJW.p() + vogtFixedJW.c(), 2));

		RawSVIParameter vogtFixedParams = vogtFixedJW.toRawSVI();

		cout << vogtJW << endl;
		cout << vogtFixedJW << endl;
		cout << vogtFixedParams << endl;

		Array strikes = linspace(-1, 1, 21);
		Array callVals(strikes.size());

		cout << strikes << endl;

		for (size_t i = 0; i < strikes.size(); i++)
			callVals[i] = BSFormula(1.0, std::exp(strikes[i]), 1.0, 0, std::sqrt(vogtParams.totalImpliedVariance(strikes[i])));
		
		ofSmile << callVals << endl;

		SVIFunction sviCostFunction(vogtJW, strikes, callVals);
		double upperC = std::max(vogtJW.c(), vogtFixedJW.c());
		double lowerC = std::min(vogtJW.c(), vogtFixedJW.c());
		double upperV = std::max(vogtJW.v_tilde(), vogtFixedJW.v_tilde());
		double lowerV = std::min(vogtJW.v_tilde(), vogtFixedJW.v_tilde());
		Array upperBound(2);
		Array lowerBound(2);
		upperBound[0] = std::max(vogtJW.c(), vogtFixedJW.c());
		upperBound[1] = std::max(vogtJW.v_tilde(), vogtFixedJW.v_tilde());
		lowerBound[0] = std::min(vogtJW.c(), vogtFixedJW.c());
		lowerBound[1] = std::min(vogtJW.v_tilde(), vogtFixedJW.v_tilde());

		QuantLib::NonhomogeneousBoundaryConstraint constrate(lowerBound, upperBound);
		Array initialGuess(2);
		//initialGuess[0] = 0.9;
		initialGuess[0] = vogtJW.c() - 0.02;
		initialGuess[1] = vogtFixedJW.v_tilde();

		cout << "Initial Guess : " << initialGuess << endl;

		Array tempInitial(2);
		tempInitial[0] = 0.8570644;
		tempInitial[1] = 0.0116249;
		

		QuantLib::Problem myProblem(sviCostFunction, constrate, initialGuess);

		//cout << myProblem.values(tempInitial) << endl;
		//cin >> a;

		QuantLib::EndCriteria endCriteria(10000, 100, 1e-8, 1e-8, 1e-8);

	/*	QuantLib::SimulatedAnnealing<> sa(0.2, 0.25, 0.0001, 1, QuantLib::MersenneTwisterUniformRng(42));
		QuantLib::EndCriteria::Type result = sa.minimize(myProblem, endCriteria);
		initialGuess = myProblem.currentValue();
		cout << initialGuess << endl;
		
		cin >> a;*/

		QuantLib::EndCriteria ec(1000, 100, 1e-7, 1e-7, 1e-7);
		QuantLib::Problem P(sviCostFunction, constrate, initialGuess);


		QuantLib::BFGS optimizationMethod;
		//QuantLib::ConjugateGradient optimizationMethod;
		QuantLib::EndCriteria::Type endCriteriaResult = optimizationMethod.minimize(P, ec);

		cout << endCriteriaResult << endl;

		bool completed = false;
		switch (endCriteriaResult) {
		case QuantLib::EndCriteria::MaxIterations:
		case QuantLib::EndCriteria::Unknown:
		case QuantLib::EndCriteria::None:
			completed = false;
		default:
			completed = true;
		}
		JW_SVI vogtOptimalJW(vogtJW);
		if (completed)
		{
			Array xMinCalculated = P.currentValue();
			Array yMinCalculated = P.values(xMinCalculated);

			cout << "x" << xMinCalculated << endl;
			cout << "y" << yMinCalculated << endl;

			vogtOptimalJW.setC(xMinCalculated[0]);
			vogtOptimalJW.setV_Tilde(xMinCalculated[1]);
			cout << vogtOptimalJW << endl;
		}
		RawSVIParameter vogtOptimalParams = vogtOptimalJW.toRawSVI();
		cout << vogtOptimalParams << endl;

		plotSmile(ofSmile, vogtFixedParams);
		plotSmile(ofSmile, vogtOptimalParams);

		functionG(offuncG, vogtFixedParams);
		functionG(offuncG, vogtOptimalParams);

		ofSmile.close();
		offuncG.close();
		ofOptionPrice.close();
	}
	catch (QuantLib::Error ex) {
		cerr << ex.what() << endl;
	}
	catch (std::exception ex) {
		cerr << ex.what() << endl;
	}
	return 0;
}
#pragma warning(disable:4996)
#include <iostream>
#include <fstream>

#include <ql/quantlib.hpp>
#include "raw_parameter.hpp"

using namespace std;

int main(int argc, char** argv) {
	RawSVIParameter aa(-0.0410, 0.1331, 0.3586, 0.3060, 0.4153);
	std::ofstream outfile("output.txt");	

	for (int i = 0; i < 300; i++) {
		double tt = -1.5 + i / 100.0;
		outfile << tt << "\t" << aa.totalImpliedVariance(tt) << std::endl;
		//std::cout << tt << "\t" << aa.totalImpliedVariance(tt) << std::endl;
	}
	outfile.close();

	std::ofstream distFile("dist.txt");
	for (int i = 0; i < 300; i++) {
		double tt = -1.5 + i / 100.0;
		distFile << tt << "\t" << aa.distributionFunction(tt) << std::endl;
	}
	distFile.close();
	return 0;
}
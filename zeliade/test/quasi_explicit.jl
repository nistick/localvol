include("../quasi_explicit_svi.jl")
include("../database/database.jl")
using Memento
using Pandas
using QuasiExplicit
using ELSDBA
try
    rm("logginglogging.log")
catch

end
logger = getlogger(current_module())
push!(logger, DefaultHandler("logginglogging.log"))
setlevel!(logger, "debug")
und = "HSCEI"
asOfDate = Date(2018, 3, 20)
wrapper = ELSDBA.db
spot_und = spot(wrapper.conn, und, asOfDate)
vs = volatilitySurface(wrapper.conn, und, asOfDate)
vs_pt = pivot(vs, index="MATURITYDATE", columns="MONEYNESS", values="VOLATILITY")
idx = index(vs_pt)[3]
vc = dropna(loc(vs_pt)[idx])
ts = forwardTermstructure(wrapper.conn, und, asOfDate)

strikes = strike_grid(und, spot_und)
strikes = values(index(vc))
info("strikes : $strikes")
vol_curve = loc(vs_pt)[idx]
info("$vol_curve")
variance = vol_curve.^2
F = loc(ts)[idx]["VALUE"]
info("Forward : $F")
xs = log.(strikes/F)
info("variance : $variance")
info("xs : $xs")
mIdx = indmin(vol_curve)
mInit = xs[mIdx]
info("mInit : $mInit")
τ = (Date(idx)- asOfDate).value/365.0
info("τ : $τ")
debug(logger, "start calibrations")
# reducedProblem(mInit, 0.02, variance, xs, τ)
# calibrationJuMP(mInit, 0.02, variance, xs, τ)
calibrationLBFGSB(mInit, 0.02, variance, xs, τ)
# @enter calibrationLBFGSB(mInit, 0.02, variance, xs, τ)
# status, optX, optVal = reducedProblem(mInit, 0.02, variance, xs, τ)

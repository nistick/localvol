using Base

module ELSDBA
    export spot, volatilitySurface, forwardTermstructure,
        strike_grid

    using Pandas
    using PyCall
    using JSON

    filename = "zeliade/code_map.json"
    jcode_map = JSON.parsefile(filename)
    code_map = jcode_map["code_map"]
    code_map_mk = jcode_map["code_map_mk"]
    bsh_db_code_map = jcode_map["bsh_db_code_map"]

    @pyimport cx_Oracle
    id = "ELSDBA"
    pwd = "dwsea#01"
    host = "DS76E_DIRECT"
    info_table = "FENT_BLP_OPT_INFO"
    quote_table = "FENT_BLP_OPT_QUOTE"


    struct DBWrapper
        id::String
        pwd::String
        host::String
        conn
        cur
    end

    function DBWrapper(id, pwd, host)
        conn = cx_Oracle.connect(id, pwd, host)
        cur = conn[:cursor]()
        DBWrapper(id, pwd, host, conn, cur)
    end

    # function volatilitySurface(db, date, code)
    db = DBWrapper(id, pwd, host)

    function read_option_info(und_ticker)
        sql = "select * from fent_blp_opt_info where opt_undl_ticker='$und_ticker'"
        data = read_sql(sql, db.conn)
    end

    function spot(conn, und, date)
        table = "SD_TERMSTRUCTURE"
        code = code_map_mk[und]
        sql = "select maturitydate, value as F from $table where code='$code' and nalja='$date' and type = 'Spot' order by maturitydate asc"
        df = read_sql(sql, conn)
        df["F"][1]
    end

    function spot(conn, und, date::Date)
        date = Dates.format(date, "yyyy-mm-dd")
        spot(conn, und, date)
    end

    function volatilitySurface(conn, und, date)
        table = "FENT_EQ_VOLSURFACE"
        code = bsh_db_code_map[und]
        date = Dates.format(date, "yyyy-mm-dd")
        sql = "select * from $table where code='$code' and nalja='$date'"
        info(sql)
        read_sql(sql, conn)
    end

    function forwardTermstructure(conn, und, date)
        table = "FENT_EQ_TERMSTRUCTURE"
        date = Dates.format(date, "yyyy-mm-dd")
        code = bsh_db_code_map[und]
        sql = "select MATURITYDATE, VALUE from $table where code = '$code' and nalja = '$date' and type = 'ForwardPrice'"
        info(sql)
        read_sql(sql, conn; index_col="MATURITYDATE")
    end


    function strike_grid(und, spot_und; is_fixed=false)
        if is_fixed
            linspace(0.5, 1.5, 21) * spot
        else
            dk_map = Dict("KOSPI2"=> 15.0, "NKY"=>750.0, "HSCEI"=>600.0, "SPX"=>100.0, "SX5E"=>150.0, "DAX"=>500.0, "HSI"=>1100.0)
            dK = dk_map[und]
            baseS = floor(Int64,spot_und/dK) * dK
            target_ks = [baseS + (dK*i) for i in range(-10, 21) if baseS + (dK * i) >= 0.5*spot_und && baseS + (dK * i) <= 1.5*spot_und]
            target_ks
        end
    end
end


# using ELSDBA

# dw_wrapper = ELSDBA.DBWrapper(ELSDBA.id, ELSDBA.pwd, ELSDBA.host)
# println(ELSDBA.spot(dw_wrapper.conn, "HSCEI", "2018-03-20"))

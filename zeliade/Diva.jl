module Diva

using HTTP
using JSON
using Pandas

export ref, spot, hist


function ref(securities, fields)
    secstr = join(securities, ",")
    fiestr = join(fields, ",")
    res = HTTP.get("http://172.21.35.174/api/ref?securities=$secstr")
    JSON.parse(String(res.body))
end

function hist(securities, fields, startDate, endDate)
    info("securities : $securities")
    info("fields : $fields")
    secstr = join(securities, ",")
    fiestr = join(fields, ",")
    info("http://172.21.35.174/api/hist?securities=$secstr&fields=$fiestr&startDate=$startDate&endDate=$endDate")
    res = HTTP.get("http://172.21.35.174/api/hist?securities=$secstr&fields=$fiestr&startDate=$startDate&endDate=$endDate")
    jsonData = JSON.parse(String(res.body))
    ret = Dict()
    for item in jsonData
        df = DataFrame(item["fieldData"])
        security = String(item["security"])
        ret[security] = df
    end
    ret
end

function spot(index, date)
    lindex = [index]
    ret = hist(lindex, ["PX_LAST"], date, date)
    ret[index]["PX_LAST"][1]
end

end

# using JSON
# using Diva
# # @pretty println(ref(["HSCEI Index"], ["PX_LAST"]))
# # print(ref(["HSCEI Index"], ["PX_LAST"]))
# # print(Diva.hist(["HSCEI Index"], ["PX_LAST"], "20180320", "20180328")["HSCEI Index"])
# print(Diva.spot("HSCEI Index", "20180320"))
# # print(@enter Diva.spot("HSCEI Index", "20180320"))
# # df = DataFrame(ref(["HSCEI Index"], ["PX_LAST"]))
# # print(df)

module QuasiExplicit
    include("svi.jl")
    using Convex
    using LBFGSB
    using Clp
    using Memento

    import Convex:Variable

    export QE_SVI, rawSVI, reducedProblem, calibration, calibrationJuMP,
        calibrationLBFGSB, v̂

    struct QE_SVI
        m::Float64
        σ::Float64
        c::Float64
        d::Float64
        â::Float64
    end

    Base.show(io::IO, svi::QE_SVI) = println(io, @sprintf("m : %5.5f\tσ : %5.5f\tc : %5.5f\td : %5.5f\tâ : %5.5f", svi.m, svi.σ, svi.c, svi.d, svi.â))

    """
        v̂(y) = â + + dy + c√(y²+1)
    Compute total variance
    """
    v̂(svi::QE_SVI, y) = svi.â + svi.d*y + svi.c*sqrt(y.^2 + 1)

    function QE_SVI(source, τ::Float64)
        a = source.a
        b = source.b
        m = source.m
        σ = source.σ
        ρ = source.ρ
        QE_SVI(m, σ, b*σ*τ, ρ*b*σ*τ, a*τ)
    end

    function rawSVI(source::QE_SVI, τ::Float64)
        m = source.m
        σ = source.σ
        c = source.c
        d = source.d
        â = source.â
        SVI(â/τ, c/(σ*τ), m, σ, d/(c/(σ*τ)*σ*τ))
    end

    """
        reducedProblem(m::Float64, σ::Float64, variance, xs)

    Optimization Function with variance and log forward strike
    """
    function reducedProblem(m::Float64, σ::Float64, variance, xs, τ)
        logger = getlogger(current_module())
        y = (xs - m)/σ
        A = hcat(ones(y), y, sqrt.(y.^2+1))
        x = Variable(3)     # x[1] = â, x[2] = d, x[3] = c
        totalVariance = values(variance*τ)
        problem = minimize(sumsquares(A*x-totalVariance),)
        problem.constraints += [abs(x[2])<= x[3];abs(x[2]) <= 4σ-x[3];0.0<=x[1];x[2]<max(totalVariance...); 0.0<=x[3]; x[3]<=4*σ;]
        solve!(problem)
        info(logger, "$problem")
        qe_svi = QE_SVI(m, σ, x.value[3], x.value[2], x.value[1])
    return qe_svi    end

    function calibrationJuMP(m_::Float64, σ_::Float64, variance, xs, τ)
        # model = Model(solver = ClpSolver())
        # totalVariance = variance*τ
        # function costFunction(xs, m, σ)
        #     qe_svi = reducedProblem(m, σ, variance, xs, τ)
        #     y = (xs - m)/σ
        #     ret = qe_svi.â + qe_svi.d.*y + qe_svi.c*sqrt.(y.^2 +1) - totalVariance
        #     sum(ret.^2)
        # end
        # @variable(model, m)
        # @variable(model, 0.0 <= σ)
        # @objective(model, Min, costFunction(cx, getvalue(m), getvalue(σ)))
        # status = solve(model)
        # println("Objective value : ", getobjectivevalue(model))
        # println("m : ", getvalue(m))
        # println("σ : ", getvalue(σ))
    end

    function calibrationLBFGSB(μ::Float64, σ::Float64, variance, xs, τ)
        logger = getlogger(current_module())
        info(logger, "μ : $μ")
        info(logger, "σ : $σ")
        nmax = 1024
        mmax = 17
        task = fill(Cuchar(' '), 60)    # fortran's blank padding
        csave = fill(Cuchar(' '), 60)   # fortran's blank padding
        lsave = zeros(Bool, 4)
        isave = zeros(Cint, 44)
        dsave = zeros(Cdouble, 29)
        tlimit = 20.0
        iprint = Ref{Cint}(-1)
        f = 0.0
        fRef = Ref{Cdouble}(f)
        g = zeros(Cdouble, nmax)
        wa = zeros(Cdouble, 2mmax*nmax+5nmax+11mmax*mmax+8mmax)
        iwa = zeros(Cint, 3*nmax)
        factr = Ref{Cdouble}(0.0)
        pgtol = Ref{Cdouble}(0.0)
        n = 2
        m = 10
        nRef = Ref{Cint}(n)
        mRef = Ref{Cint}(n)
        nbd = zeros(Cint, nmax)
        l = zeros(Cdouble, nmax)
        u = zeros(Cdouble, nmax)

        nbd[1] = 0
        nbd[2] = 1
        l[2] = 1e-10

        x = zeros(Cdouble, nmax) # m, σ
        x[1] = μ
        x[2] = σ

        println(logger, "solving Problem")

        task[1:5] = b"START"

        time1Ref = Ref{Cdouble}(0)
        time2Ref = Ref{Cdouble}(0)

        timer(time1Ref)

        let
            @label CALLLBFGSB
            setulb(nRef, mRef, x, l, u, nbd, fRef, g, factr, pgtol, wa, iwa, task, iprint, csave, lsave, isave, dsave)
            info(logger, String(task))
            if task[1:2] == b"FG"
                f = fRef[]
                timer(time2Ref)
                if time2Ref[] - time1Ref[] > tlimit
                    task[1:4] = b"STOP"
                    # "In this driver we have chosen to disable the printing options of the
                    #  code (we set iprint=-1); instead we are using customized output: we
                    #  print the latest value of x, the corresponding function value f and
                    #  the norm of the projected gradient |proj g|."

                    println("STOP: CPU EXCEEDING THE TIME LIMIT.")

                    # "We print the latest iterate contained in wa(j+1:j+n), where j = 3*n+2*m*n+11*m**2"
                    j = 3n + 2m*n + 11m^2
                    println("Latest iterate X =")
                    for i = j+1:j+n
                        println("wa[$i] = ", wa[i])
                    end
                else
                    info("x[1] : m : $(x[1])")
                    info("x[2] : σ : $(x[2])")
                    svi = reducedProblem(x[1], x[2], variance, xs, τ)
                    info(logger, "$svi")
                    info(logger, "Compute Function value of f")
                    # "Compute function value f for the sample problem."
                    totalVariance = variance*τ
                    f = 0.0
                    y = (xs-x[1])/x[2]
                    function costFunction(â, d, c, xs, m, σ)
                        y = (xs - m)/σ
                        ret = â + d.*y + c*sqrt.(y.^2 +1) - totalVariance
                        sum(ret.^2)
                    end
                    f = costFunction(svi.â, svi.d, svi.c, xs, x[1], x[2])
                    info("Compute Gradient g for the sample Problem")
                    # "Compute gradient g for the sample problem."
                    info(logger, "value : $f")
                    # ϵ = 1e-15
                    # m_up = costFunction(svi.â, svi.d, svi.c, xs, (1.0+ϵ)x[1], x[2])
                    # m_do = costFunction(svi.â, svi.d, svi.c, xs, (1.0-ϵ)x[1], x[2])
                    # σ_up = costFunction(svi.â, svi.d, svi.c, xs, x[1], (1.0+ϵ)x[2])
                    # σ_do = costFunction(svi.â, svi.d, svi.c, xs, x[1], (1.0-ϵ)x[2])
                    # # info("m_up : $m_up")
                    # # info("m_do : $m_do")
                    # # info("σ_up : $σ_up")
                    # # info("σ_do : $σ_do")
                    # g[1] = (m_up - m_do) / (2ϵ*x[1])
                    # g[2] = (σ_up - σ_do) / (2ϵ*x[2])
                    df = svi.â+svi.d .*y +svi.c.*sqrt.(y.^2+1)-totalVariance
                    dfdy = svi.d+svi.c ./(2sqrt(y.^2+1))
                    dydm = (-1./svi.σ)
                    dyds = (-y./svi.σ)
                    info("df : $df")
                    info("dfdy :  $dfdy")
                    info("dydm : $dydm")
                    info("dyds : $dyds")
                    info("df*dfdy : $(df.*dfdy)")
                    g[1] = sum(2*(svi.â+svi.d .*y +svi.c.*sqrt.(y.^2+1)-totalVariance).*(svi.d+svi.c ./(2sqrt(y.^2+1))).*(-1./svi.σ))
                    g[2] = sum(2*(svi.â+svi.d .*y +svi.c.*sqrt.(y.^2+1)-totalVariance).*(svi.d+svi.c ./(2sqrt(y.^2+1))).*(-y./svi.σ))
                    info(logger, "g[1] : $(g[1])")
                    info(logger, "g[2] : $(g[2])")
                    fRef[] = f
                end
                    # "go back to the minimization routine."
                    @goto CALLLBFGSB
            end

            # "the minimization routine has returned with a new iterate, The time limit
            #  has not been reached, and we test whether the following two stopping tests are satisfied:
            if task[1:5] == b"NEW_X"
                f = fRef[]

                # "1) Terminate if the total number of f and g evaluations exceeds 900."
                isave[34] ≥ 900 && (task[1:4] = b"STOP"; terminate_info="TOTAL NO. of f AND g EVALUATIONS EXCEEDS LIMIT")

                # "2) Terminate if  |proj g|/(1+|f|) < 1.0d-10"
                dsave[13] ≤ 1e-10 * (1e0 + abs(f)) && (task[1:4] = b"STOP"; terminate_info="THE PROJECTED GRADIENT IS SUFFICIENTLY SMALL")


                # "We wish to print the following information at each iteration:"
                # "1) the current iteration number, isave(30),"
                # "2) the total number of f and g evaluations, isave(34),"
                # "3) the value of the objective function f,"
                # "4) the norm of the projected gradient,  dsave(13)"

                println("Iterate ", isave[30], "  nfg = ", isave[34], "  f = ", f, "  |proj g| = ", dsave[13])

                # "If the run is to be terminated, we print also the information contained
                #  in task as well as the final value of x."
                if task[1:4] == b"STOP"
                    println(terminate_info)
                    for i = 1:n
                        println("x($i) = ", x[i])
                    end
                end

                # "go back to the minimization routine."
                @goto CALLLBFGSB
            end

            # ---------- the end of the loop -------------
            # "If task is neither FG nor NEW_X we terminate execution."
        end

    end



    function solve_quadratic_problem(p, x, impvs, τ)

    end

    function target(m::Float64, σ::Float64, x, impvs, τ)

    end


end

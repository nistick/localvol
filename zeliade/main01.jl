using Pandas
# using JuMP
# using Clp
using Convex
using PyPlot
import Convex: Variable


strikes = linspace(50, 150, 21)
vols = [36.304648,	34.389024,	32.377184,	30.263562,	27.886101,	25.760669,	23.619387,	22.063395,	20.371331,	18.903685,	17.653946,	16.540907,	15.983452,	15.742859,	15.733416,	15.85407,	16.098564,	16.438753,	16.857478,	17.285207,	17.703775,]
vols ./= 100.0
impliedVol = DataFrame(Dict(:strikes=>strikes, :volatility=>vols))
# print(impliedVol)

n1 = 21
n2 = 3
T = 1.5

m = 0.001
σ = 0.2
F = 100.0
X = log.(strikes/F)
y = (X-m)/σ
# println(sqrt(y.^2+1))
# Matrix(ones(vols), y, sqrt(y.^2+1))
A = hcat(ones(vols), y, sqrt.(y.^2+1))
function v̂(y, â, d, c)
    return â + d * y + c * sqrt.(y^2 +1)
end

x = Variable(n2)
problem = minimize(sumsquares(A*x-vols.^2*T), x[1]>0)
problem.constraints += abs(x[2]) <= x[3]
problem.constraints += abs(x[2]) <= 4.0 * σ -x[3]
problem.constraints += 0.0000001 < x[1]
problem.constraints += x[1] <= max(vols.^2...)

solve!(problem)
a = x.value
println(a[2] < a[3])
println(a[2] < (4 * σ-a[3]))
println(a[1] <max(vols.^2...))
println(0.00000001 <= a[1])


plot(X, vols.^2*T)jjj
plot(X, v̂.(y, a[1], a[2], a[3]))


3e0
1.e12

include("Diva.jl")

include("database.jl")
using ArgParse
using Diva
using ELSDBA

import ELSDBA: spot

und_code_map = Dict(0=>"KOSPI2", 2=>"NKY", 3=>"HSCEI", 4=>"HSI", 5=>"SX5E", 6=>"SPX")

function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table s begin
        "--opt1"
            help = "an option with an argument"
        "--date", "-d"
            help = "evaluation date"
            arg_type = String
        "--flag1"
            help = "an option without argument, i.e. a flag"
            action = :store_true
        "arg1"
            help = "a positional argument"
            required = true
        end

    return parse_args(s)
end

function main()
    parse_args = parse_commandline()
    info("Parsed args!")
    for (arg, val) in parse_args
        println("    $arg => $val")
    end

    asOfDate = Date("2018-")
    info(asOfDate)
    ELSDBA.db_code_map[und]


end

# main()
include("database/database.jl")
using ELSDBA
using Pandas
# import ELSDBA: spot, volatilitySurface
asOfDate = Date(2018, 3,20)
und = "KOSPI2"
und = "HSCEI"
und_db = ELSDBA.code_map_mk[und]
wrapper = ELSDBA.db
spot_und = spot(wrapper.conn, und, asOfDate)
vs = volatilitySurface(wrapper.conn, und, asOfDate)
vs_pt = pivot(vs, index="MATURITYDATE", columns="MONEYNESS", values="VOLATILITY")
ts = forwardTermstructure(wrapper.conn, und, asOfDate)
ts["FWD"] = values(ts[:VALUE]) ./ spot_und .* 100
strikes = @enter strike_grid(und, spot_und)
vs_svi = DataFrame(index=index(vs_pt), columns=Index(strikes, name="Moneyness"))
svi_params = DataFrame(index=index(vs_pt), columns=Index(["a", "b", "m", "σ", "ρ"], name="Moneyness"))

for t in index(vs_pt)
    iDate = Date(t)
    info(typeof(iDate))
    τ = (iDate - asOfDate).value/365.0
    vc = dropna(loc(vs_pt)[t])
    F = loc(ts)[t]["VALUE"]
    info("Forward $F")
    xs = log(values(index(vc))/F)
    info("log forward strike $xs")

    options = Dict(:flag_atm_fit_yn=>false,
                   :weight_method=>0,
                   :weight=>ones(length(vc)),
                   :a_min=>0.0015)
    idx_mask = [true for x in vc]
    info(idx_mask)

end

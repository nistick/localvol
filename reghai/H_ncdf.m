function out = H_ncdf(x)
    out = (1.0+erf(x/sqrt(2.0)))/2.0;
end


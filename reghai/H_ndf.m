function out = H_ndf(x)
    out = 1.0/sqrt(2*pi)*exp(-0.5*x*x);
end


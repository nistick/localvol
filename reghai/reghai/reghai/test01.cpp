#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <vector>

using namespace std;
using namespace boost;

int main(int argc, char** argv) {



	string str1("hello abc-*-ABC-*-aBc goodbye");

	typedef vector< iterator_range<string::iterator> > find_vector_type;

	find_vector_type FindVec; // #1: Search for separators
	ifind_all(FindVec, str1, "abc"); // FindVec == { [abc],[ABC],[aBc] }

	typedef vector< string > split_vector_type;

	split_vector_type SplitVec; // #2: Search for tokens
	split(SplitVec, str1, is_any_of("-*"), token_compress_on); // SplitVec == { "hello abc","ABC","aBc goodbye" }

}
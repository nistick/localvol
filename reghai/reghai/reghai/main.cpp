#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <vector>

using namespace std;

int main(int argc, char** argv) {
	using boost::property_tree::ptree;
	using std::string;

	ptree props;
	boost::property_tree::read_json("Data\\hscei_20170202.json", props);
	string repoRateCurve = props.get<string>("repoRateCurve");
	
	cout << repoRateCurve << endl;

	vector<string> repoRateCurveV;
	boost::split(repoRateCurveV, repoRateCurve, boost::is_any_of("\n"));
	for each (string str in repoRateCurveV)
	{
		cout << str << endl;
	}
	return 0;
}
function v = H_interpolation(x,y,u,interpolOrder)
    
    if nargin == 4
        interpolation_order = interpolOrder;
    else
        interpolation_order = 0;
    end
    
    n = length(x);
    k = 1;
    
    switch interpolation_order
        case 0 % piecewixe constant interpolation
            if u <= x(1)
                v = y(1);  % constant extrapolation
            elseif u > x(n)
                v= y(n); %constant extrapolation
            else    
                for j = 2:n
                    if u <= x(j)
                        k=j;
                        break;
                    end
                end
                v = y(k);
            end
        case 1 % piecewise linear interpolation
            if u <= x(1)
                v = y(1);  % constant extrapolation
            elseif u > x(n)
                v= y(n); %constant extrapolation
            else
                delta = diff(y)./diff(x);
                for j = 2:n
                     if u <= x(j)
                        k=j;
                        break;
                    end
                end
                % Evaluate interpolant
                s = u - x(k-1);
                v = y(k-1) + s.*delta(k-1);
            end
        otherwise
            disp('only works for piecewise constant and linear  cases!!')
    end
    
            
end


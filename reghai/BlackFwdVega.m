function y = BlackFwdVega(F,K,vol,T)

% Bisection algorithm
% PutCall = "P"ut or "C"all
% F = Forward price
% K = Strike price
% vol = volatility
% T = Maturity

    d1= (log(F/K)+(vol^2/2.0)*T)/(vol*sqrt(T));
    
    y = sqrt(T)*H_ndf(d1);

    
end


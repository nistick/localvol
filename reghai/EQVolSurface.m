classdef EQVolSurface < MktData
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetAccess = public)
        strikes;
        maturities;
        vol;
    end
    
    methods
        % constructor
        function eqVolSurface = EQVolSurface(MktData)
            if nargin > 0
                volSurfaceData = MktData.params('rawData');
                pmaturities = volSurfaceData(2:size(volSurfaceData,1),1);
                pstrikes  = volSurfaceData(1,2:size(volSurfaceData,2))';
                volSurfaceR = volSurfaceData(2:size(volSurfaceData,1),2:size(volSurfaceData,2));
                eqVolSurface.maturities = pmaturities;
                eqVolSurface.strikes    = pstrikes;
                eqVolSurface.vol = @(t,K) interp2(pmaturities,pstrikes,volSurfaceR',t,K);
                eqVolSurface.params = MktData.params;
                
            end     
        end
        
            
    end
    
end



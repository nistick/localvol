# -*- coding: utf-8 -*-

"""
Fixed Pointer Calibration
"""
from log_conf import Logger
from container import Container
import math
from volatilityinterpolation import interpolateTargetStrikeVolFPI


logger = Logger.logger

def fixedPointIter(targetfunc, tvar, tol, maxIter, optParams):
    n = 0
    # Initial ErrorBound make it big enough
    errorTry = 1000.0 
    tvarOld = tvar
    while (errorTry > tol) and (n < maxIter):
        out = targetfunc(tvar, optParams)
        tvarOld = tvar
        #  % reduce implied vol error
        if optParams.params.targetType == 1:
            errorTry = out.volErrorTotal
        else:
            errorTry = out.priceReTotal
        tvar = out.newLocalVol
        n = n+1
        logger.debug('{0}th challenge'.format(n))
        logger.debug('Error is {0}'.format(errorTry))
    x = tvar
    finalOut = targetfunc(tvarOld, optParams)
    fval = finalOut.volErrorTotal
    marketImpVol = finalOut.marketImpVol
    modelImpVol  = finalOut.modelImpVol
    interpBlackPrices = finalOut.interpBlackPrices
    interpOTMPrices = finalOut.interpOTMPrices
    volError     = finalOut.impVolError
    volErrorTotal = finalOut.volErrorTotal
    priceRe      = finalOut.priceRe
    priceReTotal = finalOut.priceReTotal
    nIter = n

    return x, fval, marketImpVol, modelImpVol, interpBlackPrices, interpOTMPrices, volError, volErrorTotal, priceRe, priceReTotal, nIter




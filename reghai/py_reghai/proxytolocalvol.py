# -*- coding: utf-8 -*-

"""

"""
import numpy as np
from log_conf import Logger

logger = Logger.logger

def GetLocalVolFromProxy(volProxy, volProxyX, returnX):
    """
    Linear Interpolation Volatility Proxy Surface
    """
    # adjusted volProxyX
    x = list()
    for i in volProxyX.tolist():
        prevX = 0.0
        for j in returnX:
            if i < j:
                x.append(prevX)
                break
            else:
                prevX = j
    x = np.array(x)
    logger.debug('VOL Proxy')
    logger.debug(volProxy)
    logger.debug('Vol Proxy X')
    logger.debug(x)
    return np.interp(returnX, x, volProxy)

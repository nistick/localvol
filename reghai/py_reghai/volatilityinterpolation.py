# -*- coding: utf-8 -*-

"""

"""
from container import Container
import numpy as np
from vollib import black
from vollib.black.greeks import analytical
from vollib.black.implied_volatility import implied_volatility_of_undiscounted_option_price
from log_conf import Logger

logger = Logger.logger


def interpolateTargetStrikeVolFPI(idxNow, meshProb, params):
    """
    function of Interpolation of Implied volatility surface with mesh probability and strike
    """
    logger.debug('mesh Probability')
    logger.debug(meshProb)
    strikes = params.marketStrikes
    out = Container()
    out.vols = np.zeros(strikes.size)
    out.interpOTMPrices = np.zeros(strikes.size)
    out.interpBlackPrices = np.zeros(strikes.size)
    
    meshC = np.array([np.maximum.reduce([x-strikes, np.zeros(strikes.size)]) for x in params.Ks])
    meshP = np.array([np.maximum.reduce([strikes-x, np.zeros(strikes.size)]) for x in params.Ks])
    
    cValue = np.matmul(meshProb, meshC)
    pValue = np.matmul(meshProb, meshP)
    
    # use jackel's Lets Be Rational 
    logger.debug('Put Value')
    logger.debug(pValue)
    logger.debug('Call Value')
    logger.debug(cValue)
    logger.debug('Maturity')
    logger.debug(params.params.expiry[idxNow])
    
    for i in range(strikes.size):
        K = strikes[i]
        if K <= 1.0:
            out.vols[i] = implied_volatility_of_undiscounted_option_price(pValue[i],1,K,params.params.expiry[idxNow]/365.0,'p')
            out.interpOTMPrices[i] = pValue[i]
            out.interpBlackPrices[i] = black.undiscounted_black(1.0,K,out.vols[i],params.params.expiry[idxNow]/365.0,'p')
        else:
            out.vols[i] = implied_volatility_of_undiscounted_option_price(cValue[i], 1, K, params.params.expiry[idxNow]/365.0, 'c')
            out.interpOTMPrices[i] = cValue[i]
            out.interpBlackPrices[i] = black.undiscounted_black(1.0,K,out.vols[i], params.params.expiry[idxNow]/365.0,'c')
    return out

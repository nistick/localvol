# -*- coding: utf-8 -*-

"""
main scripts
"""

import json
from EQBlack import EQBlack
import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
from log_conf import Logger

logger = Logger.logger
np.set_printoptions(linewidth=8000, threshold=10000)


if __name__ == '__main__':
    marketData = dict()

    logger.info('load data json file')
    with open('hscei_20170202.json', 'r') as fp:
        hsceiData = json.load(fp)
    logger.info(hsceiData.keys())
    # Evaluation Date
    evaluationDate = pd.Timestamp(hsceiData['evaluationDate'])
    logger.info(evaluationDate)
    marketData['evaluationDate'] = evaluationDate

    # Dividend Curve
    series = hsceiData['dividendCurve'].split('\n')
    dates = [evaluationDate +
             relativedelta(days=int(d.split()[0])) for d in series]
    values = [float(d.split()[1]) for d in series]
    dividendCurve = pd.Series(values, dates)
    marketData['dividendCurve'] = dividendCurve

    # Repo Rate Curve
    series_repo = hsceiData['repoRateCurve'].split('\n')
    dates_repo = [evaluationDate +
                  relativedelta(days=int(d.split()[0])) for d in series_repo]
    values_repo = [float(d.split()[1]) for d in series_repo]
    repoCurve = pd.Series(values_repo, dates_repo)
    marketData['repoRateCurve'] = repoCurve

    # ZeroCurve
    series_zero = hsceiData['zeroCurve'].split('\n')
    times_zero = [float(d.split()[0]) for d in series_zero]
    values_zero = [float(d.split()[1]) for d in series_zero]
    zeroCurve = pd.Series(values_zero, times_zero)
    marketData['zeroCurve'] = zeroCurve

    # Volatility Surface
    volDict = hsceiData['volatilitySurface']
    expiry = np.array([float(d) for d in volDict['expiry'].split('\t')])
    strikes = np.array([float(d) for d in volDict['strike'].split('\t')])
    volData = volDict['data'].split('\n')
    volData = [[float(x) for x in d.split('\t')] for d in volData]
    volData = np.matrix(volData)
    volDict = dict()
    volDict['data'] = volData
    volDict['expiry'] = expiry
    volDict['strike'] = strikes
    marketData['volatilitySurface'] = volDict
    logger.info(volDict)

    # Spot
    marketData['spot'] = hsceiData['spot']

    modelParam = dict()
    modelParam = hsceiData['modelParam']

    eqBlack = EQBlack(marketData, modelParam)
    ret = eqBlack.calibration()
    logger.info(ret)

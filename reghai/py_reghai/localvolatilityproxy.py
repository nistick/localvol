# -*- coding: utf-8 -*-

from log_conf import Logger
import numpy as np
from container import Container
import math
from fixedpointeriteration import fixedPointIter
from volatilityinterpolation import interpolateTargetStrikeVolFPI
from solve1dPDE import Solve1DGF

logger = Logger.logger


def FindVolProxy(eqModel, idxNow, dT, lastProb, params, targetFunction):
    logger.debug('Call Function FindvolProxy')
    # %initialize calibration parameters
    optParams = Container()
    optParams.idxNow = idxNow
    optParams.dT = dT
    optParams.params = params
    optParams.ipos = np.zeros(params.strike.size)
    optParams.Ks = np.zeros(params.Ns)
    optParams.Ks = params.Ks
    optParams.meshSize = params.Ns
    optParams.meshProb = np.zeros(params.Ns)
    optParams.proxy = np.zeros(params.Ns)
    optParams.dK = params.Ks[2] - params.Ks[1]
    optParams.marketStrikes = np.zeros(params.strike.size)
    optParams.blackOTMPrices = params.blackOTMPrices
    optParams.callCurve = np.zeros(params.strike.size)
    optParams.putCurve = np.zeros(params.strike.size)
    if eqModel.modelParam['moneyNessType'] == 'spotMoneyNess':
        # % market strike points in fwd PDE grid
        forwardFactor = eqModel.forwardFactor(params.expiry[idxNow])
        for i in range(params.strike.size):
            optParams.marketStrikes[i] = params.strike[i]/forwardFactor
    else:
        for i in range(params.strike.size):
            optParams.marketStrikes[i] = params.strike[i]

    # % positions to separate vol proxy regimes
    for i in range(params.strike.size):
        #  add default 1
        optParams.ipos[i] = math.floor((optParams.marketStrikes[i] - optParams.Ks[1])/optParams.dK)
        optParams.lastProb = lastProb

    # exception for one month maturity for restricted interval
    lowerCutoff = params.lowerCutoffVector[idxNow]
    upperCutoff = params.upperCutoffVector[idxNow]
    numOfCutoffTenor = params.numOfCutoffTenor
    lowerIdx = 1
    upperIdx = params.strike.size
    if idxNow <= numOfCutoffTenor:
        marketStrikes = np.array(optParams.marketStrikes)
        lowerIdx = np.where(marketStrikes >= lowerCutoff)[0].min()
        upperIdx = np.where(marketStrikes <= upperCutoff)[0].max()
    optParams.lowerIdx = lowerIdx
    optParams.upperIdx = upperIdx
    tvar = np.zeros(params.strike.size)

    for i in range(params.strike.size):
        tvar[i] = params.blackVol[idxNow, i]
    for i in range(lowerIdx-1):
        tvar[i] = tvar[lowerIdx]
    for i in range(upperIdx+1, params.strike.size):
        tvar[i] = tvar[upperIdx]
    targetfunc = targetFunction
    tol = 0.0001
    maxIter = 1000

    ret = fixedPointIter(targetfunc, tvar, tol, maxIter, optParams)
    x = ret[0]
    fval = ret[1]
    marketImpVol = ret[2]
    modelImpVol = ret[3]
    interpBlackPrices = ret[4]
    interpOTMPrices = ret[5]
    volError = ret[6]
    volErrorTotal = ret[7]
    priceRe = ret[8]
    priceReTotal = ret[9]
    nIter = ret[10]

    bootStrapOut = Container()
    bootStrapOut.volProxy = np.zeros(params.strike.size)
    bootStrapOut.ipos = np.zeros(params.strike.size)
    for i in range(params.strike.size):
        bootStrapOut.volProxy[i] = x[i]
        bootStrapOut.ipos[i] = optParams.ipos[i]
    #  % for later use
    bootStrapOut.marketStrikes = optParams.marketStrikes
    bootStrapOut.volError = volError
    bootStrapOut.marketImpVol = marketImpVol
    bootStrapOut.modelImpVol = modelImpVol
    bootStrapOut.interpBlackPrices = interpBlackPrices
    bootStrapOut.dupireOTMPrices = interpOTMPrices
    bootStrapOut.nIter = nIter

    out = Solve1DGF(bootStrapOut.volProxy, dT, optParams.dK, lastProb, optParams)
    bootStrapOut.lastProb = out.newProb
    bootStrapOut.condProb = out.condProb
    return bootStrapOut


def TargetFunctionVolProxyFPI(tvar, optParams):
    """
    Target function for FixedPointIteration Optimizer
    """
    expiry = optParams.params.expiry
    out = Container()
    idxNow = optParams.idxNow
    if idxNow != 0:
        mkt_dT = expiry[idxNow] - expiry[idxNow-1]
    else:
        mkt_dT = optParams.params.expiry[0]
    mkt_dT = mkt_dT/365.0

    # Solve  Forward PDE(GF) One time
    outGF = Solve1DGF(tvar, mkt_dT, optParams.dK, optParams.lastProb, optParams)

    newProb = outGF.newProb
    optParams.meshProb = newProb
    # PDE(GF) Solve End
    # find the implied vol of dupire model
    target = interpolateTargetStrikeVolFPI(idxNow, newProb, optParams)

    marketImpVol = optParams.params.blackVol
    logger.debug('Input Black Volatility')
    logger.debug(marketImpVol)
    logger.debug('Model Volatility')
    logger.debug(target.vols)
    logger.debug('Volatility Ratio')
    logger.debug(marketImpVol[idxNow, :]/target.vols)
    out.newLocalVol = tvar * marketImpVol[idxNow, :]/target.vols
    out.localVolError = out.newLocalVol - tvar
    out.modelImpVol = target.vols
    out.marketImpVol = marketImpVol[idxNow, :]
    logger.debug('Market Implied Volatility')
    logger.debug(out.marketImpVol)
    out.impVolError = out.modelImpVol - out.marketImpVol
    logger.debug('Implied Volatility Error')
    logger.debug(out.impVolError)
    out.interpOTMPrices = target.interpOTMPrices
    out.interpBlackPrices = target.interpBlackPrices
    out.priceRe = out.interpOTMPrices/optParams.params.blackOTMPrices[idxNow, :] - 1.0

    #  % for shorter tenor we only fit for the restricted interval
    if idxNow <= optParams.params.numOfCutoffTenor:
        for i in range(optParams.lowerIdx-1):
            out.newLocalVol[i] = out.newLocalVol(optParams.lowerIdx)

        for i in range(optParams.upperIdx+1, optParams.marketStrikes.size):
            out.newLocalVol[i] = out.newLocalVol(optParams.upperIdx)

    out.volErrorTotal = 0.0
    out.priceReTotal = 0.0
    #  %for one month we only fit betwee lower and upper
    dummyN = 0
    if idxNow <= optParams.params.numOfCutoffTenor:
        for i in range(optParams.lowerIdx, optParams.upperIdx+1):
            out.volErrorTotal = out.volErrorTotal + out.impVolError[i]*out.impVolError[i]
            out.priceReTotal = out.priceReTotal + out.priceRe[i]*out.priceRe[i]
            dummyN = dummyN + 1
    else:
        for i in out.impVolError:
            out.volErrorTotal = out.volErrorTotal + i*i
        for i in out.priceRe:
            out.priceReTotal = out.priceReTotal + i*i
            dummyN = dummyN + 1
    logger.debug('Volatility Error Total')
    logger.debug(out.volErrorTotal)
    logger.debug('dummy N')
    logger.debug(dummyN)
    out.volErrorTotal = math.sqrt(out.volErrorTotal/dummyN)
    out.priceReTotal = math.sqrt(out.priceReTotal/dummyN)
    return out

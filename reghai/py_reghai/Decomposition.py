# -*- coding: utf-8 -*-

"""
Matrix Decomposition Methods
"""
import numpy as np


def HugeDecomp(n, a, b, c):
    x = np.zeros(n)
    y = np.zeros(n)
    d = np.zeros(n)
    x[0] = 1.0
    x[1] = 1.0

    for i in range(2, n):
        x[i] = x[i-1] - (a[i-1]/b[i-1])*(c[i-2]/b[i-2])*x[i-2]

    #  %bwd
    y[-1] = 1.0/(x[-1]-(a[-1]/b[-1])*(c[-2]/b[-2])*x[-2])
    y[-2] = y[-1]

    for i in range(n-3, 0, -1):
        y[i] = y[i+1] - (a[i+2]/b[i+2])*(c[i+1]/b[i+1])*y[i+2]

    #  %set d

    d[0] = x[0]*y[0]/b[0]
    for i in range(1, n):
        d[i] = -(a[i]/b[i])*(y[i]/y[i-1])*d[i-1] + x[i]*y[i]/b[i]
    return x, y, d

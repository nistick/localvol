classdef EQDupireSpotGF < EQModel
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        localVolSurface
        modelSchedule
    end
    
    methods
        function eqDupireSpotGF = EQDupireSpotGF(EQModel)
            if nargin > 0
                eqDupireSpotGF.zeroCurve = EQModel.zeroCurve;
                eqDupireSpotGF.dividendCurve = EQModel.dividendCurve;
                eqDupireSpotGF.repoRateCurve = EQModel.repoRateCurve;
                eqDupireSpotGF.spot = EQModel.spot;
                eqDupireSpotGF.mktData =  EQModel.mktData;
                eqDupireSpotGF.modelParams = EQModel.modelParams;
                eqDupireSpotGF.modelSchedule = [];
            end
        end
        
        function out= expmReghai(eqDupireSpot,A,t)
            % Scale A by power of 2 so that its norm is < 1/2 .
            A = A*t;
            [f,e] = log2(norm(A,'inf'));
            s = max(0,e+1);
            A = A/2^s;
            Aexp = eye(size(A,1)) + A;
            for i=1:s
                Aexp = Aexp*Aexp;
            end
            out = Aexp;
        end
        
        function path= bbgenerator(eqDupireSpot,rn)
            %generate brownian bridge 
            
            [Npaths,Nsteps]=size(rn);
            path=zeros(Npaths,Nsteps);
            map= zeros(1,Nsteps);
            bridgeindex = zeros(1,Nsteps);
            leftindex = zeros(1,Nsteps);
            rightindex = zeros(1,Nsteps);
            sigma = zeros(1,Nsteps);
            leftweight = zeros(1,Nsteps);
            rightweight = zeros(1,Nsteps);
            
            map(Nsteps)=1;
            bridgeindex(1)=Nsteps;
            sigma(1)=sqrt(Nsteps);
            leftweight(1)=1; %////////////////
            rightweight(1)=1; %////////////////
            j=1;
            for i=2:Nsteps    
                while map(j) %////////////////////
                    j=j+1;
                end
                k=j;
                while ~map(k) % //////////////
                    k=k+1;
                end
                l=j+fix((k-1-j)/2);
                map(l)=i;
                bridgeindex(i)=l;
                leftindex(i)=j;
                rightindex(i)=k;
                leftweight(i)  = (k-l)/(k+1-j);
                rightweight(i) = (l+1-j)/(k+1-j);
                sigma(i) = sqrt(((l+1-j)*(k-l))/(k+1-j));
                j=k+1;
                if j>=Nsteps+1
                    j=1;
                end
            end

            path(:,Nsteps)=sigma(1)*rn(:,1);
            for i=2:Nsteps
                j=leftindex(i);
                k=rightindex(i);
                l=bridgeindex(i);
                if j~=1
                    path(:,l)=leftweight(i)*path(:,j-1) + rightweight(i)*path(:,k) + sigma(i)*rn(:,i);
                else
                    path(:,l) = rightweight(i)*path(:,k) + sigma(i)*rn(:,i);
                end
            end
        end
        
        function out = TDMASolve(eqDupireSpotGF,tdma,vector)
            size = tdma.size;
            out = vector;
            
            for i=2:size
                out(i) = out(i) - tdma.ll(i)*out(i-1);
            end
            
            out(size) = out(size)/tdma.dd(size);
            
            for i=size-1:-1:1
                out(i) = (out(i) - out(i+1)*tdma.uu(i))/tdma.dd(i);
            end
            
        end
        
        function out = LUDecompose(eqDupireSpotGF,tdma)
            out = tdma;
            if out.bupdated == 1
                for i=1:out.size
                    out.uu(i) = out.c(i);
                end
                
                out.dd(1) = out.b(1);
                for i=2:out.size
                    out.ll(i) = out.a(i)/out.dd(i-1);
                    out.dd(i) = out.b(i) - out.c(i-1)*out.ll(i);
                end
                
                out.bupdated = 0;
            end
        
        end
        
        function [ x,y,d] = HugeDecomp(eqDupireSpotGF,n,a,b,c)
    
            x = zeros(n,1);
            y = zeros(n,1);
            d = zeros(n,1);
            %fwd

            x(1) = 1.0;
            x(2) = 1.0;

            for i=3:n
                x(i) = x(i-1) - (a(i-1)/b(i-1))*(c(i-2)/b(i-2))*x(i-2);
            end

            %bwd
            y(n) = 1.0/(x(n)-(a(n)/b(n))*(c(n-1)/b(n-1))*x(n-1));
            y(n-1) = y(n);

            for i=n-2:-1:1
                y(i) = y(i+1) - (a(i+2)/b(i+2))*(c(i+1)/b(i+1))*y(i+2);
            end

            %set d

            d(1) = x(1)*y(1)/b(1);
            for i=2:n
                d(i) = -(a(i)/b(i))*(y(i)/y(i-1))*d(i-1) + x(i)*y(i)/b(i);
            end


        end

        
        function localVol = GetLocalVolFromProxy(eqDupireSpotGF,volProxy,Ks,ipos)

            localVol = zeros(length(Ks),1);
            
            for i=1: length(volProxy)
%                 Extrapolation at the left and right
                if i == 1
                    for j=1:ipos(i)-1
                        localVol(j) = volProxy(i);
                    end
                elseif i == length(volProxy)
                    for j=ipos(i-1):ipos(i)-1
                        localVol(j) = (Ks(j)-Ks(ipos(i-1)))*(volProxy(i)-volProxy(i-1))/(Ks(ipos(i))-Ks(ipos(i-1))) + volProxy(i-1);
                    end
                    for j=ipos(i):length(Ks)
                        localVol(j) = volProxy(i);
                    end
%                  linear interpolation in the spot grid in-between    
                else
                    for j=ipos(i-1):ipos(i)-1
                        localVol(j) = (Ks(j)-Ks(ipos(i-1)))*(volProxy(i)-volProxy(i-1))/(Ks(ipos(i))-Ks(ipos(i-1))) + volProxy(i-1);
                    end
                end
            end
            
        end
                        
        
        function [newC newP] = Solve1DPDE(eqDupireSpotGF,tvar,dT,dK,lastC,lastP,optParams)
            
            %filling proxy grid vol
            proxy = zeros(length(lastC),1);
            proxy = eqDupireSpotGF.GetLocalVolFromProxy(tvar,optParams.Ks,optParams.ipos);

            %filling end
            
            meshC = lastC;
            meshP = lastP;
            
            tdma.size = length(meshC);
            tdma.bupdated = 0;
            tdma.a = zeros(tdma.size,1);
            tdma.b = zeros(tdma.size,1);
            tdma.c = zeros(tdma.size,1);
            tdma.uu = zeros(tdma.size,1);
            tdma.ll = zeros(tdma.size,1);
            tdma.dd = zeros(tdma.size,1);
            
            dK2 = dK*dK;
            for i=2:tdma.size-1
                tdma.a(i) = -0.5*dT*proxy(i)*proxy(i)*optParams.Ks(i)*optParams.Ks(i)/dK2;
                tdma.b(i) = 1 + dT*proxy(i)*proxy(i)*optParams.Ks(i)*optParams.Ks(i)/dK2;
                tdma.c(i) = -0.5*dT*proxy(i)*proxy(i)*optParams.Ks(i)*optParams.Ks(i)/dK2;
            end
            
            tdma.a(1) = 0;
            tdma.a(tdma.size) = 0;
            tdma.c(1) = 0;
            tdma.c(tdma.size) = 0;
            tdma.b(1) = 1;
            tdma.b(tdma.size) = 1;
            tdma.bupdated = 1;
            
            tdmaLU =  eqDupireSpotGF.LUDecompose(tdma);
            pmeshC = eqDupireSpotGF.TDMASolve(tdmaLU,meshC);
            newC = pmeshC;
            
            pmeshP = eqDupireSpotGF.TDMASolve(tdmaLU,meshP);
            newP = pmeshP;
        end
        
        function out = Solve1DGF(eqDupireSpotGF,tvar,dT,dK,lastProb,optParams)
            
            %filling proxy grid vol
            proxy = zeros(length(lastProb),1);
            proxy = eqDupireSpotGF.GetLocalVolFromProxy(tvar,optParams.Ks,optParams.ipos);

            %filling end
            
            meshProb = lastProb;
            
            size = length(meshProb);
            a = zeros(size,1);
            b = zeros(size,1);
            c = zeros(size,1);
            b_rg = zeros(size,1);
            
            dK2 = dK*dK;
            for i=1:size
                a(i) = -0.5*dT*proxy(i)*proxy(i)*optParams.Ks(i)*optParams.Ks(i)/dK2;
                b(i) = 1 + dT*proxy(i)*proxy(i)*optParams.Ks(i)*optParams.Ks(i)/dK2;
                b_rg(i) = dT*proxy(i)*proxy(i)*optParams.Ks(i)*optParams.Ks(i)/dK2;
                c(i) = -0.5*dT*proxy(i)*proxy(i)*optParams.Ks(i)*optParams.Ks(i)/dK2;
            end
            
            a(1) = 0;
            c(size) = 0;
            
            % absorbing boundary condition
            c(1) = 0;
            a(size) = 0;
            
            b(1) = 1;
            b(size) = 1;
            
            % to create tridiagonal matrix
            
            a1 = a(2:size);
            c1 = c(1:size-1);
            
            b_rg(1) = 0;
            b_rg(size) = 0;
            
            
            condProb = zeros(size,size);
            if optParams.params.GFType == -1
                oneStepDt = optParams.params.oneStepDt;
                NT = round(dT/oneStepDt);
                
                A_rg = full(gallery('tridiag',-a1*1.0/dT,-b_rg*1.0/dT,-c1*1.0/dT));
                A_exp1 = eqDupireSpotGF.expmReghai(A_rg,dT);
                newProb = lastProb*A_exp1;
                condProb =A_exp1;
            elseif optParams.params.GFType == 0
                oneStepDt = optParams.params.oneStepDt;
                NT = round(dT/oneStepDt);
                
                A_rg = full(gallery('tridiag',-a1*oneStepDt/dT,-b_rg*oneStepDt/dT,-c1*oneStepDt/dT));
                A_exp1 = expmdemo1(A_rg*NT);
                newProb = lastProb*A_exp1;
                condProb =A_exp1;
            
            elseif optParams.params.GFType == 1
                oneStepDt = optParams.params.oneStepDt;
                NT = round(dT/oneStepDt);
                
                A_rg = full(gallery('tridiag',-a1*oneStepDt/dT,-b_rg*oneStepDt/dT,-c1*oneStepDt/dT));
                A_exp1 = expm(A_rg*NT);
                newProb = lastProb*A_exp1;
                condProb =A_exp1;
            elseif optParams.params.GFType == 2
                oneStepDt = optParams.params.oneStepDt;
                NT = round(dT/oneStepDt);
                
                A_rg = full(gallery('tridiag',-a1*oneStepDt/dT,-b_rg*oneStepDt/dT,-c1*oneStepDt/dT));
                A_exp1 = expm(A_rg);
                
                newProb = lastProb;
                condProb = eye(size,size);
                for t=1:NT
                    newProb = newProb*A_exp1;
                    condProb = A_exp1*A_exp1;
                end
                
            elseif optParams.params.GFType == 3
                
                a1 = a(2:size);
                c1 = c(1:size-1);
            
                % Test for HugeDecomposition for inverse matrix
                [ x,y,d] = eqDupireSpotGF.HugeDecomp(size,a,b,c);
                A = gallery('tridiag',a1,b,c1);
                oneStep = OneStepGF(size,x,y,d,A);
                newProb = lastProb;
                newProb = newProb*oneStep;
                
                condProb = oneStep;
                
            else
                oneStepDt = optParams.params.oneStepDt;
                NT = round(dT/oneStepDt);
                
                % rescale matrix element to smaller time step
                a= a*oneStepDt/dT;
                b = (b-1)*oneStepDt/dT + 1.0;
                c = c*oneStepDt/dT;
                
                a1 = a(2:size);
                c1 = c(1:size-1);
            
                % Test for HugeDecomposition for inverse matrix
                [ x,y,d] = eqDupireSpotGF.HugeDecomp(size,a,b,c);
                A = gallery('tridiag',a1,b,c1);
                oneStep = OneStepGF(size,x,y,d,A);
                newProb = lastProb;
                %numberOfTimeStep;
                %oneStepDt = 1.0/365;
                
                condProb = eye(size,size);
                for t=1:NT
                    newProb = newProb*oneStep;
                    condProb = oneStep*oneStep;
                end 
                
            end
            
            out.newProb = newProb;
            out.condProb = condProb;
        end
        
        function newP = inductPDEBackward(eqDupireSpotGF,fromTime,toTime,lastP,params)
            
            expiry = eqDupireSpotGF.localVolSurface.expiry;
            
            idx = find(eqDupireSpotGF.localVolSurface.expiry >= fromTime);
            if isempty(idx)
                idxNow = length(expiry);
            else
                idxNow = min(idx);
            end
            
            volProxy = eqDupireSpotGF.localVolSurface.volProxy(idxNow,:);
            ipos = eqDupireSpotGF.localVolSurface.ipos(idxNow,:);
            Ks = params.Ks;
            %filling proxy grid vol
            proxy = zeros(length(lastP),1);
            proxy = eqDupireSpotGF.GetLocalVolFromProxy(volProxy,Ks,ipos);
            %filling end
            
            size = length(lastP);
            a = zeros(size,1);
            b = zeros(size,1);
            c = zeros(size,1);
            b_rg = zeros(size,1);
            
            dK = params.Ks(2) - params.Ks(1);
            dT = (fromTime - toTime)/365.0;
            
            dK2 = dK*dK;
            for i=1:size
                a(i) = -0.5*dT*proxy(i)*proxy(i)*Ks(i)*Ks(i)/dK2;
                b(i) = 1 + dT*proxy(i)*proxy(i)*Ks(i)*Ks(i)/dK2;
                b_rg(i) = dT*proxy(i)*proxy(i)*Ks(i)*Ks(i)/dK2;
                c(i) = -0.5*dT*proxy(i)*proxy(i)*Ks(i)*Ks(i)/dK2;
            end
            
            a(1) = 0;
            c(size) = 0;
            
            % absorbing boundary condition
            c(1) = 0;
            a(size) = 0;
            
            b(1) = 1;
            b(size) = 1;
            
            % to create tridiagonal matrix
            
            a1 = a(2:size);
            c1 = c(1:size-1);
            
            b_rg(1) = 0;
            b_rg(size) = 0;
            
            if params.GFType == -1
                oneStepDt = params.oneStepDt;
                NT = round(dT/oneStepDt);
                
                A_rg = full(gallery('tridiag',-a1*1.0/dT,-b_rg*1.0/dT,-c1*1.0/dT));
                A_exp1 = eqDupireSpotGF.expmReghai(A_rg,dT);
                newP = A_exp1*lastP;
                
            elseif params.backGFType == 0
                oneStepDt = params.oneStepDt;
                NT = round(dT/oneStepDt);
                
                A_rg = full(gallery('tridiag',-a1*oneStepDt/dT,-b_rg*oneStepDt/dT,-c1*oneStepDt/dT));
                A_exp1 = expmdemo1(A_rg*NT);
                newP = A_exp1*lastP;
            elseif params.backGFType == 1
                oneStepDt = params.oneStepDt;
                NT = round(dT/oneStepDt);
                
                A_rg = full(gallery('tridiag',-a1*oneStepDt/dT,-b_rg*oneStepDt/dT,-c1*oneStepDt/dT));
                A_exp1 = expm(A_rg*NT);
                newP = A_exp1*lastP;
            elseif params.backGFType == 2
                oneStepDt = params.oneStepDt;
                NT = round(dT/oneStepDt);
                
                A_rg = full(gallery('tridiag',-a1*oneStepDt/dT,-b_rg*oneStepDt/dT,-c1*oneStepDt/dT));
                A_exp1 = expm(A_rg);
                
                newP = lastP;
                
                for t=1:NT
                    newP = A_exp1*newP;
                end
                
            elseif params.backGFType == 3

                
                a1 = a(2:size);
                c1 = c(1:size-1);
                
                tdma.size = length(lastP);
                tdma.a = a;
                tdma.b = b;
                tdma.c = c;
                tdma.uu = zeros(tdma.size,1);
                tdma.ll = zeros(tdma.size,1);
                tdma.dd = zeros(tdma.size,1);

                tdma.bupdated = 1;

                tdmaLU =  eqDupireSpotGF.LUDecompose(tdma);
                newP = eqDupireSpotGF.TDMASolve(tdmaLU,lastP);

                
            elseif (params.backGFType == 4) || (params.backGFType == 14) || (params.backGFType == 24)
                oneStepDt = params.oneStepDt;
                NT = round(dT/oneStepDt);
                
                % rescale matrix element to smaller time step
                a= a*oneStepDt/dT;
                b = (b-1)*oneStepDt/dT + 1.0;
                c = c*oneStepDt/dT;
                
                a1 = a(2:size);
                c1 = c(1:size-1);
                
                tdma.size = length(lastP);
                tdma.a = a;
                tdma.b = b;
                tdma.c = c;
                tdma.uu = zeros(tdma.size,1);
                tdma.ll = zeros(tdma.size,1);
                tdma.dd = zeros(tdma.size,1);

                tdma.bupdated = 1;

                tdmaLU =  eqDupireSpotGF.LUDecompose(tdma);
                
                newP = lastP;
                for t=1:NT
                    newP = eqDupireSpotGF.TDMASolve(tdmaLU,newP);
                end

            else 
                disp('unImplemented')
            end
        end
        
        function marginal = InterpolateMarginal(eqDupireSpotGF,fromTime,toTime,params)

            expiry = eqDupireSpotGF.localVolSurface.expiry;
            idx = find(eqDupireSpotGF.localVolSurface.expiry >= toTime);
            if isempty(idx)
                idxNow = length(expiry);
            else
                idxNow = min(idx);
            end
            
            marginal(:,:) = eqDupireSpotGF.localVolSurface.marginal(idxNow,:,:);
        end
        
        function localVol = InterpolateLocalVol(eqDupireSpotGF,x,fromTime,params)

            expiry = eqDupireSpotGF.localVolSurface.expiry;
            idx = find(eqDupireSpotGF.localVolSurface.expiry > fromTime);
            if isempty(idx)
                idxNow = length(expiry);
            else
                idxNow = min(idx);
            end
            
            localVolLine = eqDupireSpotGF.localVolSurface.localVol(idxNow,:);
            ipos = eqDupireSpotGF.localVolSurface.ipos(idxNow,:);
            Ks = params.Ks;
            
            volKnotPoints = zeros(length(ipos),1);
            for i=1: length(ipos)
                volKnotPoints(i) = Ks(ipos(i));
            end
            
            localVol =  zeros(length(x),1);
            for i=1:length(x)
                xIdx = find(volKnotPoints <=x(i));
                if isempty(xIdx)
                    localVol(i) = localVolLine(1);
                elseif max(xIdx) == ipos(length(ipos))
                    localVol(i) = localVolLine(length(ipos));
                else
                    localVolIdx = max(xIdx);
                    weight = (x(i)-Ks(ipos(localVolIdx)))/(Ks(ipos(localVolIdx)+1)-Ks(ipos(localVolIdx)));
                    localVol(i) = weight*localVolLine(ipos(localVolIdx)+1)+(1-weight)*localVolLine(ipos(localVolIdx));
                end
            end
        end
        
        function out = inductMCMCForward(eqDupireSpotGF,fromTime,toTime,lastX,lastXIdx,U,params)
            
            NMC = length(lastX);
            Qa = InterpolateMarginal(eqDupireSpotGF,fromTime,toTime,params);
            uSample = U;
            
            nextX = zeros(NMC,1);
            nextXIdx = zeros(NMC,1);
            Ks = params.Ks;
            nPoints = length(Ks);
            
            for i=1:NMC
                IndexMc = lastXIdx(i);
                j= IndexMc;
                cumProb = Qa(IndexMc,:);
                idxH = min(find(uSample(i)*Qa(IndexMc,end) < cumProb));
                nextX(i) = Ks(idxH);
                nextXIdx(i) = idxH;

            end
            
            out.nextX = nextX;
            out.nextXIdx = nextXIdx;

            
        end
        
        function newX = inductMCForward(eqDupireSpotGF,fromTime,toTime,startIdx,endIdx,timeScheduleInfo,lastX,dZ,params)
            
            timeSchedule = timeScheduleInfo.timeSchedule;
            dTSchedule365 = timeScheduleInfo.dTSchedule365;
            volKnotIdx = timeScheduleInfo.volKnowIdx;
            
            predictor = lastX;
            for j=startIdx:endIdx-1
                
                localVol = eqDupireSpotGF.InterpolateLocalVol(predictor,timeSchedule(j),params);
                sqrtDt = sqrt(dTSchedule365(j));
                for i=1:length(lastX)
                    predictor(i) = predictor(i)*(1 + localVol(i)*sqrtDt*dZ(i,j-startIdx+1)); 
                end
            end
            newX = predictor;
            
        end
        
        
        
        function out = InterpolateTargetStrikeVolFPI(eqDupireSpotGF,idxNow,meshProb,params)
            % Calculate Object Function
            % settings for bisection
            a = 0.001;
            b = 10;
            Tol = 1e-10;
            MaxIter = 1000;

            strikes = params.marketStrikes;
            strikeSize = length(params.marketStrikes);
            out.vols    = zeros(strikeSize,1);
            out.interpOTMPrices = zeros(strikeSize,1);
            out.interpBlackPrices = zeros(strikeSize,1);
            
            %meshmatrixC(i,j) : max(x_i - k_j,0)
            %meshmatrixP(i,j) : max(-x_i + k_j,0)
            % payoff at maturity
            modelX = params.Ks;
            modelXSize = length(params.Ks);
            
            meshC = zeros(modelXSize,strikeSize);
            meshP = zeros(modelXSize,strikeSize);
            
            for i=1:modelXSize
                for j=1:strikeSize
                    meshC(i,j) = max(modelX(i) - strikes(j),0);
                    meshP(i,j) = max(strikes(j) - modelX(i),0);
                end
            end
            
            cValue = zeros(1,strikeSize);
            pValue = zeros(1,strikeSize);
            
            cValue = meshProb*meshC;
            pValue = meshProb*meshP;
            
            % use jackel's Lets Be Rational compiled code(mex) for price and implied vol calculation
            % blackVolLBR
            % blackPrice
            
            for i=1:strikeSize
                K = strikes(i);
                if K <= 1.0
                      out.vols(i) = blackVolLBR(pValue(i),1.0,K, params.params.expiry(idxNow)/365.0,-1);
                     out.interpOTMPrices(i) = pValue(i);
                     out.interpBlackPrices(i) = blackPrice(1.0,K,out.vols(i),params.params.expiry(idxNow)/365.0,-1);
                     
                else
                    out.vols(i) = blackVolLBR(cValue(i),1.0,K,params.params.expiry(idxNow)/365.0,1);
                    out.interpOTMPrices(i) = cValue(i);
                    out.interpBlackPrices(i) = blackPrice(1.0,K,out.vols(i),params.params.expiry(idxNow)/365.0,1);
                end
            end
            
            
        end
        
        function out = computeBackwardPDEOne(eqDupireSpotGF,maturity,strike,params)
            expiry = params.expiry;
            
            modelX = params.Ks;
            modelXSize = length(params.Ks);
            
            mesh = zeros(modelXSize,1);
            
            if strike <= 1.0
                for i=1:modelXSize
                    mesh(i,1) = max(strike- modelX(i),0);
                end

                volExpiry = expiry(find(expiry <= maturity));
                volExpiry = [volExpiry; 0];
                timeStep = sort(union(volExpiry,maturity),'ascend');
                for i=length(timeStep):-1:2
                    mesh = eqDupireSpotGF.inductPDEBackward(timeStep(i),timeStep(i-1),mesh,params);
                end
                
                out = mesh(params.Pricingidx);
                
            else
                for i=1:modelXSize
                    mesh(i,1) = max(modelX(i)-strike,0);
                end

                volExpiry = expiry(find(expiry <= maturity));
                volExpiry = [volExpiry; 0];
                timeStep = sort(union(volExpiry,maturity),'ascend');
                for i=length(timeStep):-1:2
                    mesh = eqDupireSpotGF.inductPDEBackward(timeStep(i),timeStep(i-1),mesh,params);
                end
                
                out = mesh(params.Pricingidx);
                
            end
            
        end
     
        
        function out = computeForwardMCPerExpiry(eqDupireSpotGF,idxNow,maturity,timeScheduleInfo,strikes,dZ,NMC,params)
            expiry = params.expiry;
            
            initX = ones(NMC,1);
            
            payoff = zeros(NMC,length(strikes));
            volExpiry = expiry(find(expiry <= maturity));
            volExpiry = [volExpiry; 0];
            timeStep = sort(union(volExpiry,maturity),'ascend');
            nextX = initX;
            %divide time interval into volExpiry
            for i=2:length(timeStep)
                startIdx = find(timeScheduleInfo.timeSchedule == timeStep(i-1));
                endIdx = find(timeScheduleInfo.timeSchedule == timeStep(i));
                dZStep = dZ(:,startIdx+1:endIdx);
                nextX = eqDupireSpotGF.inductMCForward(timeStep(i-1),timeStep(i),startIdx,endIdx,timeScheduleInfo,nextX,dZStep,params);
            end
            
            
            for j=1:length(strikes)
                if(strikes(j) <= 1)
                    payoff(:,j) = max(strikes(j) - nextX,0);
                else
                    payoff(:,j) = max(nextX - strikes(j),0);
                end
            end
            out = mean(payoff);
            
        end
        
        function out = computeForwardMCMCPerExpiry(eqDupireSpotGF,maturity,strikes,U,NMC,params)
            expiry = params.expiry;
            
            initX = ones(NMC,1);
            initXIdx = ones(NMC,1)*params.Pricingidx;
            
            payoff = zeros(NMC,length(strikes));
            volExpiry = expiry(find(expiry <= maturity));
            volExpiry = [volExpiry; 0];
            timeStep = sort(union(volExpiry,maturity),'ascend');
            nextX = initX;
            nextXIdx = initXIdx;
            for i=2:length(timeStep)
                mcmcOut = eqDupireSpotGF.inductMCMCForward(timeStep(i-1),timeStep(i),nextX,nextXIdx,U(:,i-1),params);
                nextX = mcmcOut.nextX;
                nextXIdx = mcmcOut.nextXIdx;
            end
            
            
            for j=1:length(strikes)
                if(strikes(j) <= 1)
                    payoff(:,j) = max(strikes(j) - nextX,0);
                else
                    payoff(:,j) = max(nextX - strikes(j),0);
                end
            end
            out = mean(payoff);
            
        end
        
        function out = computeBackwardPDEOTMTotal(eqDupireSpotGF,params)
            expiry = params.expiry;
            strikes = params.marketStrikes;
            
            pdeOTMPrices =  zeros(length(expiry),length(strikes));
            for i=1:length(expiry)
                strikePerExpiry = strikes(i,:);
                for j=1:length(strikes)
                    pdeOTMPrices(i,j) = eqDupireSpotGF.computeBackwardPDEOne(expiry(i),strikePerExpiry(j),params);
                end
            end
            
            out = pdeOTMPrices;
            
        end
        
        
        % compute forward mc(euler scheme) for 8*21 vanilla otm products
        % generate time steps from the enddate backwards 
        
        function out = computeForwardMCOTMTotal(eqDupireSpotGF,params)
            expiry = params.expiry;
            strikes = params.marketStrikes;
            
            % we generate the schedule from the enddate
            timeStepSize = params.mcOneTimeStep;
            MCTimeStep = round(expiry(length(expiry))/timeStepSize) +1; 
            dummyTimeSchedule = zeros(MCTimeStep,1);
            
            dummyN = 0;
            for i=1:MCTimeStep
                dummyTimeSchedule(i)= expiry(length(expiry)) -timeStepSize*(i-1);
                if dummyTimeSchedule(i) < 0
                    break;
                end
                dummyN = dummyN +1;
                
            end
            
            timeSchedule = zeros(dummyN,1);
            for i=1:dummyN
                timeSchedule(i)= dummyTimeSchedule(i);
            end
            
            volExpiry = [expiry; 0];
            
            newTimeSchedule = sort(union(volExpiry,timeSchedule),'ascend');
            
            dTSchedule = zeros(length(newTimeSchedule)-1,1);
            dTSchedule365 = zeros(length(newTimeSchedule)-1,1);
            for i=length(newTimeSchedule)-1:-1:1
                dTSchedule(i) = newTimeSchedule(i+1) - newTimeSchedule(i);
                dTSchedule365(i) = (newTimeSchedule(i+1) - newTimeSchedule(i))/365.0;
            end
            
            % find the vol expiry points
            
            volKnotIdx = zeros(length(expiry),1);
            for i=1:length(expiry)
                volKnotIdx(i) = find(newTimeSchedule==expiry(i));
                
            end
            
            timeScheduleInfo.timeSchedule = newTimeSchedule;
            timeScheduleInfo.dTSchedule = dTSchedule;
            timeScheduleInfo.dTSchedule365 = dTSchedule365;
            timeScheduleInfo.volKnowIdx = volKnotIdx;
            
            
            % schedule create end
            NMC = params.NMC;
%             Ps = sobolset(length(dTSchedule));
            
            Ps = sobolset(length(dTSchedule),'Skip',NMC);
%             Ps = scramble(Ps,'MatousekAffineOwen');
            Q = qrandstream(Ps);
            Q.reset;
            U = qrand(Q,NMC);
            dZ0 = zeros(size(U,1),size(U,2));
%             dZ = norminv(U);
            % Use Accurate Normal Inverse Method , ASA241 by Michael Wichura
            % generate random number from the end
            for i=1:size(U,1)
                for j=1:size(U,2)
                    dZ0(i,j) = r8_normal_01_cdf_inverse(U(i,j));
                end
            end
            
           % apply brownian bridge
           dZ1 = eqDupireSpotGF.bbgenerator(dZ0);
           
           % first dZ is dummy , 0
           dZ = zeros(size(dZ1,1),size(dZ1,2) + 1);
           dZ(:,2) = dZ1(:,1);
           for i=3:size(dZ1,2)+1
                dZ(:,i) = dZ1(:,i-1) -  dZ1(:,i-2);
           end
           
            mcOTMPrices =  zeros(length(expiry),length(strikes));
            mcOTMPricesPerExpiry = zeros(1,length(strikes));
            for i=1:length(expiry)
                strikePerExpiry = strikes(i,:);
                mcOTMPricesPerExpiry = eqDupireSpotGF.computeForwardMCPerExpiry(i,expiry(i),timeScheduleInfo,strikePerExpiry,dZ,NMC,params);
                for j=1:length(strikes)
                    mcOTMPrices(i,j) = mcOTMPricesPerExpiry(j);
                end
            end
            
            out = mcOTMPrices;
            
        end
        
        
        % compute forward mcmc(Markov Chain Monte Carlo scheme) for 8*21 vanilla otm products
        % generate time steps using only events date 
        
        function out = computeForwardMCMCOTMTotal(eqDupireSpotGF,params)
            expiry = params.expiry;
            strikes = params.marketStrikes;
            
            %mc random number initialize
%             NMC = 2^14;
            NMC = params.NMC;
            
            MCTimeStep = length(expiry); % -1 because 0

            Ps = sobolset(MCTimeStep,'Skip',NMC);
%             Ps = scramble(Ps,'MatousekAffineOwen');
            Q = qrandstream(Ps);
            reset(Q);
            U0 = qrand(Q,NMC);
            U = zeros(NMC,MCTimeStep);
            
            % first random numbers goes to the enddate..
            for i=1:size(U,2)
                U(:,i) = U0(:,size(U,2)-(i-1));
            end            
            
            mcOTMPrices =  zeros(length(expiry),length(strikes));
            mcOTMPricesPerExpiry = zeros(1,length(strikes));
            for i=1:length(expiry)
                strikePerExpiry = strikes(i,:);
                mcOTMPricesPerExpiry = eqDupireSpotGF.computeForwardMCMCPerExpiry(expiry(i),strikePerExpiry,U,NMC,params);
                for j=1:length(strikes)
                    mcOTMPrices(i,j) = mcOTMPricesPerExpiry(j);
                end
            end
            
            out = mcOTMPrices;
            
        end
        
        %generate marginal(cumulative distribution) at each event
        %date(per expiry)
        %using conditional transition Probability matrix
        function out = generateMarginalPerExpiry(eqDupireSpotGF,condProbPerExpiry,params)
            Ks = params.Ks;
            nPoints = length(params.Ks);
            condProb = condProbPerExpiry;
            Qa = zeros(length(Ks),length(Ks));
            cumM = 0;
            cumM2 = 0;
            %first row dealt separately
            Qa(1,1) = condProb(1,1);
            cumM2 = 0.0;
            for j=2:nPoints
                cumM2 = cumM2 + condProb(1,j);
                Qa(1,j) = Qa(1,1) + cumM2;
            end
            
            for i=2:nPoints
                cumM = 0.0;
                Qa(i,i) = condProb(i,i);
                for j=i-1:-1:1
                    Qa(i,i) = Qa(i,i) + condProb(i,j);
                end

                cumM = 0.0;
                for j=i-1:-1:1
                    cumM = cumM + condProb(i,j+1);
                    Qa(i,j) = Qa(i,i) - cumM;
                end

                cumM2 = 0.0;
                for j=i+1:nPoints
                    cumM2 = cumM2 + condProb(i,j);
                    Qa(i,j) = Qa(i,i) + cumM2;
                end
            end
            
            out = Qa;
        end
        
        %generate marginal(cumulative distribution) for all eventdates
        %date(per expiry)
        %using conditional transition Probability matrix
        
        function marginal = generateMarginal(eqDupireSpotGF,condProb,params)
            Ks = params.Ks;
            marginal = zeros(params.expirySize,length(Ks),length(Ks));
            condProbPerExpiry = zeros(length(Ks),length(Ks));
            marginalPerExpiry = zeros(length(Ks),length(Ks));
            for i=1:params.expirySize
                condProbPerExpiry(:,:) = condProb(i,:,:);
                marginalPerExpiry = eqDupireSpotGF.generateMarginalPerExpiry(condProbPerExpiry,params);
                marginal(i,:,:) = marginalPerExpiry(:,:);
            end
        end
        
        
        % Target function for FixedPointIteration Optimizer
        
        function  out  = TargetFunctionVolProxyFPI(eqDupireSpotGF,tvar,optParams)
            idxNow = optParams.idxNow;
            if  idxNow ~= 1 mkt_dT = optParams.params.expiry(idxNow) - optParams.params.expiry(idxNow-1);else mkt_dT =  optParams.params.expiry(1);end;
            mkt_dT=mkt_dT/365.0;
            
            
          %% Solve  Forward PDE(GF) One time
            outGF = eqDupireSpotGF.Solve1DGF(tvar,mkt_dT,optParams.dK,optParams.lastProb,optParams);
            
            newProb = outGF.newProb;
            optParams.meshProb = newProb;
           %% PDE(GF) Solve End
           % find the implied vol of dupire model
           target = InterpolateTargetStrikeVolFPI(eqDupireSpotGF,idxNow,newProb,optParams); 

           marketImpVol = optParams.params.blackVol;
           expiry = optParams.params.expiry;
           strike = optParams.params.strike;
           
           out.newLocalVol = zeros(length(optParams.strikeSize),1);
           out.localVolError = zeros(length(optParams.strikeSize),1);
           out.impVolError =  zeros(length(optParams.strikeSize),1);
           out.modelImpVol = zeros(length(optParams.strikeSize),1);
           out.marketImpVol = zeros(length(optParams.strikeSize),1);
           out.interpOTMPrices = zeros(length(optParams.strikeSize),1);
           out.interpBlackPrices = zeros(length(optParams.strikeSize),1);
           out.priceRe = zeros(length(optParams.strikeSize),1);
           
           for i=1:optParams.strikeSize
                out.newLocalVol(i) = tvar(i)*(marketImpVol(idxNow,i))/target.vols(i);
                out.localVolError(i) = out.newLocalVol(i) - tvar(i);
                out.modelImpVol(i) = target.vols(i);
                out.marketImpVol(i) =  marketImpVol(idxNow,i);
                out.impVolError(i) = out.modelImpVol(i) - out.marketImpVol(i);
                out.interpOTMPrices(i) = target.interpOTMPrices(i);
                out.interpBlackPrices(i) = target.interpBlackPrices(i);
                out.priceRe(i) = out.interpOTMPrices(i)/optParams.params.blackOTMPrices(idxNow,i)-1.0;
           end
           
           % for shorter tenor we only fit for the restricted interval
            if idxNow <= optParams.params.numOfCutoffTenor
                for i=1:optParams.lowerIdx-1
                    out.newLocalVol(i) = out.newLocalVol(optParams.lowerIdx);
                end

                for i=optParams.upperIdx+1:optParams.strikeSize
                    out.newLocalVol(i) = out.newLocalVol(optParams.upperIdx);
                end
            end
            
            out.volErrorTotal = 0.0;
            out.priceReTotal = 0.0;
            %for one month we only fit betwee lower and upper
            dummyN = 0;
            if idxNow <= optParams.params.numOfCutoffTenor
                for i=optParams.lowerIdx:optParams.upperIdx
                    out.volErrorTotal = out.volErrorTotal + out.impVolError(i)*out.impVolError(i);
                    out.priceReTotal = out.priceReTotal + out.priceRe(i)*out.priceRe(i);
                    dummyN = dummyN +1;
                end
            else
                for i=1:length(out.impVolError)
                    out.volErrorTotal = out.volErrorTotal + out.impVolError(i)*out.impVolError(i);
                    out.priceReTotal = out.priceReTotal + out.priceRe(i)*out.priceRe(i);
                    dummyN = dummyN +1;
                end
            end
            out.volErrorTotal = sqrt(out.volErrorTotal/dummyN);
            out.priceReTotal = sqrt(out.priceReTotal/dummyN);
            
        end
        
         % Fixed Point Iteration Optimizer
         % start from implied vol find the local vol using Reghai's fixed
         % point iteration optimization
         % LocalVol(n+1) = LocalVol(n)*(marketImpVol)/ModelImplVol(n);
         
        function [x, fval,marketImpVol,modelImpVol,interpBlackPrices, interpOTMPrices,volError,volErrorTotal, priceRe, priceReTotal,nIter] = FixedPointIter(eqDupireSpotGF,targetfunc,tvar,tol,maxIter,optParams)
            
            n= 0;
            errorTry = 1000.0; % Initial ErrorBound make it big enough
            tvarOld = tvar;
            if optParams.params.targetType == 1
                while (errorTry > tol) && (n < maxIter)
                    out = targetfunc(tvar);
                    tvarOld = tvar;
                    % reduce implied vol error
                    errorTry = out.volErrorTotal;
                    tvar = out.newLocalVol;
                    n = n+1;
                end
                x = tvar;
                finalOut = targetfunc(tvarOld);
                fval = finalOut.volErrorTotal;
                marketImpVol = finalOut.marketImpVol;
                modelImpVol  = finalOut.modelImpVol;
                interpBlackPrices = finalOut.interpBlackPrices;
                interpOTMPrices = finalOut.interpOTMPrices;
                volError     = finalOut.impVolError;
                volErrorTotal = finalOut.volErrorTotal;
                priceRe      = finalOut.priceRe;
                priceReTotal = finalOut.priceReTotal;
                nIter = n;
            else
                while (errorTry > tol) && (n < maxIter)
                    out = targetfunc(tvar);
                    tvarOld = tvar;
                    % reduce price relative error
                    errorTry = out.priceReTotal;
                    tvar = out.newLocalVol;
                    n = n+1;
                end
                x = tvar;
                finalOut = targetfunc(tvarOld);
                fval = finalOut.volErrorTotal;
                marketImpVol = finalOut.marketImpVol;
                modelImpVol  = finalOut.modelImpVol;
                interpBlackPrices = finalOut.interpBlackPrices;
                interpOTMPrices = finalOut.interpOTMPrices;
                volError     = finalOut.impVolError;
                volErrorTotal = finalOut.volErrorTotal;
                priceRe      = finalOut.priceRe;
                priceReTotal = finalOut.priceReTotal;
                nIter = n;
            end
            
        end
        
        % Fixed Local Vol per expiry by bootstrapping
        
        function bootStrapOut = FindVolProxy(eqDupireSpotGF,idxNow,dT,lastProb,params)
            %initialize calibration parameters
            
            optParams.idxNow = idxNow;
            optParams.dT = dT;
            optParams.params = params;
            optParams.ipos = zeros(params.strikeSize,1);
            optParams.Ks = zeros(params.Ns,1);
            optParams.Ks = params.Ks;
            optParams.strikeSize = params.strikeSize;
            optParams.meshSize = params.Ns;
            
            optParams.meshProb = zeros(1,params.Ns);
            
            optParams.proxy = zeros(params.Ns,1);
            optParams.dK = params.Ks(2) - params.Ks(1);
            
            optParams.marketStrikes = zeros(params.strikeSize,1);
            optParams.blackOTMPrices = params.blackOTMPrices;
            optParams.callCurve = zeros(params.strikeSize,1);
            optParams.putCurve = zeros(params.strikeSize,1);
            if strcmp(params.volSurface.params('moneyNessType'),'spotMoneyNess')
                % market strike points in fwd PDE grid
                fwdFactor = eqDupireSpotGF.FwdFactor(params.expiry(idxNow));
                for i=1:params.strikeSize
                    optParams.marketStrikes(i) = params.strike(i)/fwdFactor;
                end
            else
                for i=1:params.strikeSize
                    optParams.marketStrikes(i) = params.strike(i);
                end
            end
            
            % positions to separate vol proxy regimes
            for i=1:params.strikeSize
                optParams.ipos(i) = floor( (optParams.marketStrikes(i) - optParams.Ks(1))/optParams.dK) + 1;  % add default 1
            end
%             optParams.ipos(params.strikeSize) = optParams.meshSize;
            
            optParams.lastProb = lastProb;
            
            % exception for one month maturity for restricted interval
            
            lowerCutoff = params.lowerCutoffVector(idxNow);
            upperCutoff = params.upperCutoffVector(idxNow);
            numOfCutoffTenor = params.numOfCutoffTenor; 
            
            lowerIdx = 1;
            upperIdx = params.strikeSize;
            
            if idxNow <= numOfCutoffTenor
                lowerIdx = min(find(optParams.marketStrikes >= lowerCutoff));
                upperIdx = max(find(optParams.marketStrikes <= upperCutoff));
            end
            
            optParams.lowerIdx = lowerIdx;
            optParams.upperIdx = upperIdx;
            
            for i=1:params.strikeSize
                tvar(i) = params.blackVol(idxNow,i);
                lb(i) = 0.01;
                ub(i) = 1.0;
            end
            
            for i=1:lowerIdx-1
                tvar(i) = tvar(lowerIdx);
            end
            
            for i=upperIdx+1:params.strikeSize
                tvar(i) = tvar(upperIdx);
            end

            
            targetfunc = @(tvar) TargetFunctionVolProxyFPI(eqDupireSpotGF,tvar,optParams);
            tol = 0.0001;
            maxIter = 1000;
            
            [x, fval,marketImpVol,modelImpVol,interpBlackPrices, interpOTMPrices,volError,volErrorTotal, priceRe, priceReTotal,nIter]  = eqDupireSpotGF.FixedPointIter(targetfunc,tvar,tol,maxIter,optParams);
            
            for i=1:params.strikeSize
                bootStrapOut.volProxy(i) = x(i);
                bootStrapOut.ipos(i) = optParams.ipos(i);
            end
            
            % for later use
            bootStrapOut.marketStrikes = optParams.marketStrikes;
            
            bootStrapOut.volError = volError;
            bootStrapOut.marketImpVol = marketImpVol;
            bootStrapOut.modelImpVol = modelImpVol;
            bootStrapOut.interpBlackPrices = interpBlackPrices;
            bootStrapOut.dupireOTMPrices = interpOTMPrices;
            
            bootStrapOut.nIter = nIter;
            
            out = eqDupireSpotGF.Solve1DGF(bootStrapOut.volProxy,dT,optParams.dK,lastProb,optParams);
            
            bootStrapOut.lastProb = out.newProb;
            bootStrapOut.condProb = out.condProb;
            
        end
        
        function out = CalibrateToVolSurface(eqDupireSpotGF,black,calibrationFlag)
            
          % Calculate Black OTM Prices as calibration target
            volSurface = black.volSurface;
            
            expiry = volSurface.maturities;
            strike  = volSurface.strikes;
            spot =  black.spot.params('rawData');
            expirySize = length(expiry);
            strikeSize = length(strike);
            
            blackOTMPrices = zeros(expirySize,strikeSize);
            blackOTMPrices2 = zeros(expirySize,strikeSize);
            blackVol = zeros(expirySize,strikeSize);
            blackOTMVega = zeros(expirySize,strikeSize);
            
            % we add blackvol as matrix so that in the calibation we don't
            % need to do 2D interpolation again and again!!
            % also for the target black price too!!
            
            if strcmp(volSurface.params('moneyNessType'),'spotMoneyNess')
                for k=1:expirySize
                    fwd = black.Fwd(spot,expiry(k));
                    df = black.zeroCurve.DF(expiry(k)/365.0);
                    fwdFactor = eqDupireSpotGF.FwdFactor(expiry(k));
                    for j=1:strikeSize
                        blackVol(k,j) = volSurface.vol(expiry(k),strike(j));
                        fwdStrike = strike(j)/fwdFactor;
                        if fwdStrike <= 1.0
%                             blackOTMPrices(k,j) = black.BlackVanilla(expiry(k),expiry(k),spot,spot*strike(j),volSurface.vol(expiry(k),strike(j)),'P')/fwd/df;
                            blackOTMPrices(k,j) = blackPrice(1.0,spot*strike(j)/fwd,blackVol(k,j),expiry(k)/365.0,-1);
                            blackOTMPrices2(k,j) = blackPrice(1.0,spot*strike(j)/fwd,blackVol(k,j),expiry(k)/365.0,-1);
                            blackOTMVega(k,j) = BlackFwdVega(1,fwdStrike,blackVol(k,j),expiry(k)/365.0);
                        else
%                             blackOTMPrices(k,j)  = black.BlackVanilla(expiry(k),expiry(k),spot,spot*strike(j),volSurface.vol(expiry(k),strike(j)),'C')/fwd/df;
                            blackOTMPrices(k,j)  = blackPrice(1.0,spot*strike(j)/fwd,blackVol(k,j),expiry(k)/365.0,1);
                            blackOTMPrices2(k,j) = blackPrice(1.0,spot*strike(j)/fwd,blackVol(k,j),expiry(k)/365.0,1);
                            blackOTMVega(k,j) = BlackFwdVega(1,fwdStrike,blackVol(k,j),expiry(k)/365.0);
                            
                        end
                    end
                end
            else
                for k=1:expirySize
                    fwd = black.Fwd(spot,expiry(k));
                    df = black.zeroCurve.DF(expiry(k)/365.0);
                    for j=1:strikeSize
                        blackVol(k,j) = volSurface.vol(expiry(k),strike(j));
                        if strike(j) <= 1
                            blackOTMPrices(k,j) = black.BlackVanilla(expiry(k),expiry(k),spot,fwd*strike(j),volSurface.vol(expiry(k),strike(j)),'P')/fwd/df;
                        else
                            blackOTMPrices(k,j)  = black.BlackVanilla(expiry(k),expiry(k),spot,fwd*strike(j),volSurface.vol(expiry(k),strike(j)),'C')/fwd/df;
                        end
                    end
                end
            end
            
            
            params.volSurface = volSurface;
            params.expiry = expiry;
            params.strike = strike;
            
            params.blackOTMPrices = blackOTMPrices;
            params.blackVol = blackVol;
            
            params.expirySize = expirySize;
            params.strikeSize =  strikeSize;
            
            % vol proxy calibration grid setting
            
            params.Kmin = eqDupireSpotGF.modelParams('Kmin');
            params.Kmax = eqDupireSpotGF.modelParams('Kmax');
            params.Ns   = eqDupireSpotGF.modelParams('Ns');
            params.dK   = eqDupireSpotGF.modelParams('dK');
            params.Ks   = eqDupireSpotGF.modelParams('Ks');
            
            % current spot on the grid
            params.Pricingidx = floor((1.0-params.Ks(1))/params.dK) + 1;
            
            % targetType : 1 = implied vol diff
            %              2 = price relative error  
            params.targetType = 1;      
            
            % GFType : 1 = matrix exponential moler higham
            %          2 = one step matrix exponential moler higham & multiplication from
            %          3 = one step GF & multiplication from right Andreasen & Huge
            
            params.GFType = eqDupireSpotGF.modelParams('GFType');
            
            params.backGFType = eqDupireSpotGF.modelParams('backGFType');
            
            params.mcOneTimeStep = eqDupireSpotGF.modelParams('mcOneTimeStep');
            
            params.pricingSchemeType = eqDupireSpotGF.modelParams('pricingSchemeType');
            params.NMC = eqDupireSpotGF.modelParams('NMC');
            
            % backGFType : 1 = forward FD(GF)
            %              2 = implicit FD
            
            params.oneStepDt = 1.0/365.0;
            
            
            
            params.numOfCutoffTenor = eqDupireSpotGF.modelParams('numOfCutoffTenor');
            
            params.lowerCutoffVector = eqDupireSpotGF.modelParams('lowerCutoffVector');
            params.upperCutoffVector = eqDupireSpotGF.modelParams('upperCutoffVector');
            
            
            %output vol proxy
            
            out.volProxy = zeros(params.expirySize,params.strikeSize);
            out.ipos = zeros(params.expirySize,params.strikeSize);
            out.probMesh = zeros(params.expirySize,params.Ns);
            
            out.condProb = zeros(params.expirySize,params.Ns,params.Ns);
            
            
            %initialze the intial probability distribution in the grid
            initProb = zeros(1,params.Ns);
            initProb(params.Pricingidx) = 1.0;
            
            out.blackOTMPrices = blackOTMPrices;
            out.blackOTMPrices2= blackOTMPrices2;
            
            out.blackVol = blackVol;
            out.blackOTMVega = blackOTMVega;
            
            out.marketImpVol = zeros(expirySize,strikeSize);
            out.modelImpVol  = zeros(expirySize,strikeSize);
            
            out.interpBlackPrices = zeros(expirySize,strikeSize);
            out.dupireOTMPrices = zeros(expirySize,strikeSize);
            
            out.dupireBlackVol = zeros(expirySize,strikeSize);
            
            out.volError = zeros(expirySize,strikeSize);
            out.priceRe = zeros(expirySize,strikeSize);
            out.priceReOrig = zeros(expirySize,strikeSize);
            
            out.marketStrikes = zeros(expirySize,strikeSize);
            
            out.Ks =  params.Ks;
            out.nIter = zeros(expirySize,1);
            out.localVol = zeros(expirySize,length(out.Ks));
            out.rmseTotal=0.0;
            out.fwdVolMseTotal = 0.0;
            
            
            calibrationType = 'bootstrap';
            if nargin < 3
                calibrationType = 'bootstrap'; 
            else
                calibrationType = calibrationFlag;
            end
            
            switch calibrationType
                case 'bootstrap'
                    
                    tic
                    %vol proxy calibration
                    lastProb = initProb;
                    
                    for i=1:expirySize
                        if  i ~= 1 dT = params.expiry(i) - params.expiry(i-1);else dT =  params.expiry(i);end;
                        dT = dT/365.0;
                        bootStrapOut = eqDupireSpotGF.FindVolProxy(i,dT,lastProb,params);
                        for j=1:strikeSize
                            out.volProxy(i,j) = bootStrapOut.volProxy(j);
                            out.ipos(i,j)     = bootStrapOut.ipos(j);
                            out.volError(i,j) = bootStrapOut.volError(j);
                            out.marketImpVol(i,j) =  bootStrapOut.marketImpVol(j);
                            out.modelImpVol(i,j) =  bootStrapOut.modelImpVol(j);
                            out.interpBlackPrices(i,j) = bootStrapOut.interpBlackPrices(j);
                            out.dupireOTMPrices(i,j) = bootStrapOut.dupireOTMPrices(j);
                            out.priceRe(i,j) = out.dupireOTMPrices(i,j)/out.interpBlackPrices(i,j)-1.0;
                            out.priceReOrig(i,j) = out.dupireOTMPrices(i,j)/out.blackOTMPrices(i,j)-1.0;
                            out.marketStrikes(i,j) = bootStrapOut.marketStrikes(j);
                        end
                        
                        out.nIter(i) = bootStrapOut.nIter;
                        tempLocalVol = eqDupireSpotGF.GetLocalVolFromProxy(bootStrapOut.volProxy,out.Ks,bootStrapOut.ipos);
                        for j=1:params.Ns
                            out.probMesh(i,j) = bootStrapOut.lastProb(j);
                            out.localVol(i,j) = tempLocalVol(j);
                        end
                        
                        out.condProb(i,:,:) = bootStrapOut.condProb;
                        % Initial Payoff for the next period
                        lastProb = bootStrapOut.lastProb;
                        
                    end
                    
                    N=0;
                    for j=1:expirySize
                        for k=1:strikeSize
                            out.rmseTotal = out.rmseTotal + out.priceReOrig(j,k)*out.priceReOrig(j,k);
                            out.fwdVolMseTotal = out.fwdVolMseTotal + out.volError(j,k)*out.volError(j,k);
                            N=N+1;
                        end
                    end
                   
                    out.rmseTotal = sqrt(out.rmseTotal/N);
                    out.fwdVolMseTotal = sqrt(out.fwdVolMseTotal/N);
                    toc
                    
                    tic
                    % assigning the calibrated volProxy to the local vol
                    % surface
                    eqDupireSpotGF.localVolSurface.volProxy = out.volProxy;
                    eqDupireSpotGF.localVolSurface.ipos = out.ipos;
                    eqDupireSpotGF.localVolSurface.localVol = out.localVol;
                    eqDupireSpotGF.localVolSurface.probMesh = out.probMesh;
                    eqDupireSpotGF.localVolSurface.condProb = out.condProb;
                    
                    % generate marginal(cumulative) distribution for mcmc
                    % simulation
                    
                    eqDupireSpotGF.localVolSurface.marginal = eqDupireSpotGF.generateMarginal(out.condProb,params);
                    
                    eqDupireSpotGF.localVolSurface.Ks = out.Ks;
                    eqDupireSpotGF.localVolSurface.expiry = params.expiry;
                    eqDupireSpotGF.localVolSurface.strike = params.strike;
                    eqDupireSpotGF.localVolSurface.marketStrikes = out.marketStrikes;
                    
                    % calibration gives model schedule, for later use
                    % general products such as autocallables..
                    
                    eqDupireSpotGF.modelSchedule = eqDupireSpotGF.localVolSurface.expiry;
                    
                    params.marketStrikes = out.marketStrikes;
                    
                    toc
                    
%                     calculate backward scheme for given target
%                     compareforwardMCFlag = 1 for one step euler scheme
%                     compareforwardMCFlag = 2 for daily euler scheme
%                     
%                     compareforwardMCFlag = 3 for markov chain monte carlo
                     
                    
                    pricingSchemeType = params.pricingSchemeType; 
                    
                    tic
                    
                    if pricingSchemeType == 1
                        out.pdeOTMPrices = eqDupireSpotGF.computeBackwardPDEOTMTotal(params);

                        out.backRe = zeros(expirySize,strikeSize);
                        out.backwardVol = zeros(expirySize,strikeSize);
                        out.backVolError = zeros(expirySize,strikeSize);

                        out.backPriceRmseTotal = 0.0;
                        out.backVolMseTotal = 0.0;
                        for i=1:expirySize
                            for j=1:strikeSize
                                out.backRe(i,j) = out.pdeOTMPrices(i,j)/out.dupireOTMPrices(i,j)-1;
                                if out.marketStrikes(i,j) <=1.0
                                    out.backwardVol(i,j) = blackVolLBR(out.pdeOTMPrices(i,j),1,out.marketStrikes(i,j),params.expiry(i)/365.0,-1);
                                else
                                    out.backwardVol(i,j) = blackVolLBR(out.pdeOTMPrices(i,j),1,out.marketStrikes(i,j),params.expiry(i)/365.0,1);
                                end
                                out.backVolError(i,j) = out.backwardVol(i,j) - out.marketImpVol(i,j);

                            end
                        end

                        N=0;
                        for j=1:expirySize
                            for k=1:strikeSize
                                out.backPriceRmseTotal = out.backPriceRmseTotal + out.backRe(j,k)*out.backRe(j,k);
                                out.backVolMseTotal = out.backVolMseTotal +out.backVolError(j,k)*out.backVolError(j,k);
                                N=N+1;
                            end
                        end

                        out.backPriceRmseTotal = sqrt(out.backPriceRmseTotal/N);
                        out.backVolMseTotal=sqrt(out.backVolMseTotal/N);
  
                    elseif pricingSchemeType == 3
                        out.mcOTMPrices = eqDupireSpotGF.computeForwardMCOTMTotal(params);

                        out.backRe = zeros(expirySize,strikeSize);
                        out.backwardVol = zeros(expirySize,strikeSize);
                        out.backVolError = zeros(expirySize,strikeSize);

                        out.backPriceRmseTotal = 0.0;
                        out.backVolMseTotal = 0.0;
                        for i=1:expirySize
                            for j=1:strikeSize
                                out.backRe(i,j) = out.mcOTMPrices(i,j)/out.dupireOTMPrices(i,j)-1;
                                if out.marketStrikes(i,j) <=1.0
                                    out.backwardVol(i,j) = blackVolLBR(out.mcOTMPrices(i,j),1,out.marketStrikes(i,j),params.expiry(i)/365.0,-1);
                                else
                                    out.backwardVol(i,j) = blackVolLBR(out.mcOTMPrices(i,j),1,out.marketStrikes(i,j),params.expiry(i)/365.0,1);
                                end
                                out.backVolError(i,j) = out.backwardVol(i,j) - out.marketImpVol(i,j);

                            end
                        end

                        N=0;
                        for j=1:expirySize
                            for k=1:strikeSize
                                out.backPriceRmseTotal = out.backPriceRmseTotal + out.backRe(j,k)*out.backRe(j,k);
                                out.backVolMseTotal = out.backVolMseTotal +out.backVolError(j,k)*out.backVolError(j,k);
                                N=N+1;
                            end
                        end

                        out.backPriceRmseTotal = sqrt(out.backPriceRmseTotal/N);
                        out.backVolMseTotal=sqrt(out.backVolMseTotal/N);
                    
                    elseif pricingSchemeType == 4
                        out.mcOTMPrices = eqDupireSpotGF.computeForwardMCMCOTMTotal(params);
                        out.backRe = zeros(expirySize,strikeSize);
                        out.backwardVol = zeros(expirySize,strikeSize);
                        out.backVolError = zeros(expirySize,strikeSize);

                        out.backPriceRmseTotal = 0.0;
                        out.backVolMseTotal = 0.0;
                        for i=1:expirySize
                            for j=1:strikeSize
                                out.backRe(i,j) = out.mcOTMPrices(i,j)/out.dupireOTMPrices(i,j)-1;
                                if out.marketStrikes(i,j) <=1.0
                                    out.backwardVol(i,j) = blackVolLBR(out.mcOTMPrices(i,j),1,out.marketStrikes(i,j),params.expiry(i)/365.0,-1);
                                else
                                    out.backwardVol(i,j) = blackVolLBR(out.mcOTMPrices(i,j),1,out.marketStrikes(i,j),params.expiry(i)/365.0,1);
                                end
                                out.backVolError(i,j) = out.backwardVol(i,j) - out.marketImpVol(i,j);

                            end
                        end

                        N=0;
                        for j=1:expirySize
                            for k=1:strikeSize
                                out.backPriceRmseTotal = out.backPriceRmseTotal + out.backRe(j,k)*out.backRe(j,k);
                                out.backVolMseTotal = out.backVolMseTotal +out.backVolError(j,k)*out.backVolError(j,k);
                                N=N+1;
                            end
                        end

                        out.backPriceRmseTotal = sqrt(out.backPriceRmseTotal/N);
                        out.backVolMseTotal=sqrt(out.backVolMseTotal/N);
                        
                    end

                    toc
                    

                otherwise
                    disp('unImplemented')
                    
            end
        end
             
    end
    
end


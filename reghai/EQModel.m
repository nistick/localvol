classdef EQModel < Model
    %EQModel is the SuperClass of all EQModel
    % properties: zeroCurve, dividendCurve, spot 
    % methods : Fwd, GetFwdDividends
    properties(SetAccess = public)
        zeroCurve
        dividendCurve
        repoRateCurve
        spot
    end
    
    methods
        function eq = EQModel(Model)
            if nargin > 0
                eq.zeroCurve     = Model.mktData('zeroCurve');
                eq.dividendCurve = Model.mktData('dividendCurve');
                eq.repoRateCurve = Model.mktData('repoRateCurve');
                eq.spot          = Model.mktData('spot');
                eq.mktData =  Model.mktData;
                eq.modelParams = Model.modelParams;
            end
        end
       
        %determinstic forward functor
        function fwd = Fwd(eq,spotPrice,tDay)
            t = tDay/365.0;
            df = eq.zeroCurve.DF(t);
            fwd = spotPrice*1.0/df*eq.GetFwdDividends(tDay)*eq.GetFwdRepoRate(tDay);;
        end
        
        %determinstic GetFwdDividends functor
        function out = GetFwdDividends(eq,tDay)
            out =1.0;
            
            dateSize = size(eq.dividendCurve.params('rawData'),1);
            dummy = eq.dividendCurve.params('rawData');
            dates = dummy(1:dateSize,1);
%             asOfDate = datenum(eq.zeroCurve.params('asOfDate'),'yyyymmdd');
            abscisses =zeros(dateSize,1);
            for i=1:dateSize
%                 abscisses(i) = datenum(dates(i),'dd/mm/yyyy') - asOfDate;
                abscisses(i) = dates(i);
            end
            ordinates = dummy(:,2);
            out = 1.0;
            for i=1:size(dates,1)
                if abscisses(i) > tDay || abscisses(i) <= 0 
                    continue
                end
%                 t= (abscisses(i) + 105.0)/365.0 ;
%                 df = eq.zeroCurve.DF(t);
                out = out *(1.0 - ordinates(i));
            end
        end
        
        function out = GetFwdRepoRate(eq,tDay)
            out =1.0;
            
            dateSize = size(eq.repoRateCurve.params('rawData'),1);
            dummy = eq.repoRateCurve.params('rawData');
            dates = dummy(1:dateSize,1);
            abscisses =zeros(dateSize,1);
            repo_factor = 1.0;
            for i=1:dateSize
                abscisses(i) = dates(i);
            end
            ordinates = dummy(:,2);
            out = 1.0;
            cum_dt = 0;
            dt = 0;
            last_repo_rate = ordinates(end);
            for i=1:size(dates,1)
                dt = abscisses(i);
                if abscisses(i) <= 0 
                    continue
                end
                last_repo_rate = ordinates(i);
                if cum_dt >= tDay;
                    break;
                end
                repo_factor = repo_factor*exp(ordinates(i)*(min(tDay,abscisses(i))-cum_dt)/365.0);
                cum_dt = min(tDay,dt);
            end
            if(cum_dt < tDay)
                repo_factor = repo_factor*exp(last_repo_rate*(tDay-cum_dt)/365.0);
            end
            out = repo_factor;
        end
        
        function out = FwdFactor(eq,tDay)
            t = tDay/365.0;
            df = eq.zeroCurve.DF(t);
%             out =  1.0/df*eq.GetFwdDividends(tDay);
            out =  1.0/df*eq.GetFwdDividends(tDay)*eq.GetFwdRepoRate(tDay);
            
        end
    end
    
end



using QuadGK
import QuadGK: quadgk

struct HestonParameter
    λ::Float64
    η::Float64
    ρ::Float64
    v̄::Float64
    v₀::Float64
end

function hestonPiIntegrand(u::Float64, x::Float64, τ::Float64, typo::Int, hp::HestonParameter)
    λ = hp.λ
    η = hp.η
    ρ = hp.ρ
    v̄ = hp.v̄
    v₀ = hp.v₀

    α = -u^2/2 - im*u/2 + im*typo*u
    β = λ - ρ*η*typo - ρ*η*im*u
    γ = η^2/2
    d = sqrt(β^2 - 4*α*γ)
    r₊ = (β + d)/η^2
    r₋ = (β - d)/η^2
    g = r₋/r₊

    D = r₋*((1-exp(-d*τ))/(1-g*exp(-d*τ)))
    C = λ*(r₋*τ-(2/η^2)*log((1-g*exp(-d*τ))/(1-g)))
    real(exp(C*v̄ + D*v₀ + im*u*x)/(im*u))
end


function hestonPi(x::Float64, τ::Float64, typo::Int, hp::HestonParameter)
    Q(u) = hestonPiIntegrand(u, x, τ, typo, hp)
    1/2 + 1/π*quadgk(Q, 0.000001, 500.0)[1]
end

function hestonFormular(F::Float64, K::Float64, T::Float64, t::Float64, hp::HestonParameter)
    τ = T-t
    x = log(F/K)
    P1 = hestonPi(x, τ, 1, hp)
    P0 = hestonPi(x, τ, 0, hp)
    K * (exp(x)*P1 - P0)
end


function main()
    hp = HestonParameter(5.0, 0.5, -0.8, 0.05, 0.05)
    F = 100.0 * exp(0.01 * 0.5)
    info(F)
    K = 100.0
    T = 0.5
    t = 0.0
    println(hestonFormular(F, K, T, t, hp))
end


main()

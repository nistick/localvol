#ifndef fixed_termstructure_hpp
#define fixed_termstructure_hpp

#include <string>
#include <ql/quantlib.hpp>
#include <fstream>
#include "CSVRow.hpp"
#include <boost/algorithm/string/predicate.hpp>
#include <vector>
#include <map>
using namespace std;

class FixedTermStructure
{
public:
	FixedTermStructure() {};
	~FixedTermStructure() {};

	FixedTermStructure(string filepath);

	//Getter
	QuantLib::Array discountFactors() { return discountFactors_; }
	QuantLib::Array forwardFactors() const { return forwardFactors_; }

	//Setter
	void setDiscountFactors(QuantLib::Array data) { discountFactors_ = data; }
	void setForwardFactors(QuantLib::Array data) { forwardFactors_ = data; }
private:
	string currency_;
	string exchange_;
	double spot_;
	QuantLib::Date tradeDate_;
	string underlyingAsset_;

	std::map<std::string, std::vector<double> > data_;
	std::map<std::string, double> factors_;
	std::vector<QuantLib::Date> maturities_;
	QuantLib::Array discountFactors_;
	QuantLib::Array forwardFactors_;
};

#endif
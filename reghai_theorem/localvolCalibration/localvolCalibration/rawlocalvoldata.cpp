#include "rawlocalvoldata.hpp"

RawLocalVolData::RawLocalVolData(string filepath)
{
	std::ifstream file(filepath);
	CSVRow row;

	file >> row;
	
	std::stringstream   lineStream(row[0]);
	std::string         cell;

	while (std::getline(lineStream, cell, ';'))
	{
		std::stringstream cellStream(cell);
		std::string element;
		while (std::getline(cellStream, element, ':')) {
			if (boost::algorithm::ends_with(element, "Underlying asset"))
			{
				std::getline(cellStream, underlyingAsset_, ':');
			}
			else if(boost::algorithm::ends_with(element, "Trade date"))
			{
				std::string tradeDate;
				std::getline(cellStream, tradeDate, ',');
				std::getline(cellStream, tradeDate, ',');
				tradeDate_ = QuantLib::DateParser::parseFormatted(tradeDate, "%e %b %Y");

			}
			else if (boost::algorithm::ends_with(element, "Spot"))
			{
				std::string spot;
				std::getline(cellStream, spot, ':');
				spot_ = atof(spot.c_str());
			}
			else if (boost::algorithm::ends_with(element, "Currency"))
			{
				std::getline(cellStream, currency_, ':');
			}
			else if (boost::algorithm::ends_with(element, "Exchange"))
			{
				std::getline(cellStream, exchange_, ':');
			}
		}
		
	}

	
	file >> row;
	std::vector<double> vTmp;
	for (size_t i = 2; i < row.size(); i++)
	{
		vTmp.push_back(stod(row[i].substr(0, row[i].length() - 1))/100.0);
	}
	strikes_ = QuantLib::Array(vTmp.begin(), vTmp.end());
	file >> row;
	std::vector< std::vector<double> > vvTmp;
	while (file >> row)
	{
		if (row.size() == 0)
			break;
		maturities_.push_back(QuantLib::DateParser::parseFormatted(row[0], "%e-%b-%Y"));
		std::vector<double> temp;
		for (int i = 2; i < row.size(); i++) {
			temp.push_back(stod(row[i]));
		}
		vvTmp.push_back(temp);
	}
	setQuote(vvTmp);
}

void RawLocalVolData::setQuote(std::vector<std::vector<double> > &data) {
	quote_ = QuantLib::Matrix(data.size(), data[0].size());
	for (unsigned int i = 0; i < data.size(); i++) {
		for (unsigned int j = 0; j < data[0].size(); j++) {
			quote_[i][j] = data[i][j];
		}
	}
}
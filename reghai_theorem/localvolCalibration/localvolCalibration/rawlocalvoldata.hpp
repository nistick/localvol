#ifndef rawlocalvoldata_hpp
#define rawlocalvoldata_hpp

#include <string>
#include <fstream>
#include "CSVRow.hpp"
#include <boost/algorithm/string/predicate.hpp>
#include <vector>
#include <ql/quantlib.hpp>

using namespace std;

class RawLocalVolData
{
public:
	RawLocalVolData() {};
	~RawLocalVolData() {};

	RawLocalVolData(string filepath);

public:
	//Getter
	QuantLib::Array strikes() const {
		return strikes_;
	}
	std::vector<QuantLib::Date> maturities(void) {
		return maturities_;
	}
	QuantLib::Matrix& quote(void) { return quote_; }
	QuantLib::Date tradeDate(void) { return tradeDate_; }
	double spot(void) const { return spot_; }
	//Setter
	void setQuote(QuantLib::Matrix& data) { quote_ = data; }
	void setQuote(std::vector<std::vector<double> > &data);
	void setMaturities(std::vector < QuantLib::Date> &data) { maturities_ = data; }
	void setMaturities(std::vector<int> &data)
	{
		maturities_.clear();
		for (size_t i = 0; i < data.size(); i++)
		{
			maturities_.push_back(tradeDate_ + data[i]);
		}
	}
private:
	string currency_;
	string exchange_;
	double spot_;
	QuantLib::Date tradeDate_;
	string underlyingAsset_;

	QuantLib::Array strikes_;
	std::vector<QuantLib::Date> maturities_;
	QuantLib::Matrix quote_;
};

#endif
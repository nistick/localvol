#include <boost/log/trivial.hpp>
#include <iostream>
#include <ql/quantlib.hpp>
#include <string>
#include <boost/format.hpp>
#include <sstream>
#include "CSVIterator.hpp"
#include "rawlocalvoldata.hpp"
#include "fixed_termstructures.hpp"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <regex>
#include "linspace.hpp"
#include <algorithm>
#include "blackprice.hpp"
#include "localvolfunction.hpp"

using boost::format;
using namespace boost::filesystem;
using std::regex;
using namespace LocalVolCalibration;

std::vector<path> getFiles(const path& p) {
	std::vector<path> ret;
	try {
		for (directory_entry& x : directory_iterator(p)) {
			if (is_regular_file(x.path()))ret.push_back(x.path());
		}
	}
	catch (filesystem_error &ex)
	{
		cout << ex.what() << endl;
	}
	return ret;
}

bool cond(const path& p) {
	try {
		std::string filename = p.filename().string();
		//BOOST_LOG_TRIVIAL(info) << p.extension().string();

		if (p.extension().string() == ".txt")
		{
			//BOOST_LOG_TRIVIAL(info) << filename.find("SD");
			if (filename.find("SD") != string::npos) {
				std::smatch m;
				regex pat("([0-9]+\\-[0-9]+\\-[0-9]+)");
				if (std::regex_search(filename, m, pat))
				{
					return m[0] > "2015-06-18";
				}
			}
		}
	}
	catch (std::exception & ex)
	{
		BOOST_LOG_TRIVIAL(error) << ex.what();
	}
	return false;
}

typedef struct calibration_opt {
	double smin = 0.1;
	double smax = 2.5;
	int ns = 1001;
} calibration_opt;

std::vector<int> getPosition(int i, FixedTermStructure& ts, RawLocalVolData& vs, Array& xs)
{
	std::vector<int> ret;
	QuantLib::Array ks = vs.spot() * vs.strikes();
	double forwardFactor = ts.forwardFactors()[i];
	double forward = vs.spot() * forwardFactor;
	double dx = xs[1] - xs[0];
	for (size_t i = 0; i < ks.size()-1; i++)
	{
		ret.push_back((int)(((log(ks[i + 1] / forward) + log(ks[i] / forward)) / 2.0 - xs[0]) / dx));
	}
	return ret;
}


int main(int argc, char** argv)
{
	try {
		BOOST_LOG_TRIVIAL(trace) << "A trace severity message";

		QuantLib::Date asOfDate(13, QuantLib::January, 2016);
		path topDir("../..");
		std::stringstream filepath;
		filepath << "data\\SD_KS200_Fixed_";
		filepath << QuantLib::io::formatted_date(asOfDate, "%Y%m%d") << ".csv";
		path rawlocalvolpath = topDir;
		rawlocalvolpath /= filepath.str();
		RawLocalVolData vs(rawlocalvolpath.string());

		filepath.str("");
		filepath << "data\\SD_KS200_Fixed_TS_";
		filepath << QuantLib::io::formatted_date(asOfDate, "%Y%m%d") << ".csv";
		path tsPath = topDir;
		tsPath /= filepath.str();
		FixedTermStructure ts(tsPath.string());

		double discount_factor[] = { 0.99586274186292134, 0.99200828409482189, 0.98829144396192603, 0.98454793565769261, 0.97705044962189502, 0.9695862601534454, 0.95371912219470023 };
		double forward_factor[] = { 1.0041538737249611, 1.0072800801346671, 1.0110683341459556, 1.0019633850935281, 1.0088748538461689, 1.0036730085727663, 1.006584837092847 };

		QuantLib::Array discountFactor(7);
		QuantLib::Array forwardFactor(7);
		for (int i = 0; i < 7; i++)
		{
			discountFactor[i] = static_cast<QuantLib::Real>(discount_factor[i]);
			forwardFactor[i] = static_cast<QuantLib::Real>(forward_factor[i]);
		}
		ts.setDiscountFactors(discountFactor);
		ts.setForwardFactors(forwardFactor);
		std::vector<int> indexs;
		for (int i = 0; i < vs.maturities().size(); i++)
		{
			int daysFromAsOfDate = vs.maturities()[i] - vs.tradeDate();
			if (daysFromAsOfDate > 80 && daysFromAsOfDate < 1100)
			{
				indexs.push_back(i);
			}
		}
		QuantLib::Matrix quote(indexs.size(), vs.strikes().size());
		for (size_t i = 0; i < indexs.size(); i++)
		{
			std::copy(vs.quote().row_begin(indexs[i]), vs.quote().row_end(indexs[i]), quote.row_begin(i));
		}
		vs.setQuote(quote);
		std::vector<int> days({ 91, 182, 273, 365, 547, 730, 1095 });
		vs.setMaturities(days);

		path datapath = topDir;
		datapath /= "src/vs";

		std::vector<path> files = getFiles(datapath);
		std::vector<path> filteredfiles;
		std::copy_if(files.begin(), files.end(), std::back_inserter(filteredfiles), cond);
		
		filteredfiles.clear();
		//path tPath("d:/Users/nistick/Documents/Finance/localvol/src/vs/IVSKS_SD_2016-01-13.txt");
		path tPath("../../src/vs/IVSKS_SD_2016-01-13.txt");
		filteredfiles.push_back(tPath);
		for (path& datafilepath : filteredfiles) {
			std::ifstream datafile(datafilepath.string());
			if (!datafile.good()) {
				throw "wrong file";
			}
			string headers;
			int row = 0;
			int column = 0;
			getline(datafile, headers);
			while (getline(datafile, headers)) {
				string col;
				stringstream headerstream(headers);
				column = 0;
				getline(headerstream, col, '\t');
				while (getline(headerstream, col, '\t')) {
					vs.quote()[row][column++] = atof(col.c_str());
				}
				row++;
			}

			QuantLib::Array ks = vs.strikes() * vs.spot();
			QuantLib::Matrix black_c(vs.quote().rows(), vs.quote().columns());
			QuantLib::Matrix black_p(vs.quote().rows(), vs.quote().columns());
			calibration_opt option;
			Array xs = linspace(log(option.smin), log(option.smax), option.ns);
			double dx = xs[1] - xs[0];
			QuantLib::DayCounter dc = QuantLib::Actual365Fixed();
			for (int i = 0; i < vs.maturities().size(); i++) {
				QuantLib::Array yearVSQuote(vs.quote().row_begin(i), vs.quote().row_end(i));
				QuantLib::Time t = dc.yearFraction(vs.tradeDate(), vs.maturities()[i]);
				QuantLib::Array stdDev = yearVSQuote * sqrt(t);
				double F = vs.spot() * ts.forwardFactors()[i];
				QuantLib::Array blackCallArray = price_black(ks, F, stdDev, 1);
				QuantLib::Array blackPutArray = price_black(ks, F, stdDev, -1);
				std::copy(blackCallArray.begin(), blackCallArray.end(), black_c.row_begin(i));
				std::copy(blackPutArray.begin(), blackPutArray.end(), black_p.row_begin(i));				
			}
			QuantLib::Matrix volProxy(black_c.rows(), black_c.rows(), 0.0);

			QuantLib::Array cs(xs.size());
			QuantLib::Array zeroArray(xs.size(), 0.0);
			QuantLib::Array oneArray(xs.size(), 1.0);

			std::transform(xs.begin(), xs.end(), cs.begin(), (double(*)(double))exp);
			cs = 1.0 - cs;
			std::transform(cs.begin(), cs.end(), zeroArray.begin(), cs.begin(), [](double a, double b) {return std::max(a, b); });
			QuantLib::Array ps(xs.size());
			std::transform(xs.begin(), xs.end(), ps.begin(), (double(*)(double))exp);
			ps = ps - 1.0;
			std::transform(ps.begin(), ps.end(), zeroArray.begin(), ps.begin(), [](double a, double b) {return std::max(a, b); });
			for (int i = 0; i < vs.maturities().size(); i++)
			{
				std::vector<int> pos = getPosition(i, ts, vs, xs);
				QuantLib::DayCounter dc = QuantLib::Actual365Fixed();
				double dt = i == 0 ? dc.yearFraction(vs.tradeDate(), vs.maturities()[i]) : dc.yearFraction(vs.maturities()[i - 1], vs.maturities()[i]);

				Size maxIterations = 1000;
				Size minStatIterations = 100;
				Real rootEpsilon = 1e-25;
				Real functionEpsilon = 1e-25;
				Real gradientNormEpsilon = 1e-25;
				QuantLib::Array black_c_a(black_c.row_begin(i), black_c.row_end(i));
				QuantLib::Array black_p_a(black_p.row_begin(i), black_p.row_end(i));
				LocalVolFunction localVolFunction(i, cs, ps, 
					black_c_a, black_p_a,

					pos, dt, xs, vs.spot(),vs, ts);
				QuantLib::BoundaryConstraint bcConstraint(0.0001, 2.0);

				QuantLib::EndCriteria myEndCrit(maxIterations, minStatIterations, rootEpsilon,
					functionEpsilon, gradientNormEpsilon);

				Array initValue(vs.quote().row_begin(i), vs.quote().row_end(i));

				QuantLib::Problem myProblem(localVolFunction, bcConstraint, initValue);

				QuantLib::LevenbergMarquardt levmar;

				QuantLib::EndCriteria::Type solvedCrit = levmar.minimize(myProblem, myEndCrit);

				std::cout <<"Criteria  :"<< solvedCrit << std::endl;
				std::cout <<"Root :"<< myProblem.currentValue() << std::endl;
				std::cout <<"Min  Function  Value :"<< myProblem.functionValue() << std::endl;

				Array tmp = localVolFunction.values(myProblem.currentValue());
				double aaa = localVolFunction.value(tmp);
				std::vector<Array> csps = localVolFunction.csps(myProblem.currentValue());
				cs = csps[0];
				ps = csps[1];
			}
		}
	}
	catch (std::exception &ex) {
		BOOST_LOG_TRIVIAL(error) << ex.what();
	}


	return 0;
}
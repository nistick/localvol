#include "localvolfunction.hpp"


namespace LocalVolCalibration
{

	LocalVolFunction::LocalVolFunction(
		size_t timeIndex,
		Array callSurface,
		Array putSurface,
		Array blackCall,
		Array blackPut,
		std::vector<int> pos,
		double dt,
		Array xs,
		double spot,
		RawLocalVolData vs,
		FixedTermStructure ts)
	{
		timeIndex_ = timeIndex;
		callSurface_ = callSurface;
		putSurface_ = putSurface;
		blackCall_ = blackCall;
		blackPut_ = blackPut;
		pos_ = pos;
		dt_ = dt;
		spot_ = spot;
		vs_ = vs;
		ts_ = ts;
		xs_ = xs;
	}

	Real LocalVolFunction::value(const Array& x) const {
		Array errSquare = QuantLib::Pow(x, 2);
		QuantLib::Statistics s;
		for (size_t i = 0; i < x.size(); i++)
		{
			s.add(errSquare[i], 1.0);
		}
		double rmse = sqrt(s.mean());
		return rmse;
	}

	std::vector<Array> LocalVolFunction::csps(const Array&x) const {
		Array vols(xs_.size());
		// use linear interpolated proxy vol
		Array ks = vs_.strikes() * vs_.spot();
		Array lks = QuantLib::Log(vs_.strikes() / ts_.forwardFactors()[timeIndex_]);
		QuantLib::LinearInterpolation fn(lks.begin(), lks.end(), x.begin());
		for (size_t i = 0; i < xs_.size(); i++) {
			if (xs_[i] <= lks[0]) {
				vols[i] = x[0];
			}
			else if (lks.back() <= xs_[i]) {
				vols[i] = x.back();
			}
			else {
				vols[i] = fn(xs_[i]);
			}
		}
		double dx = xs_[1] - xs_[0];
		Array mj = 0.5 * vols * vols * dt_ / dx;
		Array l = -mj * (1 / dx + 0.5);
		Array d = (1 + 2 * mj / dx);
		Array u = -mj * (1 / dx - 0.5);
		u[0] = l[l.size() - 1] = 0.0;
		d[0] = d[l.size() - 1] = 1.0;
		QuantLib::SparseMatrix A(d.size(), d.size(), 3 * d.size());
		for (size_t i = 0; i < d.size(); i++)
		{
			A(i, i) = d[i];
			if (i != 0)
			{
				A(i, i - 1) = l[i];
			}
			if (i != d.size() - 1)
			{
				A(i, i + 1) = u[i];
			}
		}

		QuantLib::SparseILUPreconditioner sparseSolver(A);
		std::vector<Array> ret;
		Array cx = sparseSolver.apply(callSurface_);
		Array px = sparseSolver.apply(putSurface_);
		ret.push_back(cx);
		ret.push_back(px);
		return ret;
	}

	QuantLib::Disposable<Array> LocalVolFunction::values(const Array &x) const {
		Array vols(xs_.size());
		// use linear interpolated proxy vol
		Array ks = vs_.strikes() * vs_.spot();
		Array lks = QuantLib::Log(vs_.strikes() / ts_.forwardFactors()[timeIndex_]);
		QuantLib::LinearInterpolation fn(lks.begin(), lks.end(), x.begin());
		for (size_t i = 0; i < xs_.size(); i++) {
			if (xs_[i] <= lks[0]) {
				vols[i] = x[0];
			}
			else if (lks.back() <= xs_[i]) {
				vols[i] = x.back();
			}
			else {
				vols[i] = fn(xs_[i]);
			}
		}
		double dx = xs_[1] - xs_[0];
		Array mj = 0.5 * vols * vols * dt_ / dx;
		Array l = -mj * (1 / dx + 0.5);
		Array d = (1 + 2 * mj / dx);
		Array u = -mj * (1 / dx - 0.5);
		u[0] = l[l.size() - 1] = 0.0;
		d[0] = d[l.size() - 1] = 1.0;
		QuantLib::SparseMatrix A(d.size(), d.size(), 3 * d.size());
		for (size_t i = 0; i < d.size(); i++)
		{
			A(i, i) = d[i];
			if (i != 0)
			{
				A(i, i - 1) = l[i];
			}
			if (i != d.size() - 1)
			{
				A(i, i + 1) = u[i];
			}
		}

		QuantLib::SparseILUPreconditioner sparseSolver(A);

		Array cx = sparseSolver.apply(callSurface_);
		Array px = sparseSolver.apply(putSurface_);
		QuantLib::LinearInterpolation cfun(xs_.begin(), xs_.end(), cx.begin());
		QuantLib::LinearInterpolation pfun(xs_.begin(), xs_.end(), px.begin());
		double F = spot_ * ts_.forwardFactors()[timeIndex_];
		Array cval(ks.size());
		for (size_t i = 0; i < ks.size(); i++)
		{
			cval[i] = cfun(QuantLib::Log(ks / F)[i]) * F;
		}
		Array pval(ks.size());
		for (size_t i = 0; i < ks.size(); i++)
		{
			pval[i] = pfun(QuantLib::Log(ks / F)[i]) * F;
		}
		Array err(ks.size());
		for (size_t i = 0; i < ks.size(); i++)
		{
			err[i] = ks[i] >= F ? cval[i] / blackCall_[i] - 1 : pval[i] / blackPut_[i] - 1;
		}

		return err;
	}
}
#ifndef localvolfunction_hpp
#define localvolfunction_hpp
#include <vector>
#include "fixed_termstructures.hpp"
#include "rawlocalvoldata.hpp"
#include "types.hpp"

namespace LocalVolCalibration
{
	class LocalVolFunction : public QuantLib::CostFunction {
	public:
		LocalVolFunction() {}
		LocalVolFunction(
			size_t timeIndex,
			Array callSurface,
			Array putSurface,
			Array blackCall,
			Array blackPut,
			std::vector<int> pos,
			double dt,
			Array xs,
			double spot,
			RawLocalVolData vs,
			FixedTermStructure ts
			);

		Real value(const Array& x) const;
		QuantLib::Disposable<Array> values(const Array&x) const;
		std::vector<Array> csps(const Array&x) const;
	private:
		size_t timeIndex_;
		Array callSurface_;
		Array putSurface_;
		Array blackCall_;
		Array blackPut_;
		std::vector<int> pos_;
		double dt_;
		Array xs_;
		double spot_;
		RawLocalVolData vs_;
		FixedTermStructure ts_;
	};
}

#endif
#ifndef blackprice_hpp
#define blackprice_hpp

#include <cmath>
#include <ql/quantlib.hpp>


/*
 * K : strike
 * F : forward
 * v : sigma * sqrt(t)
 * w : 1(call), -1(put)
 * return black option price
 */
QuantLib::Array price_black(QuantLib::Array strike,
	double forward,
	QuantLib::Array stdDev,
	int w = 1) {
	QuantLib::Array FK = forward / strike;
	QuantLib::Array logFK(strike.size());
	std::transform(FK.begin(), FK.end(), logFK.begin(), (double(*)(double))log);
	QuantLib::Array d1 = logFK / stdDev + 0.5 * stdDev;
	QuantLib::Array d2 = d1 - stdDev;
	QuantLib::Array cum_d1(strike.size());
	QuantLib::Array cum_d2(strike.size());
	QuantLib::CumulativeNormalDistribution cdf;
	d1 = w * d1;
	d2 = w * d2;
	std::transform(d1.begin(), d1.end(), cum_d1.begin(), cdf);
	std::transform(d2.begin(), d2.end(), cum_d2.begin(), cdf);
	return w * (forward * cum_d1 - strike * cum_d2);
}


#endif
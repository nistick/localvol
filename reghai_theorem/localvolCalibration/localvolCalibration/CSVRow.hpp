#ifndef csvrow_hpp
#define csvrow_hpp

#include <boost/log/trivial.hpp>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

class CSVRow
{
public:
	std::string const& operator[](std::size_t index) const
	{
		return m_data[index];
	}
	std::size_t size() const
	{
		return m_data.size();
	}

	std::string get_csv_column(std::istream & in)
	{
		std::string col;
		unsigned quotes = 0;
		char prev = 0;
		bool finis = false;
		for (int ch; !finis && (ch = in.get()) != EOF; ) {
			switch (ch) {
			case '"':
				++quotes;
				break;
			case ',':
				if (quotes == 0 || (prev == '"' && (quotes & 1) == 0)) {
					finis = true;
				}
				break;
			default:;
			}
			if(!finis)
				col += prev = ch;
		}
		return col;
	}

	void readNextRow(std::istream& str)
	{
		std::string         line;
		std::getline(str, line);

		std::stringstream   lineStream(line);
		std::string         cell;

		m_data.clear();
		if (line == "")
		{
			return;
		}
		/*while (std::getline(lineStream, cell, ','))
		{
			m_data.push_back(cell);
		}*/
		//BOOST_LOG_TRIVIAL(info) << lineStream.str();
		for (std::string col; lineStream; ) {
			col = get_csv_column(lineStream);
			//BOOST_LOG_TRIVIAL(info) << col;
			m_data.push_back(col);
		}
	}
private:
	std::vector<std::string>    m_data;
};

inline std::istream& operator>>(std::istream& str, CSVRow& data)
{
	data.readNextRow(str);
	return str;
}

#endif
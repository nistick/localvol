#include "fixed_termstructures.hpp"

FixedTermStructure::FixedTermStructure(string filepath)
{
	std::ifstream file(filepath);

	std::string line;
	std::getline(file, line);

	std::stringstream   lineStream(line);
	std::string         cell;


	while (std::getline(lineStream, cell, ';'))
	{
		std::stringstream cellStream(cell);
		std::string element;
		while (std::getline(cellStream, element, ':')) {
			if (boost::algorithm::ends_with(element, "Underlying asset"))
			{
				std::getline(cellStream, underlyingAsset_, ':');
			}
			else if (boost::algorithm::ends_with(element, "Trade date"))
			{
				std::string tradeDate;
				std::getline(cellStream, tradeDate, ',');
				std::getline(cellStream, tradeDate, ',');
				tradeDate_ = QuantLib::DateParser::parseFormatted(tradeDate, "%e %b %Y");

			}
			else if (boost::algorithm::ends_with(element, "Spot"))
			{
				std::string spot;
				std::getline(cellStream, spot, ':');
				spot_ = atof(spot.c_str());
			}
			else if (boost::algorithm::ends_with(element, "Currency"))
			{
				std::getline(cellStream, currency_, ':');
			}
			else if (boost::algorithm::ends_with(element, "Exchange"))
			{
				std::getline(cellStream, exchange_, ':');
			}
		}

	}

	CSVRow row;
	file >> row;
	std::vector<string> keys;
	for (size_t i = 1; i < row.size(); i++)
	{
		keys.push_back(row[i]);
		double factor = row[i].find("%")!= string::npos ? 100.0 : 1.0;
		factors_.insert(make_pair(row[i], factor));
		data_.insert(make_pair(row[i], vector<double>()));
	}
		
	while (file >> row)
	{
		if (row.size() == 0)
			break;
		maturities_.push_back(QuantLib::DateParser::parseFormatted(row[0], "%e-%b-%Y"));
		std::vector<double> temp;
		for (int i = 1; i < row.size(); i++) {
			data_[keys[i - 1]].push_back(stod(row[i]) / factors_[keys[i - 1]]);
		}
	}
}
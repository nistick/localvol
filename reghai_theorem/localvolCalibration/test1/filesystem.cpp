#include "stdafx.h"
#include <iostream>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/log/trivial.hpp>

int main() {
	boost::filesystem::path p(__FILE__);
	BOOST_LOG_TRIVIAL(info) << p.string() ;
	
	p /= "subdir";

	BOOST_LOG_TRIVIAL(info) << p.string();

	//std::string new_filename = p.leaf() + ".foo";
	//p.remove_leaf() /= new_filename;
	std::cout << p << '\n';

	return 0;
}
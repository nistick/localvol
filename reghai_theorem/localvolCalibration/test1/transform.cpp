#include "stdafx.h"
#include <cmath>

#include <vector>
#include <algorithm>
#include <iostream>
#include <ql/quantlib.hpp>

using namespace std;

int main()
{
	vector<double> aa;
	for (size_t i = 0; i < 10; i++)
	{
		aa.push_back(i);
	}

	transform(aa.begin(), aa.end(), aa.begin(), (float(*)(float))log);

	for (size_t i = 0; i < aa.size(); i++)
	{
		cout << aa[i] << endl;
	}
}
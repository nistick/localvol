#include "stdafx.h"
#include <iostream>
#include <regex>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	// 3개의 하위 표현식으로 구성되어 있다.
	regex pattern("(\\w+[\\w\\.]*)@(\\w+[\\w\\.]*)\\.([A-Za-z]+)");
	string str("SooKkaRak@gmail.com");

	// string 형식으로 match에 문자열을 넘기므로, 결과 집합 smatch 사용
	smatch m;

	//regex pat("([0-9]+\\-[0-9]+-\\-[0-9]+)");
	regex pat("([0-9]+\\-[0-9]+\\-[0-9]+)");
	string stri("IVSKS_SD_2015-12-18.txt");
	
	if (regex_search(stri, m, pat))
	{
		// 파이썬의 match 오브젝트와 유사하게
		// m[0]은 전체 결과 집합이 들어있고, m[1]~m[3]까지 개별 결과가 들어 있다.
		for (size_t i = 0; i < m.size(); i++)
		{
			cout << i << "번째 : " << m[i] << endl;
		}
	}
	else
	{
		cout << "NOT MATCH" << endl;
	}

	if (regex_match(str, m, pattern))
	{
		// 파이썬의 match 오브젝트와 유사하게
		// m[0]은 전체 결과 집합이 들어있고, m[1]~m[3]까지 개별 결과가 들어 있다.
		for (size_t i = 0; i < m.size(); i++)
		{
			cout << i << "번째 : " << m[i] << endl;
		}
	}
	else
	{
		cout << "NOT MATCH" << endl;
	}
}
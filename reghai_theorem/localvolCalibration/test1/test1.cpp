// test1.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <string>
#include <iostream>

int main()
{
	std::string orbits("365.24 29.53");
	std::string::size_type sz;     // alias of size_t

	double earth = std::stod(orbits, &sz);
	double moon = std::stod(orbits.substr(sz));
	std::cout << "The moon completes " << (earth / moon) << " orbits per Earth year.\n";
	return 0;


    return 0;
}


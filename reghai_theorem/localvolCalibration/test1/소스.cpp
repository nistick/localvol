#include "stdafx.h"
#include <boost/log/trivial.hpp>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>



class CSVRow
{
public:
	std::string const& operator[](std::size_t index) const
	{
		return m_data[index];
	}
	std::size_t size() const
	{
		return m_data.size();
	}
	void readNextRow(std::istream& str)
	{
		std::string         line;
		std::getline(str, line);

		std::stringstream   lineStream(line);
		std::string         cell;

		m_data.clear();
		while (std::getline(lineStream, cell, ','))
		{
			m_data.push_back(cell);
		}
	}
private:
	std::vector<std::string>    m_data;
};

std::istream& operator>>(std::istream& str, CSVRow& data)
{
	data.readNextRow(str);
	return str;
}
int main()
{
	BOOST_LOG_TRIVIAL(trace) << "A trace severity message";
	std::vector<int> aa({ 1,2,3,4 });
	for(int&x : aa)
		std::cout << x << std::endl;
	std::ifstream       file("D:\\Users\\nistick\\Documents\\Finance\\localvol\\data\\SD_KS200_Exchange_20160113.csv");

	CSVRow              row;
	while (file >> row)
	{
		std::cout << "4th Element(" << row[1] << ")\n";
	}
}
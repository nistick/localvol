#pragma warning(disable: 4996)

#include <ql/quantlib.hpp>
#include "costfunction.hpp"



using namespace QuantLib;

int main() {
	RawSVIParameter vogtParams(-0.0410, 0.1331, 0.3060, 0.3586, 0.4153);

	double eps = 1e-8;
	int maxIter = 10000;

	OneDimensionalPolynomialDegreeN sviDist(vogtParams);

	double t0 = 2.0;
	double dec = 0.0001;
	int steps = 1;
	SimulatedAnnealing<> sa(0.2, t0, dec, steps, MersenneTwisterUniformRng(42));
	Array initialGuess(1, 0.0);
	BoundaryConstraint boxConstrate(-1.5, 1.5);
	Problem myProblem(sviDist, boxConstrate, initialGuess);
	EndCriteria endCriteria(1000, 100, 1e-5, 1e-5, 1e-5);

	EndCriteria::Type result = sa.minimize(myProblem, endCriteria);

	std::cout << "found x=" << myProblem.currentValue()[0]
		<< " / y=" << myProblem.functionValue() 
		<< " with ret code " << result
		<< std::endl;

	return 0;
}


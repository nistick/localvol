#pragma warning(disable: 4996)

#include <ql/quantlib.hpp>
#include "costfunction.hpp"
#include "..\svi\linspace.hpp"
#include <fstream>


using namespace QuantLib;

int main() {
	RawSVIParameter vogtParams(-0.0410, 0.1331, 0.3060, 0.3586, 0.4153);

	//JW_SVI tempParams(0.01742625, -0.1752111, 0.6997381, 0.9, 0.0116249, 1);
	JW_SVI tempParams(0.01742625, -0.1752111, 0.6997381, 1.316798, 0.0116249, 1);

	Array ct = linspace(0.3493158, 1.316798, 1000);
	std::ofstream fp("test.txt");
	for each (double var in ct)
	{
		tempParams.setC(var);
		vogtParams = tempParams.toRawSVI();
		fp << vogtParams.distributionFunction
	}

	vogtParams = tempParams.toRawSVI();
	double strike = 0.8234413;
	cout << tempParams << endl;
	cout << vogtParams << endl;
	cout << vogtParams.distributionFunction(strike) << endl;
	//[1] - 0.006230621

	return 0;
}

#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright 2016  <@NISTICK-PC>
#
# Distributed under terms of the MIT license.

"""

"""
import logging
from svi.svi import SVI
logging.basicConfig(level=logging.INFO)

sviparams = SVI(0.04, 0.4, 0.1, -0.4, 0.1)
logging.info(sviparams)

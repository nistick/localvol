#include "natural_parameter.hpp"

NaturalParameter::NaturalParameter(double delta,
	double mu,
	double rho,
	double omega,
	double zeta)
{
	delta_ = delta;
	mu_ = mu;
	rho_ = rho;
	omega_ = omega;
	zeta_ = zeta;
}

double NaturalParameter::totalImpliedVariance(double strike) {
	double ret;

	ret = delta_ + omega_ / 2.0 * (1 + zeta_*rho_*(strike - mu_) +
		std::sqrt(std::pow(zeta_*(strike - mu_) + rho_, 2) + (1 - std::pow(rho_, 2))));

	return ret;
}
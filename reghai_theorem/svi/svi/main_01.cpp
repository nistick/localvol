﻿#pragma warning(disable:4996)
#include <iostream>
#include <fstream>
#include <functional>
#include <ql/quantlib.hpp>
#include "raw_parameter.hpp"
#include "jw_svi.hpp"
#include "natural_parameter.hpp"
#include "types.hpp"
#include "linspace.hpp"
#include "blackprice.hpp"

using namespace std;

int main(int argc, char** argv) {
	RawSVIParameter vogtParams(-0.0410, 0.1331, 0.3060, 0.3586, 0.4153);

	std::ofstream ofSmile("smile.csv");
	std::ofstream offuncG("g.csv");

	plotSmile(ofSmile, vogtParams);
	functionG(offuncG, vogtParams);

	JW_SVI vogtJW = vogtParams.jwSVI(1.0);
	JW_SVI vogtFixedJW = vogtJW;
	vogtFixedJW.setC(vogtJW.p() + 2 * vogtJW.psi());
	vogtFixedJW.setV_Tilde(vogtJW.v() * 4 * vogtJW.p()*vogtFixedJW.c() / pow(vogtJW.p() + vogtFixedJW.c(), 2));
	cout << vogtJW << endl;
	cout << vogtFixedJW << endl;
	RawSVIParameter vogtFixedParams = vogtFixedJW.toRawSVI();
	cout << vogtFixedParams << endl;
	Array strikes = linspace(-1, 1, 100);
	Array stdDev(strikes.size());
	std::transform(strikes.begin(), strikes.end(), stdDev.begin(), std::mem_fun(&RawSVIParameter::totalImpliedVariance)(&vogtFixedParams));
	
	cout << stdDev << endl;
	
	//std::transform(strikes.begin(), strikes.end(), stdDev.begin(), std::ptr_fun<Real, Real>(vogtParams.totalImpliedVariance));
	//std::transform(stdDev.begin(), stdDev.end(), stdDev.begin(), std::ptr_fun<Real, Real>(std::sqrt));
	Array callVals = price_black(strikes, 1, stdDev, 1);

	ofSmile.close();
	offuncG.close();
	return 0;
}
#ifndef linspace_hpp
#define linspace_hpp

#include <vector>
#include "types.hpp"

using namespace std;

//template <typename T = double>
//vector<T> linspace(T a, T b, size_t N) {
//	T h = (b - a) / static_cast<T>(N - 1);
//	vector<T> xs(N);
//	typename vector<T>::iterator x;
//	T val;
//	for (x = xs.begin(), val = a; x != xs.end(); ++x, val += h)
//		*x = val;
//	return xs;
//}

inline Array linspace(double a, double b, size_t N) {
	Array res(N);
	double h = (b - a) / static_cast<double>(N - 1);
	for (size_t i = 0; i < N; i++)
	{
		res[i] = a + h * i;
	}
	return res;
}

#endif
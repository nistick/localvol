#pragma once
#include <cmath>
#include <iostream>
#include <boost/math/special_functions/sign.hpp>

using namespace std;

class RawSVIParameter;

class JW_SVI {
public:
	JW_SVI() {}
	JW_SVI(double v,
		double psi,
		double p,
		double c,
		double v_tilde,
		double t);

	double choosingC();
	double choosingV_tilda();

	double v() const;
	double psi() const;
	double p() const;
	double c() const;
	double v_tilde() const;
	double t() const;

	void setV(double value);
	void setPsi(double value);
	void setP(double value);
	void setC(double value);
	void setV_Tilde(double value);
	void setT(double value);

	RawSVIParameter toRawSVI() const;

private:
	double v_;
	double psi_;
	double p_;
	double c_;
	double v_tilde_;
	double t_;
};

void plotSmile(ostream& os, const JW_SVI& jw);

ostream& operator<<(ostream& os, const JW_SVI& jw);
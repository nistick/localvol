#ifndef types_hpp
#define types_hpp
#include <ql/quantlib.hpp>

typedef QuantLib::SparseMatrix SparseMatrix;

typedef QuantLib::Matrix Matrix;

typedef QuantLib::Array Array;

typedef QuantLib::Real Real;

typedef QuantLib::Size Size;
#endif
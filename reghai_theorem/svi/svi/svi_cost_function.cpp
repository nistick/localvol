
#include "svi_cost_function.hpp"

SVIFunction::SVIFunction(
	JW_SVI jw_svi,
	Array strikes,
	Array callVals){
	jw_svi_ = jw_svi;
	strikes_ = strikes;
	callVals_ = callVals;
}

Real SVIFunction::finiteDifferenceEpsilon() const {
	return 1e-3;
}

Real SVIFunction::value(const Array& x) const {
	//cout << std::setprecision(20) << x << endl;
	Real ret;
	double c = x[0];
	double varmin = x[1];
	JW_SVI newJW_SVI = jw_svi_;
	newJW_SVI.setC(c);
	newJW_SVI.setV_Tilde(varmin);
	cout << newJW_SVI << endl;
	RawSVIParameter vogtNew = newJW_SVI.toRawSVI();
	Array stdDev(strikes_.size());
	Array callValsNew(strikes_.size());
	for (size_t i = 0; i < strikes_.size(); i++)
		callValsNew[i] = BSFormula(1.0, std::exp(strikes_[i]), 1.0, 0, std::sqrt(vogtNew.totalImpliedVariance(strikes_[i])));
	Array callDiff = callVals_ - callValsNew;
	Array ones(callDiff.size(), 1.0);
	callDiff *= callDiff;
	ret = QuantLib::DotProduct(callDiff, ones);
	
	OneDimensionalPolynomialDegreeN sviDist(vogtNew);
	QuantLib::BoundaryConstraint boxConstrate(-1.5, 1.5);
	Array initialGuess(1, 1.0);
	QuantLib::Problem myProblem(sviDist, boxConstrate, initialGuess);
	QuantLib::EndCriteria endCriteria(1000, 100, 1e-8, 1e-8, 1e-8);
	QuantLib::SimulatedAnnealing<> sa(0.2, 0.25, 0.0001, 1, QuantLib::MersenneTwisterUniformRng(42));

	QuantLib::EndCriteria::Type result = sa.minimize(myProblem, endCriteria);
	initialGuess[0] = myProblem.currentValue()[0];
	QuantLib::Problem P(sviDist, boxConstrate, initialGuess);
	QuantLib::ConjugateGradient optimizationMethod;
	QuantLib::EndCriteria::Type endCriteriaResult = optimizationMethod.minimize(P, endCriteria);

	bool completed = false;
	switch (endCriteriaResult) {
	case QuantLib::EndCriteria::MaxIterations:
	case QuantLib::EndCriteria::Unknown:
	case QuantLib::EndCriteria::None:
		completed = false;
	default:
		completed = true;
	}

	if (completed)
	{
		Array xMinCalculated = P.currentValue();
		Array yMinCalculated = P.values(xMinCalculated);
		cout << yMinCalculated << endl;
		ret += (100*std::pow(std::min(yMinCalculated[0], 0.0), 2));
	}
	else {
		cerr << "error in fungtion g" << endl;
	}
	cout << "Error : " << ret << endl;
	return ret;
}

QuantLib::Disposable<Array> SVIFunction::values(const Array &x) const {
	QL_REQUIRE(x.size() == 2, "svi paramertization is 2 dimension");
	Array y(1);
	y[0] = value(x);
	return y;
}


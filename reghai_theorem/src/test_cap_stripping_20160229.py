# -*- coding: ms949 -*-
from __future__ import division
import numpy as np
from scipy import interpolate
import scipy.stats as ss


class DiscountFactor(object):
    def __init__(self, days, spot, dfs):
        self._days = days
        self._spot = spot
        self._dfs = dfs
        self._interp = interpolate.interp1d(self._days/365, self._spot)

    def discount(self, t):
        r = self._interp(t)
        return np.exp(-r * t)

    def test(self):
        import numpy.testing
        tt = self._days / 365
        for i, t in enumerate(tt):
            df = self.discount(t)
            np.testing.assert_almost_equal(self._dfs[i], df)


def Bl(K, F, v, w):
    N = ss.norm.cdf
    d1 = (np.log(F/K) + v**2 / 2) / v
    d2 = d1 - v
    return w * (F * N(w * d1) - K * N(w * d2))


def compute_par_swap_rate(df, tau):
    Pa = df.discount(tau[0])
    Pb = df.discount(tau[-1])
    dt = tau[1:] - tau[:-1]
    return (Pa - Pb) / (dt * df.discount(tau[1:])).sum()


def compute_cap_black(k, df, tau, sigma, w=1):
    dt = tau[1:] - tau[:-1]
    dfs = df.discount(tau[1:])
    fwd = (df.discount(tau[:-1]) / df.discount(tau[1:]) - 1) / dt
    v = sigma * np.sqrt(tau[:-1])
    return (dfs * dt * Bl(k, fwd, v, w)).sum()



irs_data = '''days	spot	discount
0	0.0149996917892707	1.0000000000000000
2	0.0149996917892707	0.9999178132855170
3	0.0149996917892707	0.9998767224613060
94	0.0162396515649711	0.9958264670663040
186	0.0153891643063360	0.9921885199483320
277	0.0148948804362170	0.9887598617679100
367	0.0146215817792770	0.9854058422929760
553	0.0144219530533677	0.9783867407429120
732	0.0141714474437717	0.9719795181028530
1099	0.0141977731998812	0.9581519406705190
1463	0.0144017574715321	0.9439091088259670
1828	0.0146063378673873	0.9294597817691500
2193	0.0148894537046804	0.9144256631807440
2558	0.0151218067637333	0.8994453924933610
2926	0.0154345789935024	0.8836180754434940
3290	0.0157227948301868	0.8678637608189420
3654	0.0159323300827239	0.8525717563417200
4385	0.0165198606253710	0.8199887757190060
5481	0.0167769376681549	0.7772992634673250
7308	0.0170929725047633	0.7101819383413350'''.splitlines()

cap_quote = '''1Y	0.3081
2Y	0.3234
3Y	0.3346
4Y	0.3378
5Y	0.3332
7Y	0.3319
10Y	0.298
12Y	0.298
15Y	0.298'''
cap_tenor = np.array([d.split()[0][:-1] for d in cap_quote.splitlines()], dtype=int)
cap_quote = np.array([d.split()[1] for d in cap_quote.splitlines()], dtype=float)


value = np.array([d.split() for d in irs_data[1:]], dtype=float)
tenor = value[:, 0].astype(int)
spot = value[:, 1]
dfs = value[:, 2]

df = DiscountFactor(tenor, spot, dfs)

# print cap_tenor
td = np.arange(cap_tenor[-1]/0.25 + 1)
# print td
# print td * 0.25
tt = td * 0.25
dfs = df.discount(td * 0.25)
# print dfs

print cap_tenor
print cap_quote

get_tau_for_cap = lambda Tb, freq: np.linspace(1/freq, Tb, ((Tb - 1/freq)/(1/freq) + 1))

freq = 4
tau = get_tau_for_cap(1, freq=4)
print tau
k = compute_par_swap_rate(df, tau)
print k
print compute_cap_black(k, df, tau, cap_quote[0])

#TODO: implement caplet stripping code

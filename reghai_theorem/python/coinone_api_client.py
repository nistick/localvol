import requests


class CoinoneAPIClient:
    orderSort = lambda self,x: float(x['price'])

    def ticker(self, *args, **kwargs):
        resp = requests.get('https://api.coinone.co.kr/ticker/')
        return float(resp.json()['last'])
    def ticker_buy(self):
        resp = requests.get('https://api.coinone.co.kr/orderbook/')
        order = resp.json()['ask'] 
        order = sorted(order, key = self.orderSort)
        return float(order[0]['price'])

    def ticker_sell(self):
        resp = requests.get('https://api.coinone.co.kr/orderbook/')
        order = resp.json()['bid']
        order = sorted(order, reverse=True, key=self.orderSort)
        return float(order[0]['price'])


if __name__ == '__main__':
    client = CoinoneAPIClient()
    print(client.ticker('btc'))
    print(client.ticker_buy())
    print(client.ticker_sell())

from bithumb_api_client import BithumbAPIClient
from coinone_api_client import CoinoneAPIClient
from korbit_api_client import KorbitAPIClient
import time
import numpy as np
import datetime
from time import strftime, gmtime


if __name__ == '__main__':
    clients = {'Bithumb': BithumbAPIClient(),
            'Coinone': CoinoneAPIClient(),
            'Korbit': KorbitAPIClient()}
    client_names = ['Bithumb', 'Coinone', 'Korbit']

    while(True):
        prices = []
        #  print(strftime('%Y-%m-%d %H:%M:%S', gmtime()))
        print(strftime('%Y-%m-%d %H:%M:%S', time.localtime()))
        print('\t'.join(client_names))
        strTicker = ''
        for client in client_names:
            clientObj = clients[client]
            price = clientObj.ticker('btc')
            strTicker = strTicker + '%s\t' % price
            prices.append(float(price))
        print(strTicker)
        prices = np.array(prices)
        print('%s\t%s' % (client_names[np.argmax(prices)], client_names[np.argmin(prices)]))
        print('%f\t%f\t%f' % (np.amax(prices), np.amin(prices), np.amax(prices)-np.amin(prices)))
        time.sleep(3)


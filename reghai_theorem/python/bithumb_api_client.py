import requests


class BithumbAPIClient:
    orderSort = lambda self,x: float(x['price'])

    def ticker(self, *args, **kwargs):
        resp = requests.get('https://api.bithumb.com/public/ticker/BTC')
        return float(resp.json()['data']['closing_price'])

    def ticker_buy(self, currency='BTC'):
        resp = requests.get('https://api.bithumb.com/public/orderbook/%s' % currency)
        order = resp.json()['data']['asks'] 
        order = sorted(order, key = self.orderSort)
        return float(order[0]['price'])

    def ticker_sell(self, currency='BTC'):
        resp = requests.get('https://api.bithumb.com/public/orderbook/%s' % currency)
        order = resp.json()['data']['bids']
        order = sorted(order, reverse=True, key=self.orderSort)
        return float(order[0]['price'])


if __name__ == '__main__':
    client = BithumbAPIClient()
    print(client.ticker())
    print(client.ticker_buy())
    print(client.ticker_sell())

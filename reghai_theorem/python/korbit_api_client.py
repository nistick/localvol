import requests


class KorbitAPIClient:
    orderSort = lambda self,x: float(x[0])

    def ticker(self, currency):
        resp = requests.get('https://api.korbit.co.kr/v1/ticker/detailed')
        return float(resp.json()['last'])

    def ticker_buy(self, currency='btc_krw'):
        resp = requests.get(r'https://api.korbit.co.kr/v1/orderbook?currency_pair=%s&category=%s' % (currency, 'bid'))
        order = resp.json()['asks'] 
        order = sorted(order, key = self.orderSort)
        return float(order[0][0])

    def ticker_sell(self, currency='btc_krw'):
        resp = requests.get(r'https://api.korbit.co.kr/v1/orderbook?currency_pair=%s&category=%s' % (currency, 'ask'))
        order = resp.json()['bids']
        order = sorted(order, reverse=True, key=self.orderSort)
        return float(order[0][0])


if __name__ == '__main__':
    client = KorbitAPIClient()
    print(client.ticker('btc'))
    print(client.ticker_buy())
    print(client.ticker_sell())

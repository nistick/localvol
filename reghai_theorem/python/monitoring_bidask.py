from bithumb_api_client import BithumbAPIClient
from coinone_api_client import CoinoneAPIClient
from korbit_api_client import KorbitAPIClient
import time
import numpy as np
import datetime
from time import strftime, gmtime


if __name__ == '__main__':
    from colorama import init
    from colorama import Fore, Back, Style
    init(autoreset=True)
    clients = {'Bithumb': BithumbAPIClient(),
            'Coinone': CoinoneAPIClient(),
            'Korbit': KorbitAPIClient()}
    client_names = ['Bithumb', 'Coinone', 'Korbit']

    while(True):
        try:
            bids = []
            asks = []
            #  print(strftime('%Y-%m-%d %H:%M:%S', gmtime()))
            print(strftime('%Y-%m-%d %H:%M:%S', time.localtime()))
            print('       \tLast\tBid\tAsk')
            for client in client_names:
                clientObj = clients[client]
                price = clientObj.ticker('btc')
                bid = clientObj.ticker_buy()
                bids.append(bid)
                ask = clientObj.ticker_sell()
                asks.append(ask)
                line = '%s\t%f\t%f\t%f' % (client, price, bid, ask)
                print(Fore.LIGHTWHITE_EX + line)
            bids = np.array(bids)
            asks = np.array(asks)
            if(np.amax(bids)-np.amin(asks)> 13500.0):
                print(Fore.MAGENTA+ 'Min Diff Quotes %f' % (np.amin(bids)-np.amax(asks)))
            else:
                print(Fore.GREEN+ 'Min Diff Quotes %f' % (np.amin(bids)-np.amax(asks)))
            print(Fore.GREEN+ 'Bid : %s\t%f' %(client_names[np.argmin(bids)], np.amin(bids)))
            print(Fore.GREEN+ 'Ask : %s\t%f' %(client_names[np.argmax(asks)], np.amax(asks)))
        except:
            import sys
            print(sys.exc_info())

        #  print(strTicker)
        #  prices = np.array(prices)
        #  print('%s\t%s' % (client_names[np.argmax(prices)], client_names[np.argmin(prices)]))
        #  print('%f\t%f\t%f' % (np.amax(prices), np.amin(prices), np.amax(prices)-np.amin(prices)))
        #  print(Fore.BLUE + client_names[np.argmax(prices)] + '[SELL]')
        #  print(Fore.RED + client_names[np.argmin(prices)] + '[BUY]')
        #  diff = np.amax(prices)-np.amin(prices)
        #  if diff > 13500.0:
        #      print(Fore.LIGHTYELLOW_EX + 'Expected return ' + str(diff))
        #  else:
        #      print(Fore.WHITE + str(diff))
        time.sleep(15)


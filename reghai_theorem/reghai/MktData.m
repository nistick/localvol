classdef MktData < H_RootObject
    %MktData is the basic object that represents a market data
  
    
    properties(SetAccess = public)
        params;
    end
    
    methods
        function mktData = MktData(params)
            if nargin > 0
                mktData.params = params;
            end
        end
        
    end
    
end


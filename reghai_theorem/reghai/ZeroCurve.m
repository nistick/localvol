classdef ZeroCurve < MktData
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetAccess = public)
        convention;
    end
    
    methods
        % constructor
        function zeroCurve = ZeroCurve(MktData)
            if nargin > 0
                zeroCurve.params = MktData.params;
                zeroCurve.convention = MktData.params('convention');
            end     
        end
        
        % zero functor is a linear interpolator of zeroCurve
        function    zero = Zero(zeroCurve, t)
            rawData = zeroCurve.params('rawData');
            abscisses = rawData(:,1);
            ordinates = rawData(:,2);
            zero = H_interpolation(abscisses, ordinates,t,1);
        end
        
%         function    tAct365 = TAct365(zeroCurve, t)
%             timeData = zeroCurve.params('timeData');
%             abscisses = timeData(:,1);
%             ordinates = timeData(:,2);
%             tAct365 = H_interpolation(abscisses, ordinates,t,1);
%         end
%         
%         function    t30360 = T30360(zeroCurve, t)
%             timeData = zeroCurve.params('timeData');
%             abscisses = timeData(:,2);
%             ordinates = timeData(:,1);
%             t30360 = H_interpolation(abscisses, ordinates,t,1);
%         end
%         
        function    df = DF(zeroCurve, t)
%             df = exp(-zeroCurve.Zero(t)*zeroCurve.TAct365(t));
            df = exp(-zeroCurve.Zero(t)*t);
        end
        
            
    end
    
end


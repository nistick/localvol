# -*- coding: utf-8 -*-

"""

"""
import numpy as np
from TDMASolver import TDMAsolver
from proxytolocalvol import GetLocalVolFromProxy
from reghai import expmReghai
from container import Container
from log_conf import Logger

logger = Logger.logger


def tridiag(a, b, c, k1=-1, k2=0, k3=1):
    return np.diag(a, k1) + np.diag(b, k2) + np.diag(c, k3)


def Solve1DPDE(tvar, dT, dK, meshC, meshP, optParams):
    # %filling proxy grid vol
    proxy = np.zeros(meshC.size)
    proxy = GetLocalVolFromProxy(tvar, optParams.Ks, optParams['ipos'])
    # %filling end
    a = np.zeros(meshC.size)
    b = np.zeros(meshC.size)
    c = np.zeros(meshC.size)

    dK2 = dK*dK
    for i in range(1, meshC.size-1):
        a[i] = -0.5*dT*proxy[i]*proxy[i]*optParams.Ks[i]*optParams.Ks[i]/dK2
        b[i] = 1 + dT*proxy[i]*proxy[i]*optParams.Ks[i]*optParams.Ks[i]/dK2
        c[i] = -0.5*dT*proxy[i]*proxy[i]*optParams.Ks[i]*optParams.Ks[i]/dK2
    a[0] = 0
    a[-1] = 0
    b[0] = 1
    b[-1] = 1
    c[0] = 0
    c[-1] = 0
    newC = TDMAsolver(a, b, c, meshC)
    newP = TDMAsolver(a, b, c, meshP)
    return newC, newP


def Solve1DGF(tvar, dT, dK, lastProb, optParams):
    # %filling proxy grid vol
    proxy = GetLocalVolFromProxy(tvar, optParams.marketStrikes, optParams.Ks)
    logger.debug('VolProxy Result')
    logger.debug(proxy)
    # %filling end
    dK2 = dK*dK
    a = -0.5 * dT*proxy*proxy*optParams.Ks*optParams.Ks/dK2
    b = 1 + dT*proxy*proxy*optParams.Ks*optParams.Ks/dK2
    b_rg = dT*proxy*proxy*optParams.Ks*optParams.Ks/dK2
    c = -0.5*dT*proxy*proxy*optParams.Ks*optParams.Ks/dK2

    # % absorbing boundary condition
    a[0] = 0
    a[-1] = 0
    b[0] = 1
    b[-1] = 1
    b_rg[0] = 0
    b_rg[-1] = 0
    c[0] = 0
    c[-1] = 0
    a1 = a[1:]
    c1 = c[:-1]
    
    oneStepDt = optParams.params.oneStepDt
    NT = round(dT/oneStepDt)

    logger.debug('A1')
    logger.debug(a1)
    logger.debug('B_rg')
    logger.debug(b_rg)
    logger.debug('C1')
    logger.debug(c1)
    if optParams.params.GFType == -1:
        A_rg = tridiag(-a1/dT, -b_rg/dT, -c1/dT)
        A_exp1 = expmReghai(A_rg, dT)
        logger.debug('A_exp1')
        logger.debug(A_exp1)
        logger.debug('last probability')
        logger.debug(lastProb)
        newProb = np.matmul(lastProb, A_exp1)
        condProb = A_exp1
    elif optParams.params.GFType == 0:
        A_rg = tridiag(-a1*oneStepDt/dT, -b_rg*oneStepDt/dT, -c1*oneStepDt/dT)
        A_exp1 = np.linalg.expm(A_rg*NT)
        newProb = np.matmul(lastProb, A_exp1)
        condProb = A_exp1
    elif optParams.params.GFType == 1:
        A_rg = tridiag(-a1*oneStepDt/dT, -b_rg*oneStepDt/dT, -c1*oneStepDt/dT)
        A_exp1 = np.linalg.expm(A_rg*NT)
        newProb = np.matmul(lastProb, A_exp1)
        condProb = A_exp1
    elif optParams.params.GFType == 2:
        A_rg = tridiag(-a1*oneStepDt/dT, -b_rg*oneStepDt/dT, -c1*oneStepDt/dT)
        A_exp1 = np.linalg.expm(A_rg)
        newProb = lastProb
        condProb = np.eye(lastProb.size)
        for t in range(NT):
            newProb = np.matmul(newProb, A_exp1)
            condProb = A_exp1*A_exp1
    # elif optParams.params.GFType == 3:
        # #  % Test for HugeDecomposition for inverse matrix
        # [x, y, d] = self.HugeDecomp(lastProb.size, a, b, c)
        # A = tridiag(a1, b, c1)
        # oneStep = OneStepGF(size,x,y,d,A)
        # newProb = lastProb*oneStep
        # condProb = oneStep
    # else:
        # #  % rescale matrix element to smaller time step
        # a = a*oneStepDt/dT
        # b = (b-1)*oneStepDt/dT + 1.0
        # c = c*oneStepDt/dT
        # a1 = a[1:]
        # c1 = c[:-1]
    
        # #  % Test for HugeDecomposition for inverse matrix
        # [x, y, d] = self.HugeDecomp(lastProb.size, a, b, c)
        # A = tridiag(a1, b, c1)
        # oneStep = OneStepGF(size,x,y,d,A)
        # newProb = lastProb
        # #  %numberOfTimeStep
        # #  %oneStepDt = 1.0/365
        # for t in range(NT):
            # newProb = newProb*oneStep
            # condProb = oneStep*oneStep
    out = Container()
    out.newProb = newProb
    out.condProb = condProb
    return out

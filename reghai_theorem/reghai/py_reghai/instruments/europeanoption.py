

class EuropeanOption:

    def __init__(self, payoff, exercise):
        self.payoff = payoff
        self.exercise = exercise
        self.arguments = {'payoff': payoff, 'exercise': exercise}

    def setupArgument(self, arguments):
        self.engine.arguments = arguments

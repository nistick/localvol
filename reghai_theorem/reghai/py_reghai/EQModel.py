# -*- coding: utf-8 -*-

"""

"""

import math
import numpy as np
import QuantLib as ql
from dateutil.relativedelta import relativedelta
from Model import Model



class EQModel(Model):
    '''
    EQModel is the SuperClass of all EQModel
    properties: zeroCurve, dividendCurve, spot
    methods : Fwd, GetFwdDividends
    '''

    def __init__(self, mktData, modelParam):
        super(EQModel, self).__init__(mktData, modelParam)
        self.evaluationDate = mktData['evaluationDate']
        self.dc = ql.Actual365Fixed()

    def discountFactor(self, time):
        return np.exp(-np.interp(time, self.mktData['zeroCurve'].index,
                                 self.mktData['zeroCurve'].values)*time)
        pass

    # determinstic forward functor
    def forward(self, spotPrice, tDay):
        return spotPrice * self.forwardFactor(tDay)

    def forwardFactor(self, tDay):
        t = tDay/365.0
        df = self.discountFactor(t)
        return 1.0 / df * self.getFwdDividends(tDay) * \
                self.getFwdRepoRate(tDay)

    # determinstic GetFwdDividends functor
    def getFwdDividends(self, tDay):
        out = 1.0
        for idx, value in self.mktData['dividendCurve'].iteritems():
            if idx <= self.evaluationDate or \
                    (self.evaluationDate+relativedelta(days=tDay)) < idx:
                continue
            out = out * (1.0 - value)
        return out

    def getFwdRepoRate(self, tDay):
        last_date = self.evaluationDate
        repo_factor = 1.0
        for idx, value in self.mktData['repoRateCurve'].iteritems():
            if idx < self.evaluationDate:
                continue
            if (self.evaluationDate+relativedelta(days=tDay)) < idx:
                repo_factor = repo_factor * \
                              math.exp(value * ((self.evaluationDate+relativedelta(days=tDay)) - last_date).days/365.0)
                return repo_factor
            repo_factor = repo_factor * math.exp(value*((idx-last_date).days/365.0))
            last_date = idx
        return repo_factor


        # last_repo_rate = ordinates(end)
        # for i in range(len(dates)):
            # dt = abscisses(i)
            # if abscisses(i) <= 0 :
                # continue
            # last_repo_rate = ordinates(i)
            # if cum_dt >= tDay :
                # break
            # repo_factor = repo_factor*exp(ordinates(i)*(min(tDay,abscisses(i))-cum_dt)/365.0)
            # cum_dt = min(tDay,dt)
        # if cum_dt < tDay:
            # repo_factor = repo_factor*exp(last_repo_rate*(tDay-cum_dt)/365.0)
        # return repo_factor

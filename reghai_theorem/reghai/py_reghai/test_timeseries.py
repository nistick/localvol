#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright 2017  <@NISTICK-PC>
#
# Distributed under terms of the MIT license.

"""

"""

import json
from datetime import datetime
import logging
import pandas as pd
from dateutil.relativedelta import relativedelta


if __name__ == '__main__':
    logging.basicConfig(level = logging.INFO)

    datafile = 'hscei_20170202.json'
    with open(datafile, 'r') as fp:
        dataObj = json.load(fp)
    logging.info(dataObj.keys())
    evaluationDate = pd.Timestamp(dataObj['evaluationDate'])
    logging.info(evaluationDate)
    series = dataObj['dividendCurve'].split('\n')
    dates = [evaluationDate + relativedelta(days = int(d.split()[0])) for d in series]
    values = [float(d.split()[1]) for d in series]
    logging.info(dates)
    logging.info(values)
    dividendTS = pd.Series(values, dates)
    logging.info(dividendTS[:5])
    print(dividendTS.size)
    for idx, value in dividendTS.iteritems():
        print(idx)
        print(value)



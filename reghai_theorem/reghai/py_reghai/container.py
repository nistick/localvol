# -*- coding: utf-8 -*-


class Container:
    """
    Empty Classs
    """

    def __repr__(self):
        ret = ''
        for k in sorted(self.__dict__.keys()):
            ret = ret + "%s : %s\n" % (k, repr(self.__dict__[k]))
        return ret


if __name__ == '__main__':
    temp = Container()
    temp.a = range(10)
    temp.b = 'Good'
    temp.c = {'a': range(10), 'b': 'Good'}

    print(temp)

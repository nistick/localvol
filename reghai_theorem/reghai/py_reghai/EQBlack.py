# -*- coding: utf-8 -*-

"""
Equity Black Scholes Model
"""

from EQModel import EQModel
import numpy as np
from vollib import black
from vollib.black.greeks import analytical
from vollib.black.implied_volatility import \
        implied_volatility_of_undiscounted_option_price as impliedvol
import math
from localvolatilityproxy import FindVolProxy
from localvolatilityproxy import TargetFunctionVolProxyFPI
from proxytolocalvol import GetLocalVolFromProxy
from container import Container
from solve1dPDE import tridiag
from reghai import expmReghai
from log_conf import Logger

logger = Logger.logger


class EQBlack(EQModel):
    # UNTITLED Summary of this class goes here
    #   Detailed explanation goes here

    def inductPDEBackward(self, fromTime, toTime, lastP, params):
        idxNow = np.where(self.localVolSurface.expiry >= fromTime)[0][0] \
            if np.where(self.localVolSurface.expiry >= fromTime)[0].size != 0 \
            else self.localVolSurface.expiry.size-1
        logger.debug('From {0} To {1}'.format(fromTime, toTime))
        logger.debug('Now Index is %d' % idxNow)
        volProxy = self.localVolSurface.volProxy[idxNow, :]
        logger.debug('Local Vol Proxy')
        logger.debug(volProxy)
        # filling proxy grid vol
        logger.debug('strike level')
        logger.debug(params.marketStrikes[idxNow])
        # proxy = GetLocalVolFromProxy(volProxy, params.strike, params.Ks)
        proxy = GetLocalVolFromProxy(volProxy, params.marketStrikes[idxNow], params.Ks)
        logger.debug('Proxy result')
        logger.debug(proxy)
        # filling end
        dK = params.Ks[1] - params.Ks[0]
        dT = (fromTime - toTime)/365.0
        dK2 = dK*dK
        a = -0.5*dT*proxy*proxy*params.Ks*params.Ks/dK2
        b = 1 + dT*proxy*proxy*params.Ks*params.Ks/dK2
        b_rg = dT*proxy*proxy*params.Ks*params.Ks/dK2
        c = -0.5*dT*proxy*proxy*params.Ks*params.Ks/dK2

        # absorbing boundary condition
        a[0] = 0
        a[-1] = 0
        b[0] = 1
        b[-1] = 1
        b_rg[0] = 0
        b_rg[-1] = 0
        c[0] = 0
        c[-1] = 0
        a1 = a[1:]
        c1 = c[:-1]

        logger.debug('A1')
        logger.debug(a1)
        logger.debug('b_rg')
        logger.debug(b_rg)
        logger.debug('c1')
        logger.debug(c1)

        if params.GFType == -1:
            A_rg = tridiag(-a1/dT, -b_rg/dT, -c1/dT)
            A_exp1 = expmReghai(A_rg, dT)
            logger.debug('A Exp1')
            logger.debug(A_exp1)
            logger.debug('Last Probability')
            logger.debug(lastP)
            newP = np.matmul(A_exp1, lastP)
            # newP = np.matmul(lastP, A_exp1)
        # elif params.backGFType == 0
            # oneStepDt = params.oneStepDt
            # NT = round(dT/oneStepDt)
            # A_rg = full(gallery('tridiag',-a1*oneStepDt/dT,
                                # -b_rg*oneStepDt/dT,-c1*oneStepDt/dT))
            # A_exp1 = expmdemo1(A_rg*NT)
            # newP = A_exp1*lastP
        # elif params.backGFType == 1
            # oneStepDt = params.oneStepDt
            # NT = round(dT/oneStepDt)
            # A_rg = full(gallery('tridiag',-a1*oneStepDt/dT,
                                  # -b_rg*oneStepDt/dT,-c1*oneStepDt/dT))
            # A_exp1 = expm(A_rg*NT)
            # newP = A_exp1*lastP
        # elif params.backGFType == 2
            # oneStepDt = params.oneStepDt
            # NT = round(dT/oneStepDt)
            # A_rg = full(gallery('tridiag',-a1*oneStepDt/dT,-b_rg*oneStepDt/dT,-c1*oneStepDt/dT))
            # A_exp1 = expm(A_rg)
            # newP = lastP
            # for t=1:NT
                # newP = A_exp1*newP
        # elif params.backGFType == 3
            # a1 = a(2:size)
            # c1 = c(1:size-1)
            # tdma.size = length(lastP)
            # tdma.a = a
            # tdma.b = b
            # tdma.c = c
            # tdma.uu = zeros(tdma.size,1)
            # tdma.ll = zeros(tdma.size,1)
            # tdma.dd = zeros(tdma.size,1)
            # tdma.bupdated = 1
            # tdmaLU =  eqDupireSpotGF.LUDecompose(tdma)
            # newP = eqDupireSpotGF.TDMASolve(tdmaLU,lastP)
        # elif (params.backGFType == 4) || (params.backGFType == 14) || (params.backGFType == 24)
            # oneStepDt = params.oneStepDt
            # NT = round(dT/oneStepDt)
            # # rescale matrix element to smaller time step
            # a= a*oneStepDt/dT
            # b = (b-1)*oneStepDt/dT + 1.0
            # c = c*oneStepDt/dT
            
            # a1 = a(2:size)
            # c1 = c(1:size-1)
            
            # tdma.size = length(lastP)
            # tdma.a = a
            # tdma.b = b
            # tdma.c = c
            # tdma.uu = zeros(tdma.size,1)
            # tdma.ll = zeros(tdma.size,1)
            # tdma.dd = zeros(tdma.size,1)

            # tdma.bupdated = 1

            # tdmaLU =  eqDupireSpotGF.LUDecompose(tdma)
            
            # newP = lastP
            # for t=1:NT
                # newP = eqDupireSpotGF.TDMASolve(tdmaLU,newP)
        else:
            raise('unImplemented')
        return newP

    def computeBackwardPDEOne(self, maturity, strike, params):
        expiry = params.expiry
        modelX = params.Ks
        if strike <= 1.0:
            mesh = np.maximum(strike - modelX, 0.0)
        else:
            mesh = np.maximum(modelX - strike, 0.0)
        volExpiry = expiry[np.where(expiry <= maturity)]
        timeStep = set()
        timeStep.add(0.0)
        timeStep.update(volExpiry)
        timeStep.add(maturity)
        timeStep = sorted(list(timeStep))
        logger.debug('Time Step')
        logger.debug(timeStep)
        for i in range(len(timeStep)-1, 0, -1):
            mesh = self.inductPDEBackward(timeStep[i],
                                          timeStep[i-1],
                                          mesh,
                                          params)
        out = mesh[params.Pricingidx]
        logger.debug('maturity : {0}, strike : {1}, price : {2}'.format(maturity, strike, out))
        return out

    def computeBackwardPDEOTMTotal(self, params):
        expiry = params.expiry
        strikes = params.marketStrikes
        logger.debug('Market Strikes')
        logger.debug(strikes)
        pdeOTMPrices = np.zeros((expiry.size, strikes[0].size))
        for i in range(expiry.size):
            strikePerExpiry = strikes[i]
            for j in range(strikePerExpiry.size):
                pdeOTMPrices[i, j] = self.computeBackwardPDEOne(expiry[i], strikePerExpiry[j], params)
        out = pdeOTMPrices
        return out


    # def generateMarginalPerExpiry(self, condProb, params):
        # '''
        # generate marginal(cumulative distribution) at each event
        # date(per expiry)
        # using conditional transition Probability matrix
        # '''
        # Ks = params.Ks

        # # first row dealt separately
        # cumMat = np.asmatrix(np.ones((params.Ks, params.Ks)))
        # cumMat = np.tril(cumMat)
        # X = np.asmatrix(conProb[0, :])
        # firstRow = cumMat * X.transpose()
        
        # for i=2:nPoints
            # cumM = 0.0
            # Qa(i,i) = condProb(i,i)
            # for j=i-1:-1:1
                # Qa(i,i) = Qa(i,i) + condProb(i,j)
            # end

            # cumM = 0.0
            # for j=i-1:-1:1
                # cumM = cumM + condProb(i,j+1)
                # Qa(i,j) = Qa(i,i) - cumM
            # end

            # cumM2 = 0.0
            # for j=i+1:nPoints
                # cumM2 = cumM2 + condProb(i,j)
                # Qa(i,j) = Qa(i,i) + cumM2
        # return Qa
        

    # def generateMarginal(self, condProb, params):
        # Ks = params.Ks
        # marginal = list()
        # condProbPerExpiry = zeros(length(Ks),length(Ks))
        # marginalPerExpiry = zeros(length(Ks),length(Ks))

        # for i=1:params.expirySize
            # condProbPerExpiry(:,:) = condProb(i,:,:)
            # marginalPerExpiry = eqDupireSpotGF.generateMarginalPerExpiry(condProbPerExpiry,params)
            # marginal(i,:,:) = marginalPerExpiry(:,:)
        # return marginal
    def __init__(self, mktData, modelParam):
        super(EQBlack, self).__init__(mktData, modelParam)

    def vanilla(self, txDay, tpDay, spot, strike, sig, CPFlag):
        '''
        #BlackVanilla:a function that computes a blackCallAndPut price
        #arguments
        #txDay =(days)time to reset
        #tpDay =(days)time to pay
        #sig= implied volatility
        #strike  = caplet strike
        #CPFlag = Call Or Put
        '''
        fwd = self.forward(spot, txDay)
        tx = txDay/365.0
        tp = tpDay/365.0
        df = self.mktData['zeroCurve'].DF(tp)
        if CPFlag == 'C':
            vanilla = df * black.undiscounted_black(fwd, strike, sig, tx, 'c')
        elif CPFlag == 'P':
            vanilla = df * black.undiscounted_black(fwd, strike, sig, tx, 'p')
        return vanilla

    def calibration(self):
        volSurface = self.mktData['volatilitySurface']['data']
        expiry = self.mktData['volatilitySurface']['expiry']
        strike = self.mktData['volatilitySurface']['strike']
        spot = self.mktData['spot']
        logger.debug('Volatility Surface')
        logger.debug(volSurface)
        logger.debug('expiry')
        logger.debug(expiry)
        logger.debug('strike')
        logger.debug(strike)

        blackOTMPrices = np.zeros((expiry.size, strike.size))
        blackOTMPrices2 = np.zeros((expiry.size, strike.size))
        blackVol = np.zeros((expiry.size, strike.size))
        blackOTMVega = np.zeros((expiry.size, strike.size))

        # we add blackvol as matrix so that in the calibation we don't
        # need to do 2D interpolation again and again!!
        # also for the target black price too!!

        if self.modelParam['moneyNessType'] == 'spotMoneyNess':
            
            for k in range(expiry.size):
                forwardfactor = self.forwardFactor(expiry[k])
                fwd = self.forward(spot, expiry[k])
                df = self.discountFactor(expiry[k]/365.0)
                for j in range(strike.size):
                    blackVol[k, j] = volSurface[k, j]
                    fwdStrike = strike[j]/forwardfactor
                    if fwdStrike <= 1.0:
                        blackOTMPrices[k, j] = black.undiscounted_black(1.0, fwdStrike, blackVol[k, j], expiry[k]/365.0, 'p')
                        blackOTMPrices2[k, j] = black.undiscounted_black(1.0, fwdStrike, blackVol[k, j], expiry[k]/365.0, 'p')
                        blackOTMVega[k, j] = analytical.vega('p', 1.0, fwdStrike, expiry[k]/365.0, 0.0, blackVol[k, j]) * 100.0
                    else:
                        blackOTMPrices[k, j] = black.undiscounted_black(1.0, fwdStrike, blackVol[k, j], expiry[k]/365.0, 'c')
                        blackOTMPrices2[k, j] = black.undiscounted_black(1.0, fwdStrike, blackVol[k, j], expiry[k]/365.0, 'c')
                        blackOTMVega[k, j] = analytical.vega('c', 1.0, fwdStrike, expiry[k]/365.0, 0.0, blackVol[k, j]) * 100.0
        else:
            for k in range(expiry.size):
                    fwd = self.forward(spot, expiry[k])
                    df = self.discountFactor(expiry[k]/365.0)
                    for j in range(strike.size):
                        blackVol[k, j] = volSurface[k, j]
                        if strike[j] <= 1:
                            blackOTMPrices[k, j] = self.vanilla(expiry[k],expiry[k],spot,fwd*strike[j],volSurface.vol(expiry[k],strike[j]),'P')/fwd/df
                        else:
                            blackOTMPrices[k, j] = self.vanilla(expiry[k],expiry[k],spot,fwd*strike[j],volSurface.vol(expiry[k],strike[j]),'C')/fwd/df
        logger.debug('Market Option Price')
        logger.debug(blackOTMPrices)

        params = Container()
        params.volSurface = volSurface
        params.expiry = expiry
        params.strike = strike
        params.blackOTMPrices = blackOTMPrices
        params.blackVol = blackVol

        # vol proxy calibration grid setting
        params.Kmin = self.modelParam['Kmin']
        params.Kmax = self.modelParam['Kmax']
        params.Ns   = self.modelParam['Ns']
        params.dK   = self.modelParam['dK']
        params.Ks   = np.linspace(params.Kmin, params.Kmax, params.Ns, endpoint=True)

        # current spot on the grid
        params.Pricingidx = int(math.floor((1.0-params.Ks[0])/params.dK))
        logger.debug('params.Pricingidx : {0}'.format(params.Pricingidx))

        # targetType : 1 = implied vol diff
        #              2 = price relative error
        params.targetType = 1

        # GreenFunctionType : 1 = matrix exponential moler higham
        #          2 = one step matrix exponential moler higham & multiplication from
        #          3 = one step GF & multiplication from right Andreasen & Huge
        params.GFType = self.modelParam['GFType']

        #  backGFType : 1 = forward FD(GF)
        #               2 = implicit FD
        params.backGFType = self.modelParam['backGFType']
        params.mcOneTimeStep = self.modelParam['mcOneTimeStep']
        params.pricingSchemeType = self.modelParam['pricingSchemeType']
        params.NMC = self.modelParam['NMC']
        params.oneStepDt = 1.0/365.0
        params.numOfCutoffTenor = expiry.size
        params.lowerCutoffVector = [0] * expiry.size
        params.upperCutoffVector = [2] * expiry.size

        # %output vol proxy
        out = Container()
        out.volProxy = np.zeros((expiry.size, strike.size))
        out.ipos = np.zeros((expiry.size, strike.size))
        out.probMesh = np.zeros((expiry.size, params.Ns))
        out.condProb = np.zeros((params.expiry.size, params.Ns, params.Ns))

        # %initialze the intial probability distribution in the grid
        initProb = np.zeros(params.Ns)
        initProb[int(params.Pricingidx)] = 1.0

        out.blackOTMPrices = blackOTMPrices
        out.blackOTMPrices2 = blackOTMPrices2
        out.blackVol = blackVol
        out.blackOTMVega = blackOTMVega
        out.marketImpVol = np.zeros((expiry.size, strike.size))
        out.modelImpVol = np.zeros((expiry.size, strike.size))
        out.interpBlackPrices = np.zeros((expiry.size, strike.size))
        out.dupireOTMPrices = np.zeros((expiry.size, strike.size))
        out.dupireBlackVol = np.zeros((expiry.size, strike.size))
        out.volError = np.zeros((expiry.size, strike.size))
        out.priceRe = np.zeros((expiry.size, strike.size))
        out.priceReOrig = np.zeros((expiry.size, strike.size))
        out.marketStrikes = np.zeros((expiry.size, strike.size))
        out.Ks = params.Ks
        out.nIter = np.zeros(expiry.size)
        out.localVol = np.zeros((expiry.size, len(out.Ks)))
        out.rmseTotal = 0.0
        out.fwdVolMseTotal = 0.0
        calibrationType = self.modelParam['calibrationType']

        if calibrationType == 'bootstrap':
            # %vol proxy calibration
            lastProb = initProb
            for i in range(expiry.size):
                logger.debug('{0}th Expiry'.format(i+1))
                dT = expiry[i]/365 if i == 0 else (expiry[i]-expiry[i-1])/365.0
                bootStrapOut = FindVolProxy(self, i, dT, lastProb,
                                            params,
                                            TargetFunctionVolProxyFPI)
                logger.debug('Volatility Proxy')
                logger.debug(bootStrapOut.volProxy)
                for j in range(strike.size):
                    out.volProxy[i, j] = bootStrapOut.volProxy[j]
                    out.ipos[i, j]     = bootStrapOut.ipos[j]
                    out.volError[i, j] = bootStrapOut.volError[j]
                    out.marketImpVol[i, j] =  bootStrapOut.marketImpVol[j]
                    out.modelImpVol[i, j] =  bootStrapOut.modelImpVol[j]
                    out.interpBlackPrices[i, j] = bootStrapOut.interpBlackPrices[j]
                    out.dupireOTMPrices[i, j] = bootStrapOut.dupireOTMPrices[j]
                    out.priceRe[i, j] = out.dupireOTMPrices[i, j]/out.interpBlackPrices[i, j]-1.0
                    out.priceReOrig[i, j] = out.dupireOTMPrices[i, j]/out.blackOTMPrices[i, j]-1.0
                    out.marketStrikes[i, j] = bootStrapOut.marketStrikes[j]

                out.nIter[i] = bootStrapOut.nIter
                tempLocalVol = GetLocalVolFromProxy(bootStrapOut.volProxy, bootStrapOut.marketStrikes, out.Ks)
                out.localVol[i, :] = tempLocalVol
                out.probMesh[i, :] = bootStrapOut.lastProb
                out.condProb[i] = bootStrapOut.condProb
                #  % Initial Payoff for the next period
                lastProb = bootStrapOut.lastProb
            logger.debug('Local Volatility')
            logger.debug(out.localVol)
            N = 0
            for j in range(expiry.size):
                for k in range(strike.size):
                    out.rmseTotal = out.rmseTotal + out.priceReOrig[j, k]*out.priceReOrig[j, k]
                    out.fwdVolMseTotal = out.fwdVolMseTotal + out.volError[j, k]*out.volError[j, k]
                    N=N+1

            out.rmseTotal = math.sqrt(out.rmseTotal/N)
            out.fwdVolMseTotal = math.sqrt(out.fwdVolMseTotal/N)

            #  % assigning the calibrated volProxy to the local vol
            #  % surface
            self.localVolSurface = Container()
            self.localVolSurface.volProxy = out.volProxy
            self.localVolSurface.ipos = out.ipos
            self.localVolSurface.localVol = out.localVol
            self.localVolSurface.probMesh = out.probMesh
            self.localVolSurface.condProb = out.condProb

            #  % generate marginal(cumulative) distribution for mcmc
            #  % simulation

            # self.localVolSurface.marginal = self.generateMarginal(out.condProb,params)
            self.localVolSurface.Ks = out.Ks
            self.localVolSurface.expiry = params.expiry
            self.localVolSurface.strike = params.strike
            self.localVolSurface.marketStrikes = out.marketStrikes

            #  % calibration gives model schedule, for later use
            #  % general products such as autocallables..

            self.modelSchedule = self.localVolSurface.expiry
            params.marketStrikes = out.marketStrikes
            #  calculate backward scheme for given target
            #  compareforwardMCFlag = 1 for one step euler scheme
            #  compareforwardMCFlag = 2 for daily euler scheme
            #  compareforwardMCFlag = 3 for markov chain monte carlo
            pricingSchemeType = params.pricingSchemeType

            if pricingSchemeType == 1:
                out.pdeOTMPrices = self.computeBackwardPDEOTMTotal(params)
                logger.debug('Backward FDM Price')
                logger.debug(out.pdeOTMPrices)
                logger.debug('Dupire FDM Price')
                logger.debug(out.dupireOTMPrices)
                out.backwardVol = np.zeros((expiry.size, strike.size))
                out.backVolError = np.zeros((expiry.size, strike.size))
                out.backPriceRmseTotal = 0.0
                out.backVolMseTotal = 0.0
                out.backRe = out.pdeOTMPrices/out.dupireOTMPrices - 1.0
                for i in range(expiry.size):
                    for j in range(strike.size):
                        if out.marketStrikes[i, j] <= 1.0:
                            out.backwardVol[i, j] = impliedvol(out.pdeOTMPrices[i, j],1,out.marketStrikes[i, j],params.expiry[i]/365.0,'p')
                        else:
                            out.backwardVol[i, j] = impliedvol(out.pdeOTMPrices[i, j],1,out.marketStrikes[i, j],params.expiry[i]/365.0,'c')
                        out.backVolError[i, j] = out.backwardVol[i, j] - out.marketImpVol[i, j]
                N=0
                for j in range(expiry.size):
                    for k in range(strike.size):
                        out.backPriceRmseTotal = out.backPriceRmseTotal + out.backRe[j, k]*out.backRe[j, k]
                        out.backVolMseTotal = out.backVolMseTotal +out.backVolError[j, k]*out.backVolError[j, k]
                        N=N+1

                out.backPriceRmseTotal = math.sqrt(out.backPriceRmseTotal/N)
                out.backVolMseTotal= math.sqrt(out.backVolMseTotal/N)

            elif pricingSchemeType == 3:
                out.mcOTMPrices = self.computeForwardMCOTMTotal(params)
                out.backRe = np.zeros(expiry.size,strike.size)
                out.backwardVol = np.zeros(expiry.size,strike.size)
                out.backVolError = np.zeros(expiry.size,strike.size)
                out.backPriceRmseTotal = 0.0
                out.backVolMseTotal = 0.0
                for i in range(expiry.size):
                    for j in range(strike.size):
                        out.backRe[i, j] = out.mcOTMPrices[i, j]/out.dupireOTMPrices[i, j]-1
                        if out.marketStrikes[i, j] <=1.0:
                            out.backwardVol[i, j] = impliedvol(out.mcOTMPrices[i, j],1,out.marketStrikes[i, j],params['expiry'][i]/365.0,'p')
                        else:
                            out.backwardVol[i, j] = impliedvol(out.mcOTMPrices[i, j],1,out.marketStrikes[i, j],params['expiry'][i]/365.0,'c')
                        out.backVolError[i, j] = out.backwardVol[i, j] - out.marketImpVol[i, j]
                N=0
                for j in range(expiry.size):
                    for k in range(strike.size):
                        out.backPriceRmseTotal = out.backPriceRmseTotal + out.backRe[j, k]*out.backRe[j, k]
                        out.backVolMseTotal = out.backVolMseTotal +out.backVolError[j, k]*out.backVolError[j, k]
                        N=N+1

                out.backPriceRmseTotal = math.sqrt(out.backPriceRmseTotal/N)
                out.backVolMseTotal = math.sqrt(out.backVolMseTotal/N)
            elif pricingSchemeType == 4:
                out.mcOTMPrices = self.computeForwardMCMCOTMTotal(params)
                out.backRe = np.zeros((expiry.size,strike.size))
                out.backwardVol = np.zeros((expiry.size,strike.size))
                out.backVolError = np.zeros((expiry.size,strike.size))
                out.backPriceRmseTotal = 0.0
                out.backVolMseTotal = 0.0
                for i in range(expiry.size):
                    for j in range(strike.size):
                        out.backRe[i, j] = out.mcOTMPrices[i, j]/out.dupireOTMPrices[i, j]-1
                        if out.marketStrikes[i, j] <=1.0:
                            out.backwardVol[i, j] = impliedvol(out.mcOTMPrices[i, j],1,out.marketStrikes[i, j],params.expiry[i]/365.0,'p')
                        else:
                            out.backwardVol[i, j] = impliedvol(out.mcOTMPrices[i, j],1,out.marketStrikes[i, j],params.expiry[i]/365.0,'c')
                        out.backVolError[i, j] = out.backwardVol[i, j] - out.marketImpVol[i, j]
                N = 0
                for j in range(expiry.size):
                    for k in range(strike.size):
                        out.backPriceRmseTotal = out.backPriceRmseTotal + out.backRe[j, k]*out.backRe[j, k]
                        out.backVolMseTotal = out.backVolMseTotal +out.backVolError[j, k]*out.backVolError[j, k]
                        N=N+1

                out.backPriceRmseTotal = math.sqrt(out.backPriceRmseTotal/N)
                out.backVolMseTotal = math.sqrt(out.backVolMseTotal/N)
            else:
                raise ValueError('unImplemented')
            return out

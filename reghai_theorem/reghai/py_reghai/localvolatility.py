# -*- coding: utf-8 -*-
from log_conf import Logger
from volatilitysurface import VolatilitySurface

logger = Logger.logger

"""
Local Volatility Surface
"""


class LocalVolatility(VolatilitySurface):
    def __init__(self, expiry, strike, data):
        VolatilitySurface.__init__(expiry, strike, data)

    def localVolatility(expiry, strike):
        pass


# -*- coding: utf-8 -*-
#
# Copyright 2017  <@NISTICK-PC>
#
# Distributed under terms of the MIT license.

"""
Volatility Surface
"""

from surface import Surface


class VolatilitySurface(Surface):
    def __init__(self, expiry, strike, data):
        self.expiry = expiry
        self.strike = strike
        self.data = data

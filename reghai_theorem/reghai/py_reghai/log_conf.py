# -*- coding: utf-8 -*-

"""

"""

import logging.config


def singlton(cls):
    instances = {}

    def get_instance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return get_instance()


@singlton
class Logger():
    def __init__(self):
        logging.config.fileConfig('logging.conf')
        self.logger = logging.getLogger('root')

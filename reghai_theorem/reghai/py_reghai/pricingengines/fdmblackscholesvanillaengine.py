
from TDMASolver import TDMAsolver


class FDMBlackScholesVanillaEngine:

    def __init__(self, process, timeGrid, spaceGrid, fdmScheme, localVol):
        self.process = process
        self.timeGrid = timeGrid
        self.spaceGrid = spaceGrid
        self.fdmScheme = fdmScheme
        self.localVol = localVol

    def NPV(self):
        npv = 0.0
        prevT = self.argument.exercise.maturity
        meshProb = self.arguments.payoff(self.spaceGrid)
        for t in self.timeGrid:
            dT = prevT - t
            # filling proxy grid vol
            proxy = self.localVol(self.spaceGrid, t)
            # %filling end
            dK = self.spaceGrid[1] - self.spaceGrid[0]

            dK2 = dK*dK
            a = -0.5 * dT * proxy*proxy*self.spaceGrid*self.spaceGrid/dK2
            b = 1 + dT * proxy*proxy*self.spaceGrid*self.spaceGrid/dK2
            c = -0.5 * dT * proxy*proxy*self.spaceGrid*self.spaceGrid/dK2
            a[0] = 0
            a[-1] = 0
            b[0] = 1
            b[-1] = 1
            c[0] = 0
            c[-1] = 0
            meshProb = TDMAsolver(a, b, c, meshProb)
            prevT = t

        return npv

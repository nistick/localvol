# -*- coding: utf-8 -*-

"""
Abstract Class of Models
"""


class Model(object):
    # Model is the basic object that represents a pricing model
    mktData = None
    modelParams = None

    def __init__(self, mktData, modelParam):
        setattr(self, 'mktData', mktData)
        setattr(self, 'modelParam', modelParam)

    def variance(self, sig, tau, order=1):
        if order == 1:
            return self.variance1(sig, tau)
        elif order == 2:
            return self.variance2(sig, tau)

    @classmethod
    def variance1(cls, sig, tau):
        tidx = 0
        for i in range(len(sig.tenor)):
            if tau <= sig.tenor[i]:
                tidx = i
                break
        summation = 0.0
        prev_t = 0.0
        curr_t = 0.0
        for i in range(tidx):
            if i < tidx:
                curr_t = sig.tenor[i]
            else:
                curr_t = tau
            summation = summation + sig.quote(i)*(curr_t-prev_t)
            prev_t = sig.tenor[i]
        return summation

    @classmethod
    def variance2(cls, sig, tau):
        tidx = 0
        for i in range(sig.tenor):
            if tau <= sig.tenor[i]:
                tidx = i
                break
        summation = 0.0
        prev_t = 0.0
        curr_t = 0.0
        for i in range(tidx):
            if i < tidx:
                curr_t = sig.tenor[i]
            else:
                curr_t = tau
            summation = summation + sig.quote(i)*sig.quote(i)*(curr_t-prev_t)
            prev_t = sig.tenor[i]
        return summation

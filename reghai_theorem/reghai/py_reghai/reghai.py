# -*- coding: utf-8 -*-
"""
Reghai Exponential Matrix

"""
import math
import numpy as np


def expmReghai(A, dT):
    A = A * dT
    n = math.ceil(math.log(np.linalg.norm(A, np.inf), 2))
    n = n + 1
    A = A / 2**n
    Aexp = np.eye(A.shape[0]) + A
    Aexp = np.linalg.matrix_power(Aexp, int(2**n))
    return Aexp

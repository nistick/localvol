clc
clear
%% import and construct a zeroCurve
rawData = [0.24384 	0.009941541
0.49589 	0.010686783
0.74795 	0.011586512
1.00000 	0.012587537
2.00548 	0.015802717
3.00274 	0.01823147
5.00822 	0.021509447
7.00274 	0.023483567
10.00548 	0.025501555
];

mktData = containers.Map({'rawData','asOfDate','ccy','name'}, ...
                        {rawData,'20170202','HKD','HKDZero'});
zeroCurveMktData = MktData(mktData);
zeroCurveMktData.params('convention') = {};
zeroCurve =  ZeroCurve(zeroCurveMktData);
AllMktData =containers.Map('zeroCurve',zeroCurve);
%% import and construct a spot For HSCEI
rawData = 9696.32;
mktData = containers.Map({'rawData','asOfDate','ccy','name'}, ...
                        {rawData,'20170202','HKD','HSCEISpot'});
spotMktData = MktData(mktData);
AllMktData('spot') = spotMktData;
%% import and construct an impliedVolSurfaceFor HSCEI
rawData = [0	0.5	0.55	0.6	0.65	0.7	0.75	0.8	0.85	0.9	0.95	1	1.05	1.1	1.15	1.2	1.25	1.3	1.35	1.4	1.45	1.5
30	0.451335563	0.421576803	0.39231798	0.363325027	0.33436084	0.304953796	0.274741257	0.243266202	0.209390283	0.174585421	0.14557406	0.152422127	0.17772979	0.202492912	0.224126521	0.243631498	0.261022209	0.276809665	0.291252494	0.30457332	0.316915125
91	0.398596347	0.372611808	0.34701554	0.321866152	0.297018816	0.272328278	0.247734099	0.223347272	0.199954663	0.179890804	0.165622745	0.160985465	0.165693471	0.175633412	0.187952142	0.20080785	0.213403257	0.225400279	0.236731473	0.247420171	0.257433506
182	0.379349112	0.354710441	0.330258569	0.306632306	0.283970466	0.262338336	0.242006452	0.223493368	0.207742167	0.19550094	0.187593128	0.184124385	0.184736133	0.188540615	0.194418208	0.201676353	0.209649379	0.217934538	0.226253911	0.234498156	0.242466573
273	0.378412332	0.354379105	0.330293683	0.307303389	0.285654343	0.265483152	0.24711511	0.231020486	0.217768617	0.207737932	0.200911475	0.197214212	0.196202418	0.197308021	0.199977074	0.203740108	0.208241383	0.2131948	0.218396849	0.223754625	0.229056019
365	0.382685574	0.358490237	0.334184706	0.311078486	0.289565475	0.269804812	0.252164328	0.237102621	0.224966948	0.215909999	0.209703961	0.206054339	0.204490833	0.204536671	0.205761222	0.207840194	0.210511813	0.213582366	0.216903102	0.220404601	0.22393693
547	0.359144844	0.338554605	0.318056687	0.298991707	0.281710006	0.266188625	0.252577586	0.24100204	0.231502922	0.224077763	0.218545258	0.214739727	0.212429196	0.211374502	0.211337272	0.212101113	0.213497404	0.21537523	0.217602419	0.220127286	0.222772156
730	0.350517969	0.332118126	0.313770121	0.296825036	0.281589391	0.267977593	0.256059323	0.245869433	0.237383191	0.230544833	0.225204393	0.221227365	0.21843922	0.216668377	0.215747273	0.215518843	0.215857041	0.216648342	0.217789206	0.219234362	0.220847438
1095	0.3317215	0.316705188	0.301836772	0.288251505	0.276149211	0.265408484	0.256008205	0.247901009	0.241025001	0.235299952	0.230610385	0.226863886	0.223948218	0.221756217	0.220188895	0.219153844	0.218566602	0.218356901	0.218456372	0.218836083	0.219393388
];

mktData = containers.Map({'rawData','asOfDate','moneyNessType','ccy','name'}, ...
                        {rawData,'20170202','spotMoneyNess','HKD','HSCEIVolSurface'});
impliedVolSurfaceMktData = MktData(mktData);
impliedVolSurface = EQVolSurface(impliedVolSurfaceMktData);
% impliedVolSurface.params('moneyNessType') = 'fwdMoneyNess';
AllMktData('impliedVolSurface') = impliedVolSurface;

%% import and construct a dividend CurveFor HSCEI
rawData = [102	0.000346059
109	0.000989128
116	0.000705082
119	0.000568638
123	0.002695837
130	0.001533829
137	3.65396E-05
140	0.000637623
141	0.005786701
144	0.006990157
147	0.005584469
148	0.000883768
151	0.003245004
158	7.71014E-05
175	0.000562038
217	0.000394521
221	0.000553437
224	0.000898114
243	0.000100605
279	1.25821E-06
291	2.47517E-07
292	3.30022E-06
295	5.19785E-06
319	1.13754E-05
329	1.07257E-06
330	1.51604E-06
334	6.80671E-06
407	1.84194E-05
466	0.000799468
467	9.61189E-06
473	0.001802911
475	0.000903332
480	0.001761576
481	0.00019823
483	3.18059E-05
484	1.57276E-05
487	0.001987971
488	4.43467E-07
490	1.5511E-05
491	0.000392799
494	0.004286152
495	7.41518E-06
497	0.000276394
498	0.000287697
502	0.005149778
504	8.66514E-05
505	0.000339551
508	0.002231548
509	9.86457E-05
511	0.000129596
512	0.000187463
516	0.00652397
517	5.94762E-05
522	0.00053214
529	8.59604E-05
540	0.000126656
545	2.18743E-05
578	0.000199406
579	0.000757184
582	2.19671E-06
585	0.001880971
589	4.68941E-05
592	2.23074E-05
595	8.41556E-06
603	6.80258E-05
607	5.98165E-07
608	4.88845E-06
617	1.14476E-06
645	1.13445E-06
656	1.44385E-07
658	1.85637E-06
659	3.02176E-06
683	6.73451E-06
697	2.34109E-06
699	7.80709E-06
746	5.67226E-07
771	2.10389E-05
831	0.000809895
832	1.07773E-05
837	6.97481E-05
838	0.001767516
839	0.000920411
844	0.001797641
845	1.76356E-06
846	0.000202902
848	3.9912E-05
851	0.002031503
852	2.80313E-05
853	5.05346E-07
858	0.000506089
859	0.004232317
860	1.15095E-05
865	0.000575703
866	0.005155409
867	1.75324E-07
868	7.1161E-07
869	9.73462E-05
872	0.002576565
873	3.85404E-05
874	5.17722E-05
875	8.58161E-05
876	0.000146241
880	0.006803643
883	5.55984E-05
887	0.000545114
895	8.36194E-05
904	1.28915E-06
907	0.000127894
911	2.01004E-05
942	0.000203139
944	0.00076824
949	2.47517E-06
950	0.001914654
956	7.50388E-05
963	7.52863E-06
970	6.53134E-05
978	5.50724E-06
981	1.27884E-06
1021	1.65011E-07
1026	5.31129E-06
1050	7.54926E-06
1061	2.45454E-06
1065	8.54964E-06
1111	7.63176E-07
1138	2.30706E-05
1161	5.56912E-07
1194	0.000289615
1195	0.000393324
1196	0.000146024
1197	2.58861E-06
1201	3.51164E-05
1202	3.41882E-05
1203	0.00180025
1204	0.000936737
1208	4.92558E-05
1209	0.001778675
1210	6.54888E-06
1211	0.000207646
1215	0.001621574
1216	0.000460041
1217	4.63165E-05
1218	3.08983E-05
1219	5.56912E-07
1222	4.81729E-05
1223	0.000462351
1224	0.004283099
1225	6.49731E-06
1226	2.62986E-06
1229	0.000587017
1231	0.005169332
1233	1.95951E-07
1236	0.000648607
1237	0.002101911
1238	9.28187E-06
1240	9.18699E-05
1243	0.000306611
1244	0.00308517
1246	0.003748515
1250	5.20713E-05
1252	0.000556211
1253	5.36286E-07
1261	8.13711E-05
1271	0.000131431
1278	1.84606E-05
1306	0.0002068
1309	0.00077845
1313	2.733E-06
1315	0.001942892
1316	3.83651E-06
1320	8.07729E-05
1327	7.77615E-06
1335	7.01091E-05
1336	7.4255E-07
1341	6.08478E-06
1348	1.37165E-06
1387	1.85637E-07
1390	5.68257E-06
];
mktData = containers.Map({'rawData','asOfDate','ccy','name'}, ...
                        {rawData,'20170202','HKD','HSCEIDividendCurve'});
dividendCurveMktData = MktData(mktData);
AllMktData('dividendCurve') = dividendCurveMktData;

%% import and construct a repoRate Curve For HSCEI

rawData = [25	-0.008857458
56	-0.008848807
84	-0.009323235
147	0.028325005
238	-0.003911282
329	0.008802298
511	0.004032409
694	0.005260404
875	0.004162298
1061	0.004423197
1243	0.002615658
1427	0.002711188
1792	-0.012862688
2156	-0.000720733
2520	-0.008027754
2888	-0.010375813
3253	-0.012995928
3618	-0.005268827
];
mktData = containers.Map({'rawData','asOfDate','ccy','name'}, ...
                        {rawData,'20170202','HKD','HSCEIRepoRateCurve'});
repoRateCurveMktData = MktData(mktData);
AllMktData('repoRateCurve') = repoRateCurveMktData;

%% create  an EQBlackModel to calculate market PlainVanilla Options
modelParams= containers.Map({'modelName'},{'Black'});
HSCEIBlack = EQBlack(EQModel(Model(AllMktData,modelParams))); 
maturities = HSCEIBlack.volSurface.maturities;
strikes =    HSCEIBlack.volSurface.strikes;
impVolSurface = HSCEIBlack.volSurface.vol;
spot = HSCEIBlack.spot.params('rawData');
expiry = maturities;
fwdFactor = zeros(length(maturities),1);
fwdValues = zeros(length(maturities),1);
dfValues = zeros(length(maturities),1);
fwdMoneyness = zeros(length(maturities),length(strikes));
blackVolInput = zeros(length(maturities),length(strikes));

for i=1:length(maturities)
    fwd(i) = HSCEIBlack.Fwd(spot,expiry(i));
    df(i) = HSCEIBlack.zeroCurve.DF(expiry(i)/365.0);
    fwdFactor(i) = HSCEIBlack.FwdFactor(expiry(i));
    for j=1:length(strikes)
        fwdMoneyness(i,j) = strikes(j)/fwdFactor(i);
        blackVolInput(i,j) = impVolSurface(expiry(i),strikes(j));
        if fwdMoneyness(i,j) <= 1
%   blackOTMPrices2 : the usual black formula
%   blackOTMPrice   : Peter Jackel's Let's be rational
            blackOTMPrices2(i,j) = HSCEIBlack.BlackVanilla(expiry(i),expiry(i),spot,spot*strikes(j),impVolSurface(expiry(i),strikes(j)),'P')/fwd(i)/df(i);
            blackOTMPrices(i,j) = blackPrice(1,spot*strikes(j)/fwd(i),impVolSurface(expiry(i),strikes(j)),expiry(i)/365.0,-1);
        else
            blackOTMPrices2(i,j) = HSCEIBlack.BlackVanilla(expiry(i),expiry(i),spot,spot*strikes(j),impVolSurface(expiry(i),strikes(j)),'C')/fwd(i)/df(i);
            blackOTMPrices(i,j) = blackPrice(1,spot*strikes(j)/fwd(i),impVolSurface(expiry(i),strikes(j)),expiry(i)/365.0,1);
        end
    end
 end
            

%% create a DupireModel with initial modelParams
%Initialize local volatility
hsceiImpliedVolSurface = HSCEIBlack.volSurface;
localVolSurface = hsceiImpliedVolSurface;

modelParams= containers.Map({'localVolSurface','modelName'}, ...
                        {localVolSurface,'Dupire'});
                    
%% vol calibration grid setting
modelParams('Kmin') = 0.1;
modelParams('Kmax') = 2.5;
modelParams('Ns') = 241;

dK = (modelParams('Kmax') - modelParams('Kmin'))/(modelParams('Ns')-1);
modelParams('dK') = dK;

Ks = zeros(modelParams('Ns'),1);
for i=1:modelParams('Ns')
    Ks(i) = modelParams('Kmin') + modelParams('dK')*(i-1);
end

modelParams('Ks') = Ks;

% restricted interval for calibration target

modelParams('numOfCutoffTenor') = length(maturities);

modelParams('lowerCutoff') = 0.0;
modelParams('upperCutoff') = 2.0;

lowerCutoffVector = modelParams('lowerCutoff')*ones(length(maturities),1);
upperCutoffVector = modelParams('upperCutoff')*ones(length(maturities),1);

% 
% lowerCutoffVector(1) = 0.85;
% lowerCutoffVector(2) = 0.7;
% lowerCutoffVector(3) = 0.5;

% 
% upperCutoffVector(1) = 1.15;
% upperCutoffVector(2) = 1.3;
% upperCutoffVector(3) = 1.5;

modelParams('lowerCutoffVector') = lowerCutoffVector;
modelParams('upperCutoffVector') = upperCutoffVector;


% GFType : (Calibration) Forward Scheme option
%              -1 = one step matrix exponential , expmReghai, simple matrix exponential 
%              0  = one step matrix exponential , expmdemo1,scaling and squaring with Pade approximation
%              1  = one step matrix exponential , scaling and squaring with Pade approximation
%              2  = multisteps(daily) matrix exponential , scaling and squaring with Pade approximation
%              3  = one step Huge Decomposition & multiplication from right RG
%              4  = multi step Huge Decomposition & multiplication from right RG


%%
% % backGFType : (Pricing) backward scheme FD(PDE,GF) option
% %              -1 = one step matrix exponential , expmReghai, simple matrix exponential 
% %              0 = one step matrix exponential , expmdemo1,scaling and squaring with Pade approximation 
% %              1 = one step matrix exponential , scaling and squaring with Pade approximation
% %              2 = multisteps(daily) matrix exponential , scaling and squaring with Pade approximation
% %              3 =  one step Huge Decomposition & one step Thomas
% %              4 = multi step Huge Decomposition & multistep thomas
% %            : 14= one step matrix exponential(fwd), daily Thomas(backward)
% %              24= multisteps(daily) matrix exponential(fwd) dailyThomas(backward)
% %              34=  one step Huge, daily Thomas(backward)
% 
GFType = -1;
backGFType = -1;
modelParams('GFType') = GFType;
modelParams('backGFType') = backGFType;


% mc time steps in days 
modelParams('mcOneTimeStep') = 1095;
% modelParams('mcOneTimeStep') = 7;
modelParams('NMC') = 2^15;

%pricingSchemeType = 1 for PDE oneTimesStep
%pricingSchemeType = 3 for mc euler scheme with mcOneTimesStep
%pricingSchemeType = 4 for one time markov chain monte carlo

modelParams('pricingSchemeType') =1;

HSCEIDupire = EQDupireSpotGF(EQModel(Model(AllMktData,modelParams)));

out = HSCEIDupire.CalibrateToVolSurface(HSCEIBlack,'bootstrap');

aaa =1;








classdef EQBlack < EQModel
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % vol functor
        volSurface
    end
    
    methods
        function eqBlack = EQBlack(EQModel)
            if nargin > 0
                eqBlack.zeroCurve = EQModel.zeroCurve;
                eqBlack.dividendCurve = EQModel.dividendCurve;
                eqBlack.repoRateCurve = EQModel.repoRateCurve;
                eqBlack.spot = EQModel.spot;
                eqBlack.mktData =  EQModel.mktData;
                eqBlack.modelParams = EQModel.modelParams;
                
                if isKey(eqBlack.mktData,'impliedVolSurface')
                    eqBlack.volSurface = eqBlack.mktData('impliedVolSurface');
                    
                end
                
            end
        end
        
        function vanilla = BlackVanilla(eqBlack,txDay,tpDay,spot,strike,sig,CPFlag)
        %BlackVanilla:a function that computes a blackCallAndPut price
        %arguments
        %txDay =(days)time to reset
        %tpDay =(days)time to pay
        %sig= implied volatility
        %strike  = caplet strike
        %CPFlag = Call Or Put
            fwd = eqBlack.Fwd(spot,txDay);
            tx = txDay/365.0;
            tp = tpDay/365.0;
            df = eqBlack.zeroCurve.DF(tp);
            d1= (log(fwd/strike)+(sig^2/2.0)*tx)/(sig*sqrt(tx));
            d2 = d1 -sig*sqrt(tx);
            if strcmp(CPFlag,'C')
%                 vanilla = df*(fwd*H_ncdf(d1)-strike*H_ncdf(d2));
                vanilla = df*blackPrice(fwd,strike,sig,tx,1);
            elseif strcmp(CPFlag,'P')
%                 vanilla = df*(-fwd*H_ncdf(-d1)+strike*H_ncdf(-d2));
                vanilla = df*blackPrice(fwd,strike,sig,tx,-1);
            end
            
        end

    end
    
end


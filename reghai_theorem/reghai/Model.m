classdef Model < H_RootObject
    %Model is the basic object that represents a pricing model
 
    
    properties(SetAccess = public)
        mktData 
        modelParams
    end
    
    methods
        function model = Model(mktData, modelParams)
            if nargin > 0
                model.mktData = mktData;
                model.modelParams = modelParams;
            end
        end
        
        function out = Variance(model,sig,t,order)
            if nargin < 4
                intOrder = 1;
            else
                intOrder = order;
            end
            switch order
                case 1
                    out = Variance1(model,sig,t);
                case 2
                    out = Variance2(model,sig,t);
                otherwise
                    disp('not implemented')
            end
        end
        
        function out = Variance1(model,sig,t)
            for i=1:size(sig.tenor,2)
                if t<= sig.tenor(i)
                    break;
                end
            end
            tidx = i;
            sum = 0.0;
            prev_t = 0.0;
            curr_t = 0.0;
            for i=1:tidx
                if i< tidx
                    curr_t = sig.tenor(i);
                else
                    curr_t = t;
                end
                sum = sum + sig.quote(i)*(curr_t-prev_t);

                prev_t = sig.tenor(i);
            end
            out = sum;

        end
        
        function out = Variance2(model,sig,t)
            for i=1:size(sig.tenor,2)
                if t<= sig.tenor(i);
                    break;
                end
            end
            tidx = i;
            sum = 0.0;
            prev_t = 0.0;
            curr_t = 0.0;
            for i=1:tidx
                if (i< tidx)  curr_t = sig.tenor(i);else  curr_t = t;end;
                sum = sum + sig.quote(i)*sig.quote(i)*(curr_t-prev_t);
                prev_t = sig.tenor(i);
            end
            out = sum;

        end
        function out = HWLocalVariance(model,from,to,sig1,a1,t1,sig2,a2,t2)
            sig.tenor = unique([sig1.tenor sig2.tenor]);
            quote1 = zeros(1,size(sig.tenor,2));
            quote2 = zeros(1,size(sig.tenor,2));
            for i=1:size(sig.tenor,2)
                quote1(i) = H_interpolation(sig1.tenor,sig1.quote,sig.tenor(i),0);
                quote2(i) = H_interpolation(sig2.tenor,sig2.quote,sig.tenor(i),0);
            end 
            sig1.tenor =sig.tenor;
            sig2.tenor= sig.tenor;
            sig1.quote = quote1;
            sig2.quote = quote2;
            
            for i=1:size(sig.tenor,2)
                if from<= sig.tenor(i)
                    break;
                end
            end
            fromIdx = i;
            
            for i=1:size(sig.tenor,2)
                if to<= sig.tenor(i)
                    break;
                end
            end
            toIdx = i;
            
            eps = 0.00000001;
            Tiny = 0;
            if abs(a1) <= eps && abs(a2) <= eps
                Tiny = 0;
            elseif abs(a1) <= eps && abs(a2) > eps
                Tiny = 1;
            elseif abs(a1) > eps && abs(a2) <= eps
                Tiny = 2;
            else
                Tiny = 3;
            end
            
            variance =zeros(1,4);
            
            switch Tiny
                case 0
                    lastU = from;
                    for i= fromIdx:toIdx
                        if(i<toIdx) U=sig.tenor(i);else U=to;end;
                        variance(1) = variance(1) + sig1.quote(i)*sig2.quote(i)*(U-lastU);
                        variance(2) = variance(2) + sig1.quote(i)*sig2.quote(i)*0.5*(U*U-lastU*lastU);
                        variance(3) = variance(3) + sig1.quote(i)*sig2.quote(i)*0.5*(U*U-lastU*lastU);
                        variance(4) = variance(4) + sig1.quote(i)*sig2.quote(i)*1.0/3.0*(U*U*U-lastU*lastU*lastU);
                        lastU = U;
                    end
                    
                    out =     t1 * t2 * variance(1);
                    out = out -1.0*t1 * variance(2);
                    out = out -1.0*t2 * variance(3);
                    out = out +         variance(4);
                case 1
                case 2
                case 3
            end
 

        end
        
        function out = SP2(model,sig,to,alpha)
            values = zeros(1,4);
            values(1) = HWLocalVariance(model,0,to,sig,0,to+alpha,sig,0,to+alpha);
            values(2) = HWLocalVariance(model,0,to,sig,0,to+alpha,sig,0,to      );
            values(3) = HWLocalVariance(model,0,to,sig,0,to      ,sig,0,to+alpha);
            values(4) = HWLocalVariance(model,0,to,sig,0,to      ,sig,0,to      );
            out =  values(1) - values(2) - values(3) + values(4);
        end
        
        function out = MPI(model,sig,to,alpha,tp)
            values = zeros(1,4);
            values(1) = HWLocalVariance(model,0,to,sig,0,to+alpha,sig,0,tp      );
            values(2) = HWLocalVariance(model,0,to,sig,0,to      ,sig,0,tp      );
            values(3) = HWLocalVariance(model,0,to,sig,0,to+alpha,sig,0,to+alpha);
            values(4) = HWLocalVariance(model,0,to,sig,0,to      ,sig,0,to      );
            out =  values(1) - values(2) - 0.5*(values(3) - values(4));
        end
    end
    
end

